<div class="pip-block">
    <div class="pip-block-content">
        <p>
            Wilt u uw persoonlijke WOZ- en belastinggegevens digitaal
            inzien?<br />

            Klikt u dan op
            <a target="_blank" href="[% multichannel_session_url | html_entity %]">
                Mijn WOZ- en belastinggegevens
                <i class="icon-font-awesome icon-external-link"></i>
            </a> (let op: deze pagina opent in een nieuwe sessie).<br />

            Hier kunt u aanslagbiljetten en taxatieverslagen
            raadplegen/downloaden en/of digitaal een bezwaarschrift indienen.
        </p>

        <h2>
            Let op: belangrijke informatie over het indienen van een
            bezwaarschrift!
        </h2>

        <h3>Wilt u bezwaar indienen?</h3>
        <p>
            U vindt de WOZ-waarde van uw woning te hoog? Of u bent het om een
            andere reden niet eens met de aanslag? Belt u dan eerst even met
            de gemeente.<br />

            Op die manier kan via een quick-scan door de medewerkers bekeken
            worden of de waarde inderdaad te hoog is of kan aan u worden
            uitgelegd waarom de waarde correct is vastgesteld. Als na een
            nadere beoordeling van de taxateur blijkt dat de waarde inderdaad
            te hoog is, ontvangt u binnen een maand een correctie op de
            waarde. Dit scheelt u &eacute;n de gemeente tijd en geld.
            Bovendien beschikt u op deze manier eerder over een gecorrigeerde
            waarde van uw woning dan na een tijdrovende bezwaarprocedure.
        </p>

        <h3>No cure - no pay bureaus</h3>
        <p>
            Er verschijnen in de pers regelmatig berichten over bureaus en
            makelaars die zich opwerpen als belangenbehartiger voor de burger
            bij de bezwarenafhandeling Wet Waardering onroerende zaken.<br />

            Argumenten zoals: "WOZ waarde te hoog? Wij maken gratis bezwaar
            voor u!" worden aangehaald om hen kosteloos (no cure no pay) in
            te schakelen voor het indienen van een bezwaarschrift. Het klinkt
            mooi, maar hoe werkt het nu eigenlijk?<br />

            Het is belangrijk om te weten dat deze bureaus en makelaars voor
            zaken waarbij het bezwaar is toegekend en de WOZ-waarde is
            gewijzigd, een flinke kostenvergoeding van de gemeente
            krijgen.<br />

            In de praktijk komt het er op neer dat als in een bezwarenprocedure
            in 2016 de WOZ-waarde van een woning met &euro; 10.000,- wordt verlaagd,
            dit een vermindering van de aanslag voor u oplevert van &euro; 19,38.
            Het bureau of makelaar krijgt vervolgens voor zijn werkzaamheden een bedrag
            uitbetaald dat kan variëren van &euro; 246,00 tot wel &euro; 740,00.
        </p>
    </div>
</div>
