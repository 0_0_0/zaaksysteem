$(document).ready(function() {
    $('form.zapi_help').submit(function() {
        var postdata    = $(this).serialize();
        var method      = 'GET';
        var url         = $(this).attr('action');

        if (postdata.length) {
            method      = 'POST';
        }

        $('pre.zapi_output').load(
            url,
            postdata
        );

        $.ztWaitStop();

        return false;
    });
});
