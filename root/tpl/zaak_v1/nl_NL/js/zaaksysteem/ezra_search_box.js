/** Ezra search box
 *
 */

(function($) {

    var methods = {
        required: function() {
            return ['url', 'select_handler', 'title'];
        },
        init: function(args) {

            return this.each(function() {
                var $this = $(this),
                    data = $this.data('ezra_search_box');

                // Already initialized
                if (data) {
                  //  $this.ezra_search_box('log','ezra_search_box already initialized!');
                    return true;
                }

                // Retrieve custom options from rel
                if ($this.attr('rel')) {
                    args = $.extend(
                        {},
                        args,
                        $this.ezra_search_box('retrieve_options', $this.attr('rel'))
                    );
                }

                // Check required parameters
                var required = $this.ezra_search_box('required');

                for(var i = 0; i < required.length; i++) {
                    var param = required[i];

                    if(!args[param]) {
                        $(this).ezra_search_box(
                            'log',
                            'Missing required parameter ' + param
                        );
                        return false;                        
                    }
                }

                $this.on('click.ezra_search_box', function() {
                    return $this.ezra_search_box('show');
                })

                // Save local settings
                $(this).data('ezra_search_box', {
                    settings: args,
                    html: $this.html()
                });

                $(this).addClass('ezra_search_box-initialized');
            });
        },
        update_options : function(new_options) {
            var $this = $(this), data = $this.data('ezra_search_box');
            
            for (var key in new_options) {
                var value = new_options[key];
                data.settings.options[key] = value;
            }
        },
        show: function() {
            var $this = $(this),
                data = $this.data('ezra_search_box');

            ezra_dialog({
                url         : data.settings.url,
                title       : data.settings.title,
                which       : '#searchdialog',
                cgi_params  : data.settings.options
            }, function() {
                    var title = '';

                    $('#searchdialog form').on(
                        'submit.ezra_search_box',
                        function() {
                            $this.ezra_search_box('load_results');
                            return false;
                        }
                    );
                }
            );

            return false;
        },
        load_results: function() {
            var $this = $(this);
            var data = $this.data('ezra_search_box');

            var dialog = $('#searchdialog .dialog-content');
            var params = $('#searchdialog .dialog-content form').serializeArray();

            $('#betrokkene_loader').removeClass('disabled');

            var result_container = dialog.find('.ezra_search_box-results table');

            result_container.load(
                data.settings.url,
                params,
                function(responseText, textStatus) {

                    initializeEverything(result_container);

                    $('#betrokkene_loader').addClass('disabled');

                    $('#accordion').accordion({ heightStyle: 'content' });
                    $('#accordion').accordion('option', 'active', 1);
                    
                    // install click handler for results
                    result_container.on('click', 'tr', function() {
                        var clicked_obj = $(this);

                        if (typeof data.settings.select_handler == 'function') {
                            data.settings.select_handler(clicked_obj);
                            $('#searchdialog').dialog('close');
                        } else {
                            //WTF
                        }
                    });
                }
            );
        },
        retrieve_options: function(line) {
            return getOptions(line);
        },
        log: function(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    $.fn.ezra_search_box = function(method) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_search_box' );
        }
    }
})(jQuery);
