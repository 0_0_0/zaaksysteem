#! /bin/sh

if [ ! -e /var/mail/zaaksysteem ]; then
    mkdir /var/mail/zaaksysteem \
        /var/mail/zaaksysteem/new \
        /var/mail/zaaksysteem/cur \
        /var/mail/zaaksysteem/tmp

    chown zaaksysteem:zaaksysteem /var/mail/zaaksysteem
fi

exec su -c 'python3 /microsmtpd.py' zaaksysteem
