FROM perl:5.26

# Please see dev-bin/docker-perl for building and pushing this base
# layer

ENV DEBIAN_FRONTEND noninteractive

# libssl1.0-dev is required, because Crypt::OpenSSL::{X509,RSA,VerifyX509} do not
# support libssl1.1 yet.
# It can be removed once
# https://github.com/dsully/perl-crypt-openssl-x509/issues/53 and related bugs
# for the other projects are fixed.
#
RUN groupadd -g 1001 zaaksysteem \
    && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem \
    && apt-get update \
    && apt-get --no-install-recommends -y install \
        libpq-dev \
        uuid-dev \
        locales \
        xmlsec1 \
        unzip \
        libssl1.0-dev \
        libmagic-dev \
        poppler-utils \
    && localedef -i nl_NL -c -f UTF-8 -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY cpanfile /opt/zaaksysteem/cpanfile

# Disable network testing: Test::RequiresInternet
ENV NO_NETWORK_TESTING 1

# List::MoreUtils::XS installed outside of cpanfile due to dependency
# loophole: https://rt.cpan.org/Public/Bug/Display.html?id=122875
# cpanfile deps are not installed in order and running it twice is
# stupid enough, but trice or more would be considered ugly
#
# GnuPG::Interface requires patches which are found in the bug report,
# but haven't been merged upstream, so we don't require the testsuite
# right now: https://rt.cpan.org/Public/Bug/Display.html?id=102651
#
# Changed to two layers because.. I want some layering to stay when
# debugging module install failures. It takes ages otherwise.
# If the second run fails, fine, break things.

RUN cpanm List::MoreUtils::XS \
  && cpanm --notest GnuPG::Interface \
  # needed for ZS::Tools
  && cpanm Module::Install \
  && cpanm https://gitlab.com/zaaksysteem/zaaksysteem-tools.git@9763a75ef8eb1cc0a3219e9f1bd9a218cee002c4 \
  && cpanm https://gitlab.com/zaaksysteem/bttw-tools.git@0cddffa4322d10dd53ff9a0b78e941a6e6e54cb2 \
  && cpanm \
  https://gitlab.com/zaaksysteem/zaaksysteem-instance_config.git@5ece28da4a2829bb238345e08ff9debf52a1ab00 \
  && ( cpanm --installdeps /opt/zaaksysteem || true ) \
  && rm -rf ~/.cpanm

RUN cpanm --installdeps /opt/zaaksysteem \
  && rm -rf ~/.cpanm
