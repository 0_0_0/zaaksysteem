# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is open source software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

## Branches

* master
    The master branch contains the latest *stable* and released version of
    Zaaksysteem. This is the version most people want.

* quarterly
    The quarterly branch has a common ancestor at the current master
    branch, and contains all new features, bugfixes and other
    modifications done in a two week sprint This branch is considered
    *stable*, as it has been tested internally, but it has not seen a
    production environment yet.

* sprint
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    It is part of our bi-weekly sprint cycle and will be reset at every
    sprint.

## Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.

# Running the development environment

Zaaksysteem uses [docker](http://www.docker.com/) to manage development
environments that are the same for every developer. To create a new
environment, you first need to install `docker` and `docker-compose`.
For docker please follow the installation instructions as found on the
[docker documenation page](https://docs.docker.com/engine/installation/).

Docker is very disk consuming, make sure you have sufficient space
somewhere for docker to use. One can tweak the default `/var/lib/docker`
to be elsewhere. On Debian/Ubuntu edit `/etc/default/docker` and add the
`-g` option: `DOCKER_OPTS="-g /path/to/free/space"`.  On Fedora/CentOS
one should edit `/etc/sysconfig/docker`, eg `other_args="-g
/path/to/free/space"`. For more information see the [docker forums]
(https://forums.docker.com/t/how-do-i-change-the-docker-image-installation-directory/1169).

You can reclaim disk space from old (unused) container images using:

`docker image prune`

## Generate configuration files
When you have completed the instalation you need to create some
configuration files by running `dev-bin/generate_config.sh`.

## Start your docker

You can now start your development environment by running:

`docker-compose up`

This will build and start all the relevant containers.

You can connect to the development environment on https://dev.zaaksysteem.nl/
now.

## SSL certificates

After building the frontend container, you can extract the generated
development CA certificate from it using:

`dev-bin/get_ca_certificate.sh > ca.crt`

By importing that certificate on your system, all connections to
`dev.zaaksysteem.nl` will be trusted (there won't be certificate warnings to
ignore anymore).

## Override the docker-compose.yml file

In case you want to override certain `docker-compose.yml` entries but you
don't want to check them you can make use of the
`docker-compose.overide.yml` file, you can find a working example in
`docker-compose.overide.example`.

## Frontend development

Frontend developers may choose to have a local installed development
tools and don't want to rebuild their docker image on every change. They
are encouraged to use the override method that is provided via
`docker-compose.overide.example`. After adding the override method and
starting the frontend container, it is possible to run our JavaScript
bundler (Webpack) in dev mode (`npm run dev`) on your host machine
instead of inside the Docker container. This will allow hot reloading
and affords a speed increase in bundling to achieve much faster
development of frontend modules. See the `client` and `frontend`
directories for more information.

## Backend development

Building a new Perl docker layer can be done by running:
`dev-bin/docker-perl`.

The base image will only be build if the checksum of the `cpanfile` and
`docker/Dockerfile.perl` does not match the reference in the
`docker/Dockerfile.backend`.

It is advised to pin versions of external modules in case you are using
git repositories. This is because the docker build otherwise uses the
cache. You could also force a non-cached build.

Pushing the new layer is done like so (this can only be done by Mintlab
developers): `dev-bin/docker-perl -p`

## End-to-end testing (E2E)

Before starting with tests, make sure that docker, webpack and webdriver are running:

`docker-compose up`
`client/npm run dev`
`client/npm run e2e-server`

When running the tests for the very first time, webdriver should be updated:

`client/npm run e2e-update`

To facilitate end-to-end testing, we maintain a database image in a secondary
repository. We've created a script to manage loading and dumping that test
database, and to manage the external repository (clone, commit, etc.) for test
runs:

`dev-bin/testbase.sh`

Running the script will show an explanation of the different options. Before
running tests it is recommended to load the latest database version into the
testbase-instance:

`dev-bin/testbase.sh --pull`
`dev-bin/testbase.sh --load`
`dev-bin/testbase.sh --create`

Please update your webdriver before hand:

`client/npm run e2e-update`

There are multiple ways to run tests.

Run all the tests.

`client/npm run e2e-test`

Optionally the tests can be restricted to those inside a specific folder and/or file.

`client/npm run e2e-test --folder=folderName --file=fileName`

Run testsuites. The names of the configured suites can be found in the config file.

`client/npm run e2e-suite suiteName`

Run sequences of tests in parallel. The test sequences can be found in the config file.
When the number of sessions is not defined it will run all sequences in parallel.
When the number of sessions is defined to be less than the sequences, it will simply
iterate to a next sequence upon finishing one.

`client/npm run e2e-multi --e2esessions=3`

## Connect with your database

To connect to the database. you can you can run the PostgreSQL client in the
database container:

`docker-compose exec database psql -U zaaksysteem`

The "db/" directory in the source is available as "/opt/zaaksysteem/db" in the
container.

It's also possible to connect to port 5432 on localhost, using a tool like
PgAdmin. The username is "zaaksysteem" and the password "zaaksysteem123"

## Contained email

Some parts of Zaaksysteem send email. As long as the backend is not configured
to use an external mail server (this is the default case), all email messages
are sent to a special mailbox using a small SMTP daemon. This mailbox is
available using IMAP:

* Protocol: IMAP
* Username: zaaksysteem
* Password: zaaksysteem
* Host: localhost
* Port: 1143

If you have the "mutt" email program installed, you can connect like this:

`mutt -f imap://zaaksysteem:zaaksysteem@localhost:1143/`

### Support

We only support the community version of the software through the
[wiki](http://wiki.zaaksysteem.nl/). For professional support, please contact
[Mintlab](http://www.zaaksysteem.nl/).

-- 
The [Mintlab](http://www.mintlab.nl/) Team
