#! perl

# This prevents classes inheriting ZSTest to run twice...better idea welcome.
BEGIN {
    $ENV{'Test::Class::Load'} = 1;
};

use Test::Class::Load 't/lib/TestFor/External';
Test::Class->runtests();
