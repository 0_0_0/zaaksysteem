package TestFor::Catalyst::Controller::API::V1::Dashboard::Favourite;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

use Zaaksysteem::UserSettings;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Dashboard::Favourite - Favourites API for Dashboard

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Dashboard/Favourite.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/dashboard/Favourite> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create favourite

Url: /api/v1/dashboard/favourite/casetype

B<Request>

=begin javascript

{
   "reference_id" : "2806b1cf-faa0-4304-bdb2-8b24768dcdc6"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-1d65b6-0a77e2",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2e5ab1a6-fda7-47fb-907c-53c49f6af1b3",
                  "label" : "YFcd1AKyk6",
                  "order" : 20,
                  "reference_id" : "8ecbb15f-1f78-4eb4-b369-c333cba6779d",
                  "reference_type" : "casetype"
               },
               "reference" : "2e5ab1a6-fda7-47fb-907c-53c49f6af1b3",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_favourite_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;

        my $casetype  = $zs->create_zaaktype_predefined_ok;

        my $o_data    = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/create',
            {
              'reference_id' => $casetype->_object->uuid,
            },
        );

        is($o_data->{result}->{type}, 'set', 'Got a set of data.');
        is(@{ $o_data->{result}->{instance}->{rows} }, 1, 'Got a single row');

        $self->_validate_json_instance($o_data->{result}->{instance}->{rows}->[0]);
    }, 'api/v1/dashboard/favorite/casetype/create: create a simple casetype favorite');

}

=head3 list favourite

Url: /api/v1/dashboard/favourite/casetype

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90fa13-b05b31",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 5,
            "total_rows" : 5
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
                  "label" : "IDig0VJxc7",
                  "order" : "10",
                  "reference_id" : "4e6a194e-c533-4738-bbb8-2e91653c07e6",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
                  "label" : "JFqo4EVme3",
                  "order" : "20",
                  "reference_id" : "4ea2c978-a690-400a-a0f0-0e48912ef588",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "VKiy9CSjy7",
                  "order" : "30",
                  "reference_id" : "3194834e-e15a-48ee-8ee0-f6246c7b68ed",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "ISwb7ICdj7",
                  "order" : "40",
                  "reference_id" : "820052f5-c366-435d-9537-294906f894c4",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "LPdx5HXuv4",
                  "order" : "50",
                  "reference_id" : "6aeab45a-ee8a-4962-afc1-f8f52a418711",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_favourite_list : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $self->_create_usersettings;
        
        $mech->zs_login;

        $mech->get(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype',
        );

        my $o_data   = $zs->get_json_as_perl($mech->content);

        is($o_data->{result}->{type}, 'set', 'Found a valid set');
        is(@{ $o_data->{result}->{instance}->{rows} }, 5, 'Got five rows');

        $self->_validate_json_instance($o_data->{result}->{instance}->{rows}->[0]);
        # diag($mech->content);
    }, 'api/v1/dashboard/favourite/casetype: list the favourites');
}


=head3 update favourite

Url: /api/v1/dashboard/favourite/casetype/b66be381-282c-4fbb-a603-2d683518670d/update

B<Request>

=begin javascript

{
   "reference_id" : "2806b1cf-faa0-4304-bdb2-8b24768dcdc6"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90fa13-b05b31",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 5,
            "total_rows" : 5
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
                  "label" : "IDig0VJxc7",
                  "order" : "10",
                  "reference_id" : "4e6a194e-c533-4738-bbb8-2e91653c07e6",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
                  "label" : "JFqo4EVme3",
                  "order" : "20",
                  "reference_id" : "4ea2c978-a690-400a-a0f0-0e48912ef588",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "VKiy9CSjy7",
                  "order" : "30",
                  "reference_id" : "3194834e-e15a-48ee-8ee0-f6246c7b68ed",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "ISwb7ICdj7",
                  "order" : "40",
                  "reference_id" : "820052f5-c366-435d-9537-294906f894c4",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "LPdx5HXuv4",
                  "order" : "50",
                  "reference_id" : "6aeab45a-ee8a-4962-afc1-f8f52a418711",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_favourite_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;
        my $settings = $self->_create_usersettings;

        my @rows     = $settings->favourites->search({reference_type => 'casetype'});

        ### Update
        my $params   = {
            'reference_id' => '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
        };

        my $n_data   = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $rows[0]->id . '/update',
            $params
        );

        is($n_data->{result}->{instance}->{rows}->[0]->{type}, 'favourite', 'Got valid type');

        for my $row (@{ $n_data->{result}->{instance}->{rows} }) {
            my $instance = $row->{instance};
            if ($instance->{id} eq $rows[0]->id) {
                for my $param (keys %$params) {
                    is($instance->{$param}, $params->{$param}, "Param $param correctly updated");
                }
            }
        }

    }, 'api/v1/dashboard/favourite/casetype/UUID/update: update a widget');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;
        my $settings = $self->_create_usersettings;

        my @favrows  = $settings->favourites->search({reference_type => 'casetype'});
        my $favrow   = $favrows[1];

        ### Check sub
        my $check_order = sub {
            my ($rows, $place) = @_;

            my $count = 1;
            for my $row (@$rows) {
                if ($row->{instance}->{id} eq $favrow->id) {
                    is($count, $place, "Ordering: Found row on $place place");
                }

                $count++;
            }
        };

        ### Lets do some checking, no move
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $favrow->id . '/update',
            {
                reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
            }
        );

        $check_order->($zs->get_json_as_perl($mech->content)->{result}->{instance}->{rows}, 2);

        ### Lets do some checking, beginning
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $favrow->id . '/update',
            {
                reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
                order => 1,
            }
        );

        $check_order->($zs->get_json_as_perl($mech->content)->{result}->{instance}->{rows}, 1);

        ### End
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $favrow->id . '/update',
            {
                reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
                order => 0,
            }
        );

        $check_order->($zs->get_json_as_perl($mech->content)->{result}->{instance}->{rows}, 5);

        ### Third
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $favrow->id . '/update',
            {
                reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
                order => 25,
            }
        );

        $check_order->($zs->get_json_as_perl($mech->content)->{result}->{instance}->{rows}, 3);




    }, 'api/v1/dashboard/favourite/casetype/UUID/update: reordering');
}

=head3 bulk update favourite

Url: /api/v1/dashboard/favourite/bulk_update

B<Request>

=begin javascript

{
   "updates" : [
      {
         "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
         "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd24"
      },
      {
         "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
         "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd25"
      }
   ]
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-eee827-fac45c",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 5,
            "total_rows" : 5
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
                  "label" : "EZbs1TNxh2",
                  "order" : 10,
                  "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd24",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
                  "label" : "ZGri1YQkv7",
                  "order" : 20,
                  "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd25",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "UGru0UWbt8",
                  "order" : 30,
                  "reference_id" : "513569e8-6f0d-4477-92af-2a2c6f914deb",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "RVoi7QWvr9",
                  "order" : 40,
                  "reference_id" : "c0ff8d70-cf26-45a6-b89f-825f6ec9a8d6",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "QSju4RPru0",
                  "order" : 50,
                  "reference_id" : "1e0fcbb7-34bb-4694-86c4-01ce6c895ae1",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_favourite_bulk_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;
        my $settings = $self->_create_usersettings;

        my @newids = qw/7806b1cf-faa0-4304-bdb2-8b24768dcd24 7806b1cf-faa0-4304-bdb2-8b24768dcd25/;

        my @rows   = $settings->favourites->search({ 'reference_type' => 'casetype' });

        my $params = {
            updates => [
                map {
                    {
                        id            => $rows[$_]->id,
                        reference_id  => $newids[$_],
                    }
                } qw/0 1/
            ]
        };

        my $o_data = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/bulk_update',
            $params
        );

        is($o_data->{result}->{instance}->{rows}->[0]->{type}, 'favourite', 'Got valid type');

        for my $row (@{ $o_data->{result}->{instance}->{rows} }) {
            my $compare;
            if ($row->{reference} eq $params->{updates}->[0]->{id}) {
                $compare = $params->{updates}->[0];
            } elsif ($row->{reference} eq $params->{updates}->[1]->{id}) {
                $compare = $params->{updates}->[1];
            }

            my $instance = $row->{instance};

            for my $param (keys %$compare) {
                next if $param eq 'id';
                is($instance->{$param}, $compare->{$param}, "Param $param correctly updated");
            }
        }

    }, 'api/v1/dashboard/widget/bulk_update: bulk update a widget');

}

=head3 delete favourite

Url: /api/v1/dashboard/favourite/casetype/b66be381-282c-4fbb-a603-2d683518670d/delete

B<Request>

=begin javascript

{}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ea80c8-d8d303",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 0,
            "total_rows" : 0
         },
         "rows" : []
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_delete : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;
        my $settings = $self->_create_usersettings;

        my ($favrow) = $settings->favourites->search({reference_type => 'casetype'});

        my $o_data   = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/dashboard/favourite/casetype/' . $favrow->id . '/delete',
            {}
        );

        is(@{ $o_data->{result}->{instance}->{rows} }, 4, 'Got four rows');

        for my $row (@{ $o_data->{result}->{instance}->{rows} }) {
            if ($row->{instance}->{id} eq $favrow->id) {
                is(0, 'Delete: no result found after delete');
            }
        }

    }, 'api/v1/dashboard/widget/delete: delete a widget');
}

sub _validate_json_instance {
    my $self      = shift;
    my $instance  = shift;

    is($instance->{type}, 'favourite');

    my $row       = $instance->{instance};

    ok(exists $row->{$_}, "Found key $_") for qw/id label order reference_id reference_type/;
    like($row->{id}, qr/^[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+$/, 'Got a valid id');
    like($row->{reference_id}, qr/^[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+\-[a-z0-9]+$/, 'Got a valid reference_id');
    ok($row->{reference_type}, 'Got a reference type');
    ok($row->{label}, 'Got a filled label');
}

sub _create_usersettings {
    my $self      = shift;

    my $db_settings  = {
      'backwards_compatible_setting' => {
        'setting_uno' => 'value'
      },
      'favourites' => [
        {
          'id' => '2806b1cf-faa0-4304-bdb2-8b24768dcd27',
          'order' => 10,
          'reference_type' => 'case',
          'reference_id' => '2806b1cf-faa0-4304-bdb2-8b24768dcdc7'
        },
        {
          'id' => '2806b1cf-faa0-4304-bdb2-8b24768dcd28',
          'order' => 20,
          'reference_type' => 'case',
          'reference_id' => '2806b1cf-faa0-4304-bdb2-8b24768dcdc8'
        },
      ]
    };

    $zs->create_zaaktype_predefined_ok for 1..5;

    my @objects = $schema->resultset('ObjectData')->search({ object_class => 'casetype'})->all;

    my $order = 10;
    for my $object (@objects) {
        push(
            @{ $db_settings->{favourites} },
            {
              'id' => '2806b1cf-faa0-4304-bdb2-8b24768dcd' . $order,
              'order' => $order,
              'reference_type' => 'casetype',
              'reference_id' => $object->uuid,
            }
        );

        $order += 10;
    }

    my $settings    = Zaaksysteem::UserSettings->new_from_settings($db_settings, schema => $schema);

    my $admin = $schema->resultset('Subject')->search({username => 'admin'})->first;

    $admin->settings($settings->to_settings);
    $admin->update;

    return $settings;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
