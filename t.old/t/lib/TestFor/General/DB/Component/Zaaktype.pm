package TestFor::General::DB::Component::Zaaktype;

# ./zs_prove -v t/lib/TestFor/General/DB/Component/Zaaktype.pm
use base qw(ZSTest);
use TestSetup;

sub zaaktype_deleted : Tests {
    $zs->txn_ok(
        sub {
            my $ct = $zs->create_zaaktype_predefined_ok();

            my $model = $zs->object_model;
            my $rs    = $model->search_rs('casetype');
            is($rs->count, 1, 'One casetype found in object_data');

            my $id = $ct->id;

            $ct->deleted(DateTime->now());
            $ct->update();

            $rs = $model->search_rs('casetype');
            is($rs->count, 0, 'No casetype found in object_data');
            is(
                $zs->schema->resultset('Zaaktype')->search_rs({
                        id      => $id,
                        deleted => undef
                })->count, 0,
                "Zaaktype is not to be found"
            );
        }
    );
}

sub zaaktype_delete : Tests {

SKIP: {
    skip "AUTUMN2015BREAK:  Can't locate object method \"set_delete\" via package Zaaksysteem::Model::DB::Zaaktype",3;

    $zs->txn_ok(
        sub {
            my $ct = $zs->create_zaaktype_predefined_ok();
            my $id = $ct->id;

            my $model = $zs->object_model;
            my $rs    = $model->search_rs('casetype');
            is($rs->count, 1, 'One casetype found in object_data');

            $ct->delete;

            $rs = $model->search_rs('casetype');
            is($rs->count, 0, 'No casetype found in object_data');
            is(
                $zs->schema->resultset('Zaaktype')->search_rs({
                        id      => $id,
                        deleted => undef
                })->count, 0,
                "Zaaktype is not to be found"
            );

            $ct->deleted(undef);
            $ct->update();

            my $case = $zs->create_case_ok(zaaktype => $ct);
            throws_ok(
                sub {
                    $ct->delete();
                },
                qr#zs/casetype/delete/nen: Unable to delete case type, cases still depend on me#,
                "ZS-5968"
            );

        }
    );
}; # END SKIP

}

1;

__END__

=head1 NAME

TestFor::General::DB::Component::Zaaktype - A zaaktype tester

=head1 DESCRIPTION

Testing zaaktypes

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
