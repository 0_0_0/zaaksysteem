package TestFor::General::Backend::Sysin::Modules::OverheidIO;

use base qw(ZSTest);
use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::OverheidIO;
use Zaaksysteem::BR::Subject;

use constant EXAMPLE_JSON => '{"totalItemCount":1,"pageCount":1,"size":100,"_links":{"self":{"href":"\\/api\\/kvk\\/?filters[handelsnaam]=Mintlab&fields[]=vestigingsnummer&fields[]=straat&fields[]=huisnummer&fields[]=plaats&fields[]=postcode&fields[]=huisnummertoevoeging&ovio-api-key=59bc9ccb54a0125984b596fe192840ea960772e010d920a0f20b6d73c62d037c"}},"_embedded":{"rechtspersoon":[{"dossiernummer":"51902672","handelsnaam":"Mintlab B.V.","huisnummer":"7","huisnummertoevoeging":"UNIT 521-522","plaats":"Amsterdam","postcode":"1051JL","straat":"Donker Curtiusstraat","subdossiernummer":"0000","vestigingsnummer":"21881022","_links":{"self":{"href":"\\/api\\/kvk\\/51902672\\/0000"}}}]}}';

=head1 NAME

TestFor::General::Backend::Sysin::Modules::OverheidIO - OverheidIO tests

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/OverheidIO.pm

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 module_overheidio_usage_search_company

=cut

sub module_overheidio_usage_search_company : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my $interface   = $zs->create_interface_ok(
                name => 'overheidio',
                module => 'overheidio',
                interface_config => {
                    overheid_io_key => ($ENV{OVERHEID_IO_KEY} || 'test'),
                    overheid_io_module => 'kvk',
                    simulator       => 1,
                }
            );


            ### Spoof _get_json_from_overheid
            no warnings 'redefine';
            local *Zaaksysteem::Backend::Sysin::Modules::OverheidIO::_get_json_from_overheid = sub {
                my $json = EXAMPLE_JSON;

                return JSON->new->decode($json);
            } unless $ENV{OVERHEID_IO_KEY};

            ### Get results
            my $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab' });
            my $results     = $transaction->get_processor_params->{result};

            my $first       = shift @$results;

            is($first->{subject_type}, 'company', 'Found value for "subject_type"');
            is($first->{subject}->{company}, 'Mintlab B.V.', 'Found value for "company"');
            is($first->{subject}->{coc_number}, '51902672', 'Found value for "coc_number"');
            is($first->{subject}->{coc_location_number}, '21881022', 'Found value for "coc_location_number"');
            is($first->{subject}->{address_residence}->{street}, 'Donker Curtiusstraat', 'Found value for "street"');
            is($first->{subject}->{address_residence}->{street_number}, '7', 'Found value for "street_number"');
            is($first->{subject}->{address_residence}->{street_number_suffix}, 'UNIT 521-522', 'Found value for "street_number_suffix"');
            is($first->{subject}->{address_residence}->{city}, 'Amsterdam', 'Found value for "city"');
            is($first->{subject}->{address_residence}->{zipcode}, '1051JL', 'Found value for "zipcode"');
        },
        'Got "Mintlab" company from search'
    );
}

=head2 module_overheidio_usage_bridge

=cut

sub module_overheidio_usage_bridge : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my $interface   = $zs->create_interface_ok(
                name => 'overheidio',
                module => 'overheidio',
                interface_config => {
                    overheid_io_key => ($ENV{OVERHEID_IO_KEY} || 'test'),
                    overheid_io_module => 'kvk',
                    simulator       => 1,
                }
            );


            ### Spoof _get_json_from_overheid
            no warnings 'redefine';
            local *Zaaksysteem::Backend::Sysin::Modules::OverheidIO::_get_json_from_overheid = sub {
                my $json = EXAMPLE_JSON;

                return JSON->new->decode($json);
            } unless $ENV{OVERHEID_IO_KEY};

            ### USAGE VIA BRIDGE
            my $bridge          = Zaaksysteem::BR::Subject->new(
                schema          => $schema,
                remote_search   => 'openkvk',
            );

            my @results = $bridge->search(
                {
                    'subject_type' => 'company',
                    'subject.coc_number' => '51902672',
                }
            );

            my $first = shift @results;
            ### END USAGE

            ok($first, 'Got OpenKVK resutls');

            is($first->subject_type, 'company', 'Got company from subject bridge');
            is($first->subject->company, 'Mintlab B.V.', 'Got "Mintlab" from bridge');
            ok($first->subject->$_, "Got value for: $_") for qw/
                coc_number
                coc_location_number
                company
            /;

            ok($first->subject->address_residence->$_, "Got value for: $_") for qw/
                street
                street_number
                street_number_suffix
                zipcode
                city
            /;

            ### Import company
            $bridge->remote_import($first);

            my $company = $schema->resultset('Bedrijf')->search({ dossiernummer => 51902672})->first;

            ok($company->authenticated, 'Imported and found authenticated company');
            is($company->authenticatedby, 'overheidio', 'Authenticated by: overheidio');
        },
        'Got "Mintlab" company from search'
    );
}

=head1 IMPLEMENTATION

=head2 module_overheidio_search_company_live

=cut

sub module_overheidio_search_company_live : Tests {
    SKIP: {
        skip("live overheid.io tests. Run live tests by setting environment variable: OVERHEID_IO_KEY=[api_key]", 1) unless $ENV{OVERHEID_IO_KEY};

        $zs->txn_ok(
            sub {
                my $interface   = $zs->create_interface_ok(
                    name => 'overheidio',
                    module => 'overheidio',
                    interface_config => {
                        overheid_io_key => $ENV{OVERHEID_IO_KEY},
                        overheid_io_module => 'kvk',
                        simulator       => 1,
                    }
                );

                my ($transaction, $results, $first);

                ### Got results, street
                {
                    $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab', 'address_residence.street' => 'Dark' });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 0, 'No company found with incorrect street');

                    $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab', 'address_residence.street' => 'Donker' });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 1, 'A company found with correct street');
                }

                ### Got results, zipcode
                {
                    $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab', 'address_residence.zipcode' => '1051AP', 'address_residence.street_number' => 7 });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 0, 'No company found with incorrect zipcode');

                    $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab', 'address_residence.zipcode' => '1051JL', 'address_residence.street_number' => 7 });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 1, 'A company found with correct zipcode');
                }

                ### Got results, kvknummer
                {
                    $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab', 'coc_number' => '51902671' });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 0, 'No company found with incorrect kvknumber');

                    $transaction = $interface->process_trigger('search_companies', { 'coc_number' => '51902672' });
                    $results     = $transaction->get_processor_params->{result};
                    is(@$results, 1, 'A company found with correct kvknumber');
                }

            },
            'Got "Mintlab" company from search'
        );
    };

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
