package TestFor::General::DynClass;
use base qw(ZSTest);

use Moose;
use Moose::Meta::Class;

use Zaaksysteem::Types qw/UUID/;

use TestSetup;

=head1 NAME

TestFor::General::DynClass - Dynamic class generation.

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ./zs_prove -v t/lib/TestFor/General/DynClass.pm

=head1 DESCRIPTION

For generating a new plain moose class. E.g.: for generating a protected implementation, with only attributes,
to represent a hash.

=head1 USAGE

=head1 IMPLEMENTATION

=head2 dynclass_loading_and_exceptions

=cut

sub dynclass_loading_and_exceptions : Tests {
    my $self        = shift;
    my $dynclass    = $self->_load_valid_dynclass;

    can_ok($dynclass, qw/_create_dynamic_classes validate_dynamic_class _gen_dynamic_profile/);

    throws_ok(
        sub {
            my $errorclass = ref $dynclass;

            $errorclass->new();
        },
        qr/_dynamic_classes.*is required/,
        'Exception: check for required params',
    );

    throws_ok(
        sub {
            $dynclass->_gen_dynamic_profile('TestFor::General::DynClass::FsckUp');
        },
        qr/Cannot find class/,
        'Exception: check for valid class',
    );

    lives_ok(
        sub {
            $dynclass->_gen_dynamic_profile('TestFor::General::DynClass::DynamicClass');
        },
        'No Exception: got a valid class',
    );
}

sub dynclass_validation : Tests {
    my $self        = shift;
    my $dynclass    = $self->_load_valid_dynclass;

    my $checks      = {
        required    => [qw/id uuid/],
        optional    => [qw/label/],
    };

    my $profile     = $dynclass->_gen_dynamic_profile('TestFor::General::DynClass::DynamicClass');

    for my $reqopt (keys %$checks) {
        for my $key (@{ $checks->{$reqopt} }) {
            ok($profile->{$reqopt}->{$key}, "Found $reqopt key in profile: " . $key);
        }
    }

    throws_ok(
        sub {
            $dynclass->validate_dynamic_class(
                'TestFor::General::DynClass::DynamicClass',
                {
                    id  => 'String',
                    uuid => '24234-a243-4534-a43'
                }
            )
        },
        qr/invalid: uuid/,
        'Validation: invalid uid'
    );

    throws_ok(
        sub {
            $dynclass->validate_dynamic_class(
                'TestFor::General::DynClass::DynamicClass',
                {
                    uuid => '0f46c442-e5ba-4a53-98c2-617ceb5623c5'
                }
            )
        },
        qr/missing: id/,
        'Validation: missing id'
    );

    lives_ok(
        sub {
            my $opts = $dynclass->validate_dynamic_class(
                'TestFor::General::DynClass::DynamicClass',
                {
                    uuid    => '0f46c442-e5ba-4a53-98c2-617ceb5623c5',
                    id      => '55',
                    label   => 'Frits',
                }
            )->valid;

            ok($opts->{$_}, "Found value for $_") for qw/uuid id label/;
        }
    );
}

sub _load_valid_dynclass {
    my $self        = shift;


    my $class       = $self->_load_class_with_role('Zaaksysteem::DynClass');

    my $dynclass    = new_ok(
        $class,
        [ 
            _dynamic_classes => [
                {
                    class       => 'TestFor::General::DynClass::DynamicClass',
                    attributes  => {
                        id              => {
                            is          => 'ro',
                            isa         => 'Str',
                            writer      => '_set_id',
                            required    => 1,
                        },
                        label           => {
                            is          => 'ro',
                            writer      => '_set_label',
                        },
                        uuid            => {
                            is          => 'ro',
                            isa         => UUID,
                            required    => 1,
                        }
                    },
                    methods     => {
                        deflate_to_settings     => sub {
                            my $self        = shift;

                            return {
                                id                  => $self->id,
                                order               => $self->order,
                                reference_id        => $self->reference_id,
                                reference_type      => $self->reference_type,
                            };
                        },
                    },
                    moose           => 1,
                }
            ]
        ],
        'Succesfully instantiated Zaaksysteem::DynClass'
    );

    return $dynclass;
}

### TODO: move this to TestUtils, can be usefull for plain role testing.
sub _load_class_with_role {
    my $self        = shift;
    my $role        = shift;

    return Moose::Meta::Class->create_anon_class(
        roles        => [$role],
        cache        => 1,
        superclasses => [qw/TestUtils::Mock::Role::Moose/],
    )->name;

}

package TestUtils::Mock::Role::Moose;

use Moose;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
