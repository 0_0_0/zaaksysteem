/*global define,window,document*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.dom.getWindowHeight', function ( ) {
		
		var win = window,
			doc = document.documentElement,
			body = document.body,
			func;
		
		if("innerHeight" in win) {
			func = function ( ) {
				return win.innerHeight;
			};
		} else {
			func = function ( ) {
				return Math.min(doc.clientHeight, body.clientHeight);
			};
		}
		
		return func;
		
	});
	
})();
