/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.ActiveSubjectController', [ '$scope', 'subjectService', function ( $scope, subjectService ) {
			
			$scope.enableSession = function ( ) {
				subjectService.enableSession($scope.identifier);	
			};
			
			$scope.disableSession = function ( ) {
				subjectService.disableSession();
			};
			
		}]);
})();