/*global angular,setTimeout*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.objecttype')
		.controller('nl.mintlab.admin.objecttype.ObjectTypeRemoveController', [ '$scope', '$http', '$window', 'translationService', 'capitalizeFilter', function ( $scope, $http, $window, translationService, capitalizeFilter ) {
			
			$scope.removeObject = function ( ) {
				$http({
					method: 'POST',
					url: '/api/object/' + $scope.objectTypeId + '/delete'
				})
					.success(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get(capitalizeFilter($scope.objectTypeName) + ' is verwijderd')
						});

						// timeout ensures user is able to read message
						setTimeout(function ( ) {
							$window.location.reload();
						}, 1500);
					})
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get(capitalizeFilter($scope.objectTypeName) + ' kon niet worden verwijderd. Probeer het later opnieuw.')
						});
					});
			};
			
		}]);
	
})();
