/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.net')
		.service('fileUploader', [ '$q', '$window', function ( $q, $window ) {
			
			var EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
				inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
				FileUpload = window.zsFetch('nl.mintlab.docs.FileUpload'),
				IFrameUpload = window.zsFetch('nl.mintlab.docs.IFrameUpload'),
				safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
				supports = !!$window.File;
				
			function FileUploader ( ) {
				this._uploadList = [];
			}
			
			inherit(FileUploader, EventDispatcher);
			
			FileUploader.prototype.upload = function ( file, url, params, scope ) {
				var upload = supports ? new FileUpload(file) : new IFrameUpload(file),
					deferred = $q.defer(),
					promise = deferred.promise,
					uploadList = this._uploadList;
					
				if(!params) {
					params = {};
				}
					
				if(!supports) {
					params.return_content_type = 'text/plain';
				}
				
				uploadList.push(upload);
				
				function resolve ( ) {
					deferred.resolve(upload);
				}
				
				function reject ( ) {
					deferred.reject(upload);
				}
				
				upload.subscribe('complete', function ( ) {
					if(scope) {
						safeApply(scope, resolve);
					} else {
						resolve();
					}
				});
				upload.subscribe('error', function ( ) {
					if(scope) {
						safeApply(scope, reject);
					} else {
						reject();
					}
				});
				upload.subscribe('end', function ( ) {
					uploadList.splice(indexOf(uploadList, upload), 1);
				});
				
				upload.promise = promise;
				
				this.publish('fileadd', upload);
				upload.send(url, params);
				
				return upload;
			};
			
			FileUploader.prototype.getUploadList = function ( ) {
				return this._uploadList;
			};
			
			FileUploader.prototype.supports = function ( ) {
				return supports;	
			};
			
			
			return new FileUploader();
			
			
		}]);
	
})();
