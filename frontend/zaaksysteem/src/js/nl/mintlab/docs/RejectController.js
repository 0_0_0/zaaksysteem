/*global angular*/
angular
	.module('Zaaksysteem.docs')
	.controller('nl.mintlab.docs.RejectController', [
		'$scope',
		function ( $scope ) {
			$scope.reject = function ( entity ) {
				$scope.rejectFile([entity], $scope.rejection_reason, $scope.reject_to_queue);
				$scope.closePopup();
			};
		}
	]);
