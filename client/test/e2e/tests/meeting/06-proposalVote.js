import navigate from './../../functions/common/navigate';
import getProposalValue from './../../functions/meeting/getProposalValue';

describe('when opening a case and adding a note in the meeting app', ( ) => {

    beforeAll(( ) => {

        navigate.as('burgemeester', '/vergadering/bbv/');

        $('meeting-app ui-view .meeting-item-titlebar').click();

        let firstProposal = element.all(by.css('.proposal-item-table tbody tr')).first(),
            firstVote = element.all(by.css('.vote-button')).first(),
            voteTextarea = $('textarea'),
            voteSave = $('.proposal-vote-view__header-buttons button:nth-child(2)');

        firstProposal.click();

        browser.waitForAngular();

        firstVote.click();

        voteTextarea.sendKeys('this is my comment');

        voteSave.click();

    });

    it('the note should have the given content', ( ) => {

        expect(getProposalValue('Voorstel burgemeester')).toEqual('Akkoord');
        expect(getProposalValue('Voorstel burgemeester opmerkingen')).toEqual('this is my comment');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
