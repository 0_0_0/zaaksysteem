import navigate from './../../functions/common/navigate';

describe('when opening the meeting app', ( ) => {

    beforeAll(( ) => {

        navigate.as('burgemeester', '/vergadering/bbv/');

    });

    it('should contain a header', ( ) => {

        let nav = element.all(by.css('meeting-nav'));

        expect(nav.count()).toBeGreaterThan(0);

    });

    it('should contain a meetingList where the items are initially closed', ( ) => {

        let openProposals = element.all(by.css('.proposal-item-table tbody tr'));

        expect(openProposals.count()).toBe(0);

    });

    it('should contain a meetingList with two proposals', ( ) => {

        $('meeting-app ui-view .meeting-item-titlebar').click();

        let openProposals = element.all(by.css('.proposal-item-table tbody tr'));

        expect(openProposals.count()).toBe(2);

    });

    it('should contain an unsorted meetingList with three proposals', ( ) => {

        let unfilteredOpenProposals = element.all(by.css('.proposal-item-table tbody tr'));

        $('.meeting-nav__button-bar button:nth-child(2)').click();

        expect(unfilteredOpenProposals.count()).toBe(3);

    });

    it('should contain a meetingList with two proposals', ( ) => {

        $('.meeting-nav__tabs a:nth-child(2)').click();

        let closedProposals = element.all(by.css('.proposal-item-table tbody .closed'));

        expect(closedProposals.count()).toBe(2);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
