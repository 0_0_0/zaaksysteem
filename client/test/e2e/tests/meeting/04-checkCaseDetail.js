import navigate from './../../functions/common/navigate';
import getProposalValue from './../../functions/meeting/getProposalValue';
import checkPresenceByText from './../../functions/common/input/checkPresenceByText';

describe('when clicking on a case in the caseList in the meeting app', ( ) => {

    beforeAll(( ) => {

        navigate.as('burgemeester', '/vergadering/bbv/');

    });

    it('should navigate to the detailView', ( ) => {

        let lastProposal = element.all(by.css('meeting-app ui-view .proposal-item-table tbody tr')).last();

        $('meeting-app ui-view .meeting-item-titlebar').click();

        lastProposal.click();

        expect(browser.getCurrentUrl()).toContain('/!voorstel/');

    });

    it('should contain a detailView', ( ) => {

        let proposalDetailView = element.all(by.css('proposal-detail-view'));

        expect(proposalDetailView.count()).toBeGreaterThan(0);

    });

    it('should have a detailView that contains a proposal note bar', ( ) => {

        let noteBar = $('proposal-note-bar');

        expect(noteBar.isPresent()).toBe(true);

    });

    it('should have a detailView that contains a proposal vote bar', ( ) => {

        let voteBar = $('proposal-vote-bar');

        expect(voteBar.isPresent()).toBe(true);

    });

    it('should display the information of the case properly', ( ) => {

        expect(getProposalValue('Zaaknummer')).toEqual('41');
        expect(getProposalValue('Zaaktype')).toEqual('Voorstel 2');
        expect(getProposalValue('Voorstel onderwerp')).toEqual('Openstaand voorstel');
        expect(getProposalValue('Vertrouwelijkheid')).toEqual('Openbaar');
        expect(getProposalValue('Voorstel agenderingswijze')).toEqual('Paraafstukken');
        expect(checkPresenceByText('Datum voorstel 1', '.proposal-detail-view__label')).toBe(false);
        expect(checkPresenceByText('Datum voorstel 2', '.proposal-detail-view__label')).toBe(true);
        expect(getProposalValue('Datum voorstel 2')).toEqual('16 jan. 2017');
        expect(getProposalValue('Resultaat')).toEqual('aangehouden');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
