import navigate from './../../functions/common/navigate';

describe('when opening a case and adding a note in the meeting app', ( ) => {

    beforeAll(( ) => {

        navigate.as('burgemeester', '/vergadering/bbv/');

        $('meeting-app ui-view .meeting-item-titlebar').click();

        let firstProposal = element.all(by.css('.proposal-item-table tbody tr')).first(),
            noteBar = $('.proposal-detail-view__notecontent'),
            noteTextarea = $('textarea'),
            noteSave = $('.proposal-note-view__header-button .mdi-check');

        firstProposal.click();

        browser.waitForAngular();

        noteBar.click();

        noteTextarea.clear().sendKeys('this is my note');

        noteSave.click();

    });

    it('the note should have the given content', ( ) => {

        let firstNote = element.all(by.css('.proposal-detail-view__noteplaceholder')).first();

        expect(firstNote.getText()).toEqual('this is my note');

    });

    it('the ability to add additional notes should still be present', ( ) => {

        let addNote = element.all(by.css('.proposal-detail-view__noteplaceholder')).last();

        expect(addNote.getText()).toEqual('Voeg een nieuwe persoonlijke notitie toe');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
