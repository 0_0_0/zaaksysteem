import navigate from './../../functions/common/navigate';

describe('when opening the meeting app', ( ) => {

    beforeAll(( ) => {

        navigate.as('burgemeester', '/vergadering/bbv/');

    });

    it('should contain a header', ( ) => {

        let nav = element.all(by.css('meeting-nav'));

        expect(nav.count()).toBeGreaterThan(0);

    });

    it('should contain tabs', ( ) => {

        let tabs = element.all(by.css('.meeting-nav__tabs a'));

        expect(tabs.count()).toBe(2);

    });

    describe('when opening the archived tab', ( ) => {

        let tab = element.all(by.css('.meeting-nav__tabs a')),
            closedProposal = $('.proposal-item-table tbody .closed');

        beforeAll(() => {

            tab.last().click();

            $('meeting-app ui-view .meeting-item-titlebar').click();

        });

        it('when clicking on a tab it should navigate to that tab', ( ) => {

            expect(closedProposal.isPresent()).toBe(true);

        });

        afterAll(() => {

            tab.first().click();

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
