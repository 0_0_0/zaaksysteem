import navigate from './../../functions/common/navigate';

describe('when logging in to the meeting app', ( ) => {

    describe('with valid credentials', ( ) => {

        beforeEach(() => {

            navigate.as('burgemeester', '/vergadering/bbv/');

        });

        it('should redirect to the application', ( ) => {

            expect(browser.getCurrentUrl()).toMatch(/vergadering/);

        });

    });

    describe('with invalid credentials', ( ) => {

        beforeEach(() => {

            navigate.as('burgemeester', '/vergadering/bbv/', 'wrong password');

        });

        it('should redirect to the login form', ( ) => {

            expect(browser.getCurrentUrl()).toMatch('/auth/page');

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
