import navigate from './../../../../../functions/common/navigate';
import closeDialog from './../../../../../functions/common/closeDialog';

describe('when opening case 70', ( ) => {

    let phaseActions = element.all(by.css('.sidebar-item-list li')),
        expectedConfigs = [
            {
                name: 'R. Betrokkene toevoegen',
                state: false,
                settings: [
                    {
                        type: 'static',
                        dataName: 'related_subject',
                        value: 'R. Betrokkene toevoegen'
                    },
                    {
                        type: 'static',
                        dataName: 'related_subject_role',
                        value: 'Advocaat'
                    },
                    {
                        type: 'static',
                        dataName: 'magic_string_prefix',
                        value: 'advocaat'
                    },
                    {
                        type: 'static',
                        dataName: 'pip_authorized',
                        value: 'Nee'
                    },
                    {
                        type: 'static',
                        dataName: 'notify_subject',
                        value: 'Nee'
                    }
                ]
            },
            {
                name: 'Regelactie betrokkene toevoegen',
                state: false,
                settings: [
                    {
                        type: 'static',
                        dataName: 'related_subject',
                        value: 'Regelactie betrokkene toevoegen'
                    },
                    {
                        type: 'static',
                        dataName: 'related_subject_role',
                        value: 'Auditor'
                    },
                    {
                        type: 'static',
                        dataName: 'magic_string_prefix',
                        value: 'auditor'
                    },
                    {
                        type: 'static',
                        dataName: 'pip_authorized',
                        value: 'Ja'
                    },
                    {
                        type: 'static',
                        dataName: 'notify_subject',
                        value: 'Ja'
                    }
                ]
            },
            {
                name: 'Faseactie sjabloon 1',
                state: false,
                settings: [
                    {
                        type: 'text',
                        dataName: 'name',
                        value: 'Faseactie sjabloon'
                    },
                    {
                        type: 'radio',
                        dataName: 'filetype',
                        value: '1'
                    },
                    {
                        type: 'select',
                        dataName: 'case_document',
                        value: 'Geen'
                    }
                ]
            },
            {
                name: 'Faseactie sjabloon 2',
                state: true,
                settings: [
                    {
                        type: 'text',
                        dataName: 'name',
                        value: 'Faseactie sjabloon 2'
                    },
                    {
                        type: 'radio',
                        dataName: 'filetype',
                        value: '2'
                    },
                    {
                        type: 'select',
                        dataName: 'case_document',
                        value: 'Document'
                    }
                ]
            },
            {
                name: 'Faseactie email 1',
                state: false,
                settings: [
                    {
                        type: 'select',
                        dataName: 'recipient_type',
                        value: 'Aanvrager (T. Testpersoon)'
                    },
                    {
                        type: 'text',
                        dataName: 'recipient_cc',
                        value: ''
                    },
                    {
                        type: 'text',
                        dataName: 'recipient_bcc',
                        value: ''
                    },
                    {
                        type: 'text',
                        dataName: 'email_subject',
                        value: 'Faseactie email 1 - [[zaaknummer]]'
                    },
                    {
                        type: 'textarea',
                        dataName: 'email_content',
                        value: 'Faseactie email 1 - [[zaaknummer]]'
                    }
                ]
            },
            {
                name: 'Faseactie email 2',
                state: true,
                settings: [
                    {
                        type: 'select',
                        dataName: 'recipient_type',
                        value: 'Overig'
                    },
                    {
                        type: 'text',
                        dataName: 'recipient_address',
                        value: 'test1@test.nl'
                    },
                    {
                        type: 'text',
                        dataName: 'recipient_cc',
                        value: 'test2@test.nl'
                    },
                    {
                        type: 'text',
                        dataName: 'recipient_bcc',
                        value: 'test3@test.nl'
                    },
                    {
                        type: 'text',
                        dataName: 'email_subject',
                        value: 'Faseactie email 2 - [[zaaknummer]]'
                    },
                    {
                        type: 'textarea',
                        dataName: 'email_content',
                        value: 'Faseactie email 2 - [[zaaknummer]]'
                    }
                ]
            },
            {
                name: 'Faseactie zaaktype 1',
                state: false,
                settings: [
                    {
                        type: 'select',
                        dataName: 'department-picker',
                        value: '-Backoffice'
                    },
                    {
                        type: 'select',
                        dataName: 'role-picker',
                        value: 'Behandelaar'
                    },
                    {
                        type: 'select',
                        dataName: 'subcase_requestor_type',
                        value: 'Aanvrager van de huidige zaak (T. Testpersoon)'
                    },
                    {
                        type: 'select',
                        dataName: 'type',
                        value: 'Deelzaak'
                    },
                    {
                        type: 'select',
                        dataName: 'resolve_in_phase',
                        value: 'Afhandelen'
                    },
                    {
                        type: 'checkbox',
                        dataName: 'copy_attributes',
                        value: false
                    },
                    {
                        type: 'checkbox',
                        dataName: 'copy_attributes',
                        value: false
                    }
                ]
            },
            {
                name: 'Faseactie zaaktype 2',
                state: true,
                settings: [
                    {
                        type: 'select',
                        dataName: 'department-picker',
                        value: '-Frontoffice'
                    },
                    {
                        type: 'select',
                        dataName: 'role-picker',
                        value: 'Behandelaar'
                    },
                    {
                        type: 'select',
                        dataName: 'subcase_requestor_type',
                        value: 'Behandelaar van de huidige zaak'
                    },
                    {
                        type: 'select',
                        dataName: 'type',
                        value: 'Gerelateerde zaak'
                    },
                    {
                        type: 'checkbox',
                        dataName: 'copy_attributes',
                        value: true
                    },
                    {
                        type: 'checkbox',
                        dataName: 'automatic_assignment',
                        value: true
                    }
                    // BUG: Subject settings are not present.
                    // The test can't be written, because the elements attributes are not known.
                ]
            },
            {
                name: 'Backoffice, Behandelaar',
                state: false,
                settings: [
                    {
                        type: 'select',
                        dataName: 'department-picker',
                        value: '-Backoffice'
                    },
                    {
                        type: 'select',
                        dataName: 'role-picker',
                        value: 'Behandelaar'
                    }
                ]
            }
        ],
        form = $('zs-case-action-form');

    beforeAll(( ) => {

        navigate.as('admin', 70);

    });

    for (const config in expectedConfigs) {

        ((phaseAction, expectedConfig) => {

            it(`should see ${expectedConfig.name} in position ${config + 1}`, ( ) => {

                expect(phaseAction.getText()).toEqual(expectedConfig.name);

            });

            it(`should see the state as ${expectedConfig.name} as ${expectedConfig.state}`, ( ) => {

                expect(phaseAction.$('.sidebar-item-toggle input[checked="checked"]').isPresent()).toBe(expectedConfig.state);

            });

            describe(`and when opening ${expectedConfig.name}`, ( ) => {

                const advancedSettings = form.$('[data-name="expand"]');

                beforeAll(( ) => {

                    phaseAction.click();

                    advancedSettings.isDisplayed().then(isDisplayed => {
                        if ( isDisplayed ) {
                        advancedSettings.click();
                        }
                    });

                });

                for ( const configSetting in expectedConfig.settings ) {

                    ((setting) => {

                        it(`the setting ${setting.dataName} should be ${setting.value}`, ( ) => {

                            if ( setting.type === 'text') {

                                expect(form.$(`[data-name="${setting.dataName}"]`).$('input').getAttribute('value')).toEqual(setting.value);

                            } else if ( setting.type === 'textarea' ) {

                                expect(form.$(`[data-name="${setting.dataName}"]`).$('textarea').getAttribute('value')).toEqual(setting.value);

                            } else if ( setting.type === 'radio' ) {

                                expect(form.$(`[data-name="${setting.dataName}"] vorm-radio-group`).$(`label:nth-child(${setting.value}) input`).isSelected()).toBe(true);

                            } else if ( setting.type === 'select' ) {

                                expect(form.$(`[data-name="${setting.dataName}"]`).$('select [selected="selected"]').getText()).toContain(setting.value);

                            } else if ( setting.type === 'checkbox' ) {

                                expect(form.$(`[data-name="${setting.dataName}"]`).$('input').isSelected()).toBe(setting.value);

                            } else if ( setting.type === 'static' ) {

                                expect(form.$(`[data-name="${setting.dataName}"]`).$('.form-field-value-list').getText()).toBe(setting.value);

                            }

                        });

                    })(expectedConfig.settings[configSetting]);

                }

                it(`the button for executing should be ${!expectedConfig.state}`, ( ) => {

                    expect(form.$('.form-actions [type="button"]').isEnabled()).toBe(!expectedConfig.state);

                });

                afterAll(( ) => {

                    closeDialog();

                });

            });

        })(phaseActions.get(config), expectedConfigs[config]);

    }

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
