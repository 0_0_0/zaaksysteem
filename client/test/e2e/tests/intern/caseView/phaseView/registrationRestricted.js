import navigate from './../../../../functions/common/navigate';
import casePhase from './../../../../functions/intern/caseView/casePhase';
import caseNav from './../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

describe('when opening case 128 and opening phase 1', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 128);

        caseNav.openPhase(1);

    });

    it('the attributes should be closed', ( ) => {

        let attributes = element.all(by.css('.phase-form .vorm-field-item')),
            expected = [];

        attributes.each(() => {
            expected.push(false);
        });

        expect(caseAttribute.statuses(attributes)).toEqual(expected);

    });

    it('a button to unlock the phase should be present', ( ) => {

        expect(casePhase.getLockButtonText()).toEqual('ontgrendelen');

    });

    describe('and when unlocking', ( ) => {
    
        beforeAll(( ) => {
    
            casePhase.unlock();
    
        });
    
        it('the attributes should be open', ( ) => {
    
            let attributes = element.all(by.css('.phase-form .vorm-field-item')),
            expected = [];

            attributes.each(() => {
                expected.push(true);
            });

            expect(caseAttribute.statuses(attributes)).toEqual(expected);
    
        });

        it('a button to lock the phase should be present', ( ) => {

            expect(casePhase.getLockButtonText()).toEqual('vergrendelen');

        });

        describe('and when locking', ( ) => {
        
            beforeAll(( ) => {
        
                casePhase.lock();
        
            });
        
            it('the attributes should be closed', ( ) => {

                let attributes = element.all(by.css('.phase-form .vorm-field-item')),
                expected = [];

            attributes.each(() => {
                expected.push(false);
            });

            expect(caseAttribute.statuses(attributes)).toEqual(expected);

            });

            it('a button to unlock the phase should be present', ( ) => {

                expect(casePhase.getLockButtonText()).toEqual('ontgrendelen');

            });
        
        });

    });

});

describe('when opening case 128, opening phase 1, unlocking and refreshing', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 128);

        caseNav.openPhase(1);

        casePhase.unlock();

        navigate.to(128);

        caseNav.openPhase(1);

    });

    it('the attributes should be closed', ( ) => {

        let attributes = element.all(by.css('.phase-form .vorm-field-item')),
            expected = [];

        attributes.each(() => {
            expected.push(false);
        });

        expect(caseAttribute.statuses(attributes)).toEqual(expected);

    });

    it('a button to unlock the phase should be present', ( ) => {

        expect(casePhase.getLockButtonText()).toEqual('ontgrendelen');

    });

});

describe('when opening case 129 and opening phase 1', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 129);

        caseNav.openPhase(1);

    });

    it('a button to unlock the phase should not be present', ( ) => {

        expect($('.phase-header-status button.phase-unlock-button').isDisplayed()).toBe(false);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
