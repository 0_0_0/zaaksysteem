import navigate from './../../../../../functions/common/navigate';
import caseNav from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const confidentialityPublic = $('[data-name="vertrouwelijkheid_openbaar"]');
const confidentialityIntern = $('[data-name="vertrouwelijkheid_intern"]');
const confidentialityConfidential = $('[data-name="vertrouwelijkheid_vertrouwelijk"]');

describe('when opening case 59 with confidentiality public', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 59);

        caseNav.openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', ( ) => {

        expect(caseAttribute.getClosedValue(confidentialityPublic)).toEqual('True');
        expect(caseAttribute.getClosedValue(confidentialityIntern)).toEqual('False');
        expect(caseAttribute.getClosedValue(confidentialityConfidential)).toEqual('False');

    });

    afterAll(( ) => {

        browser.get('/intern');

    });

});

describe('when opening case 60 with confidentiality internal', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 60);

        caseNav.openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', ( ) => {

        expect(caseAttribute.getClosedValue(confidentialityPublic)).toEqual('False');
        expect(caseAttribute.getClosedValue(confidentialityIntern)).toEqual('True');
        expect(caseAttribute.getClosedValue(confidentialityConfidential)).toEqual('False');

    });

    afterAll(( ) => {

        browser.get('/intern');

    });

});

describe('when opening case 61 with confidentiality internal', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 61);

        caseNav.openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', ( ) => {

        expect(caseAttribute.getClosedValue(confidentialityPublic)).toEqual('False');
        expect(caseAttribute.getClosedValue(confidentialityIntern)).toEqual('False');
        expect(caseAttribute.getClosedValue(confidentialityConfidential)).toEqual('True');

    });

    afterAll(( ) => {

        browser.get('/intern');

    });

});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
