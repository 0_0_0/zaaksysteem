import navigate from './../../../functions/common/navigate';
import plusMenu from './../../../functions/intern/plusMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';

xdescribe('when uploading a document via the plusmenu', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 110);

        plusMenu.uploadDocument('text.txt');

        caseNav.openTab('docs');

    });

    it('there should be a document in the documentstab', ( ) => {

        expect($('.document-list-main').getText()).toContain('text.txt');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
