import navigate from './../../../functions/common/navigate';
import plusMenu from './../../../functions/intern/plusMenu';
import form from './../../../functions/common/form';

describe('when starting a case registration', ( ) => {

    beforeAll(( ) => {

        navigate.as();

        let newCase = {
            casetype: 'basic casetype',
            requestorType: 'natuurlijk_persoon',
            requestor: '123456789'
        };

        plusMenu.createCase(newCase);

    });

    it('should redirect to zaak/create', ( ) => {

        expect(browser.getCurrentUrl()).toContain('/intern/aanvragen/');

    });

    describe('and completing the steps', ( ) => {

        beforeAll(( ) => {

            form.goNext();

        });

        it('should move to the next phase', ( ) => {

            expect(browser.getCurrentUrl()).toContain('/bevestig/');

        });

        describe('and submitting the form', ( ) => {

            beforeAll(( ) => {

                form.goNext();

            });

            it('should redirect to the case', ( ) => {

                expect($('.case-view').isPresent()).toBe(true);

            });

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
