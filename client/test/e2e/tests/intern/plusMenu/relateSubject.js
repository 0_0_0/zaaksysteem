import navigate from './../../../functions/common/navigate';
import plusMenu from './../../../functions/intern/plusMenu';
import caseNav from './../../../functions/intern/caseView/caseNav';

describe('when opening case 110', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', 110);

    });

    describe('when relating a subject of type citizen via the plusmenu', ( ) => {

        let data = {
            subjectType: 'citizen',
            subjectToRelate: 'Plusknop burger',
            role: 'Advocaat'
        };

        beforeAll(( ) => {

            plusMenu.relateSubject(data);

            caseNav.openTab('relations');

        });

        it('there should be a related subject in the relationstab', ( ) => {

            expect($('.related_subjects zs-table-body').getText()).toContain(data.subjectToRelate);

        });

        afterAll(( ) => {

            caseNav.openTab('phase');

        });

    });

    describe('when relating a subject of type organisation via the plusmenu', ( ) => {

        let data = {
            subjectType: 'organisation',
            subjectToRelate: 'Plusknop organisatie',
            role: 'auditor'
        };

        beforeAll(( ) => {

            plusMenu.relateSubject(data);
            caseNav.openTab('relations');

        });

        it('there should be a related subject in the relationstab', ( ) => {

            expect($('.related_subjects zs-table-body').getText()).toContain(data.subjectToRelate);

        });

        afterAll(( ) => {

            caseNav.openTab('phase');

        });

    });

    describe('when relating a subject with manual magicstring via the plusmenu', ( ) => {

        let data = {
            subjectType: 'citizen',
            subjectToRelate: 'Plusknop magicstring',
            role: 'Anders',
            roleOther: 'Eigen magicstring'
        };

        beforeAll(( ) => {

            plusMenu.relateSubject(data);

            caseNav.openTab('relations');

        });

        it('there should be a related subject in the relationstab', ( ) => {

            expect($('.related_subjects zs-table-body').getText()).toContain(data.roleOther);

        });

        afterAll(( ) => {

            caseNav.openTab('phase');

        });

    });

    describe('when relating a subject that is pip authorized and notified via the plusmenu', ( ) => {

        let data = {
            subjectType: 'citizen',
            subjectToRelate: 'Plusknop gemachtigd',
            role: 'Aannemer',
            authorisation: true,
            notification: true
        };

        beforeAll(( ) => {

            plusMenu.relateSubject(data);

            caseNav.openTab('relations');

        });

        it('there should be a related subject with authorisation', ( ) => {

            expect($('.related_subjects zs-table-body .mdi-check').isPresent()).toBe(true);

        });

        it('there should be an email in the timeline', ( ) => {

            browser.ignoreSynchronization = true;

            caseNav.openTab('timeline');

            browser.sleep(4000);

            expect($('[data-event-type="email/send"]').getText()).toContain('Email bij machtiging - 110');

        });

    });

    afterAll(( ) => {

        browser.ignoreSynchronization = false;

        navigate.to();

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
