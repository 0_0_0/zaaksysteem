import navigate from './../../../functions/common/navigate';
import form from './../../../functions/common/form';
import caseMenu from './../../../functions/intern/caseView/caseMenu';
import startForm from './../../../functions/common/startForm';

const testData = [
    {
        requestor: {
            type: 'citizen',
            id: '1',
            name: 'T. Testpersoon'
        }
    },
    {
        requestor: {
            type: 'citizen',
            id: '13',
            name: 'B. Briefadres'
        }
    },
    {
        requestor: {
            type: 'citizen',
            id: '14',
            name: 'J. Kui'
        }
    },
    {
        requestor: {
            type: 'organisation',
            id: '1',
            name: 'Testbedrijf'
        }
    },
    {
        requestor: {
            type: 'organisation',
            id: '7',
            name: 'Tulsi Trekking'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        },
        recipient: {
            type: 'citizen',
            id: '1',
            name: 'T. Testpersoon'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        },
        recipient: {
            type: 'organisation',
            id: '1',
            name: 'Testbedrijf'
        }
    }
];

for ( const index in testData ) {

    describe(`when registering a case as a ${testData[index].requestor.type}`, ( ) => {

        beforeAll(( ) => {

            navigate.as();

            const data = {
                casetype: 'Basic casetype',
                requestorType: testData[index].requestor.type,
                requestorId: testData[index].requestor.id,
                channelOfContact: 'behandelaar'
            };

            if (testData[index].recipient) {
                data.recipientType = testData[index].recipient.type;
                data.recipientId = testData[index].recipient.id;
            }

            startForm(data);

            form.goNext(2);
        
        });

        it('there should be a case', ( ) => {

            expect($('.case-view').isPresent()).toBe(true);

        });

        it(`the case should have the ${testData[index].requestor.type} as requestor`, ( ) => {

            expect(caseMenu.getSummaryValue('Aanvrager')).toEqual(testData[index].requestor.name);

        });

        if (testData[index].recipient) {
            
            it(`the case should have the ${testData[index].recipient.type} as recipient`, ( ) => {

                expect(caseMenu.getSummaryValue('Ontvanger')).toEqual(testData[index].recipient.name);

            });

        }

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
