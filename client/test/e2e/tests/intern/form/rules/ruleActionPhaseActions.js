import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import form from './../../../../functions/common/form';
import inputDate from './../../../../functions/common/input/inputDate';
import caseNav from './../../../../functions/intern/caseView/caseNav';
import caseMenu from './../../../../functions/intern/caseView/caseMenu';

const choice = $('[data-name="fase_actie_keuze"]');
const date = $('[data-name="fase_actie_datum"]');

describe('when opening a registration form with the phase actions and activating the phase actions', ( ) => {

    beforeAll(( ) => {

        navigate.as();

        const data = {
            casetype: 'Fase actie bij registratie',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Template"]').click();
        choice.$('[value="Email"]').click();
        choice.$('[value="Case"]').click();
        choice.$('[value="Allocation"]').click();
        choice.$('[value="Contact"]').click();
        inputDate(date, '01-01-2017');
        choice.$('[value="Change date of registration"]').click();

        form.goNext();

    });

    it('the allocation should have been set', ( ) => {

        expect(form.getAssignment()).toEqual(['org-unit', '-Frontoffice', 'Behandelaar']);

    });

    describe('and when registaring the case', ( ) => {
    
        beforeAll(( ) => {

            form.assign('me');
    
            form.goNext();
    
        });
    
        // these testscenarios will be added when the document and timeline tabs are upgraded

        // it('the template should have been created', ( ) => {

        // });

        // it('the email should have been sent', ( ) => {

        // });

        xit('the case should have been created', ( ) => {

            caseNav.openTab('relations');

            expect($('.relations-block a').isPresent()).toBe(true);

            caseNav.openTab('phase');

        });

        it('the registration date and target date should have been changed', ( ) => {

            expect(caseMenu.getAboutValue('Registratiedatum')).toEqual('01-01-2017');
            expect(caseMenu.getAboutValue('Streefafhandeldatum')).toEqual('02-01-2017');

        });

        xit('the contact should have been related', ( ) => {

            caseNav.openTab('relations');

            expect($('.related-subjects [href="/betrokkene/12?gm=1&type=natuurlijk_persoon"]').isPresent()).toBe(true);

            caseNav.openTab('phase');

        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
