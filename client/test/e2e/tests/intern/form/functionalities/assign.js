import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import form from './../../../../functions/common/form';
import caseMenu from './../../../../functions/intern/caseView/caseMenu';
import caseNav from './../../../../functions/intern/caseView/caseNav';

const testData = [
    {
        caseType: 'Toewijzing bij registratie geen opties',
        performAssignment: ( ) => {
            form.goNext();
            form.openRegisteredCase();
        },
        results: {
            behandelaar: 'In behandeling nemen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice'
        }
    },
    {
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: ( ) => {
            form.assign('me');
            form.goNext();
        },
        results: {
            behandelaar: 'A. admin',
            coordinator: 'admin',
            status: 'In behandeling',
            afdeling: 'Backoffice'
        }
    },
    {
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: ( ) => {
            form.assign('coworker', { assignee: 'Toewijzing wijzigen' });
            form.goNext();
            form.openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice'
        }
    },
    {
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: ( ) => {
            form.assign('coworker', { assignee: 'Toewijzing wijzigen', changeDepartment: true });
            form.goNext();
            form.openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Zaakacties'
        }
    },
    {
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: ( ) => {
            form.assign('coworker', { assignee: 'Toewijzing wijzigen', sendEmail: true });
            form.goNext();
            form.openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice',
            emailed: true
        }
    }
];

for ( const index in testData ) {

    describe(`when starting a registration form and assigning to ${testData[index].type}`, ( ) => {

        beforeAll(( ) => {

            navigate.as();

            const data = {
                casetype: testData[index].caseType,
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            form.goNext();

            testData[index].performAssignment();

        });

        it('the case should not have an assignee', ( ) => {

            expect(caseMenu.getSummaryValue('Behandelaar')).toEqual(testData[index].results.behandelaar);

        });

        it('the case should not have a coordinator', ( ) => {

            expect(caseMenu.getAboutValue('Coordinator')).toEqual(testData[index].results.coordinator);

        });

        it('the case should have the status "Nieuw"', ( ) => {

            expect(caseMenu.getSummaryValue('Status')).toEqual(testData[index].results.status);

        });

        it('the department should not have changed', ( ) => {

            expect(caseMenu.getSummaryValue('Afdeling')).toEqual(testData[index].results.afdeling);

        });

        if ( testData[index].results.emailed ) {

            it('the assignee should have been emailed', ( ) => {

                browser.ignoreSynchronization = true;

                caseNav.openTab('timeline');

                browser.sleep(4000);

                expect($('[data-event-type="email/send"]').isPresent()).toBe(testData[index].results.emailed);

                browser.get('/intern/');

                browser.ignoreSynchronization = false;

            });

        }

    });
    
}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
