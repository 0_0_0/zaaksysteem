import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import form from './../../../../functions/common/form';
import caseMenu from './../../../../functions/intern/caseView/caseMenu';

const testData = ['Openbaar', 'Intern', 'Vertrouwelijk'];

for ( const index in testData ) {

    describe(`when starting a registration form and setting confidentiality to ${testData[index]}`, ( ) => {

        beforeAll(( ) => {

            navigate.as();

            const data = {
                casetype: 'Wijzig vertrouwelijkheid bij registratie aan',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            form.goNext();

            form.changeConfidentiality(testData[index]);

            form.goNext();

        });

        it('the confidentiality should have been set to ${testData[index]}', ( ) => {

            expect(caseMenu.getSummaryValue('Vertrouwelijkheid')).toEqual(testData[index]);

        });

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
