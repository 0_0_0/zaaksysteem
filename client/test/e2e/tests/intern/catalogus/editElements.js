import navigate from './../../../functions/common/navigate';
import catalogus from './../../../functions/intern/catalogus';
import casetypeManagement from './../../../functions/intern/casetypeManagement';

describe('when opening the catalogus and editing a casetype', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/58');

        catalogus.startEditItem('Test zaaktype to edit', false);

        casetypeManagement.enterName('Test zaaktype edited');

        casetypeManagement.publish();

    });

    it('it should have been edited', ( ) => {

        expect($('.table-closed').getText()).toContain('Test zaaktype edited');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
