import navigate from './../../../functions/common/navigate';
import catalogus from './../../../functions/intern/catalogus';
import casetypeManagement from './../../../functions/intern/casetypeManagement';
import objecttypeManagement from './../../../functions/intern/objecttypeManagement';

describe('when opening the catalogus and creating a folder', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        catalogus.createFolder('Test folder');

    });

    it('the folder should have been created', ( ) => {

        expect($('.breadcrumb-item-label').getText()).toEqual('Test folder');

    });

});

describe('when opening the catalogus and creating an attribute of type text', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test tekstveld',
            type: 'Tekstveld'
        };

        catalogus.createCaseAttribute(data);

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test tekstveld');

    });

});

describe('when opening the catalogus and creating an attribute of type option', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test enkelvoudige keuze',
            type: 'Enkelvoudige keuze',
            options: ['Optie 1', 'Optie 2', 'Optie 3']
        };

        catalogus.createCaseAttribute(data);

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test enkelvoudige keuze');

    });

    it('it should have the correct settings', ( ) => {

        catalogus.startEditItem('Test enkelvoudige keuze');

        expect($('.multiple-options').getText()).toContain('Optie 1');
        expect($('.multiple-options').getText()).toContain('Optie 2');
        expect($('.multiple-options').getText()).toContain('Optie 3');

    });

    afterAll(() => {

        catalogus.closeDialog();

    });


});

describe('when opening the catalogus and creating an email template', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test emailsjabloon',
            subject: 'Onderwerp',
            content: 'Bericht'
        };

        catalogus.createEmailTemplate(data);

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test emailsjabloon');

    });

    it('it should have the correct settings', ( ) => {

        catalogus.startEditItem('Test emailsjabloon', false);

        expect($('[data-ng-model="label"]').getAttribute('value')).toEqual('Test emailsjabloon');
        expect($('[data-ng-model="subject"]').getAttribute('value')).toEqual('Onderwerp');
        expect($('[data-ng-model="message"]').getAttribute('value')).toEqual('Bericht');

    });

    afterAll(() => {

        catalogus.closeModal();

    });

});

describe('when opening the catalogus and creating a document template', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test documentsjabloon',
            document: 'casenumber.odt'
        };

        catalogus.createDocumentTemplate(data);

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test documentsjabloon');

    });

});

describe('when opening the catalogus and creating a casetype', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        catalogus.startCreateCasetype();

        casetypeManagement.enterName('Test zaaktype created');

        casetypeManagement.enterIdentification('1');

        casetypeManagement.enterPrincipleNational('1');

        casetypeManagement.enterTermNational('1');

        casetypeManagement.enterTermService('1');

        casetypeManagement.publish();

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test zaaktype created');

    });

});

describe('when opening the catalogus and creating an objecttype', ( ) => {

    beforeAll(( ) => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        catalogus.startCreateObjecttype();

        objecttypeManagement.enterName('Test objecttype created');

        objecttypeManagement.enterTitle('Test objecttype titel');

        objecttypeManagement.enterCategory('- Create elements');

        objecttypeManagement.addCaseAttribute('omschrijving');

        const data = {
            index: true,
            title: 'Omschrijving',
            required: true
        };

        objecttypeManagement.setCaseAttribute(data);

        objecttypeManagement.publish(3);

        navigate.as('admin', '/beheer/bibliotheek/57');

    });

    it('it should have been created', ( ) => {

        expect($('.table-closed').getText()).toContain('Test objecttype created');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
