import navigate from './../../../functions/common/navigate';
import dashboard from './../../../functions/intern/dashboard';
import actionMenu from './../../../functions/intern/actionMenu';

describe('when viewing the dashboard', ( ) => {

    beforeAll(( ) => {

        navigate.as('dashboardempty');

        actionMenu.resetDashboard();

    });

    it('there should be three widgets', ( ) => {

        let widgets = element.all(by.css('.widget'));

        expect(widgets.count()).toBe(3);

    });

    it('there should be an intake widget with case 44', ( ) => {

        expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);

    });

    it('there should be a my open cases widget with case 47', ( ) => {

        expect($('[data-name="mine"] [href="/intern/zaak/47"]').isPresent()).toBe(true);

    });

    it('there should be a favorite casetype widget with casetype dashboard', ( ) => {

        expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');

    });

    afterAll(( ) => {

        dashboard.deleteWidget('Mijn openstaande zaken');
        dashboard.deleteWidget('Zaakintake');
        dashboard.deleteWidget('Favoriete zaaktypen');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
