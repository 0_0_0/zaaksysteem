import mor from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app and opening an open case and completing it', ( ) => {

    beforeAll(( ) => {

        navigate.as('mor', '/mor/');

        mor.openCase(137);

        let data = [
            {
                name: 'mor beoordeling',
                value: 'Neutraal'
            },
            {
                name: 'mor terugkoppeling',
                value: 'Dit is test tekst'
            }
        ];

        mor.completeCase('Melding niet verwerkt', data);

    });

    it('it should redirect to the home page', ( ) => {

        expect(browser.getCurrentUrl()).toMatch('/mor/zaken/open');

    });

    it('the open caseList should not contain the case', ( ) => {

        expect(mor.listCases(1)).not.toContain(137);

    });

    describe('and when opening the case again', ( ) => {
    
        beforeAll(( ) => {

            browser.get('/mor/zaken/afgehandeld');

            mor.loadMoreCases();
    
            mor.openCase(137);
    
        });
    
        it('the completed values should have been set', ( ) => {

            let data = [
                {
                    name: 'mor beoordeling',
                    value: 'Neutraal'
                },
                {
                    name: 'mor terugkoppeling',
                    value: 'Dit is test tekst'
                },
                {
                    name: 'Resultaat',
                    value: 'Melding niet verwerkt'
                }
            ];
    
            expect(mor.checkValues(data)).toBe(true);
    
        });
    
        afterAll(( ) => {
    
            mor.closeCaseView();
    
        });
    
    });

});

describe('when opening the mor app and opening an open case and initiating the completion', ( ) => {

    beforeAll(( ) => {

        navigate.as('mor', '/mor/');

        mor.openCase(138);

        mor.clickTransitionButton();

    });
    
    it('the results should be displayed', ( ) => {
    
        expect(mor.results.count()).toBe(2);

    });

    it('the button to complete with should be present', ( ) => {

        expect(mor.caseCompleteButton.isPresent()).toBe(true);

    });

    it('the button to complete with should be disabled', ( ) => {

        expect(mor.caseCompleteButton.isEnabled()).toBe(false);

    });

    describe('and when filling out the required attribute', ( ) => {
    
        beforeAll(( ) => {
    
            mor.inputAttribute('mor verplicht *', 'verplichte tekst');
    
        });
    
        it('the button to complete with should still be disabled', ( ) => {

            expect(mor.caseCompleteButton.isEnabled()).toBe(false);

        });
    
        afterAll(( ) => {

            mor.clearAttribute('mor verplicht *');
    
        });
    
    });

    describe('and when picking a result', ( ) => {
    
        beforeAll(( ) => {
    
            mor.inputResult('Melding niet verwerkt');
    
        });

        it('it should receive the active class', ( ) => {

            expect(mor.results.get(1).getAttribute('class')).toContain('active');

        });
    
        it('the button to complete with should still be disabled', ( ) => {

            expect(mor.caseCompleteButton.isEnabled()).toBe(false);

        });
    
    });

    describe('and when filling out the required attribute and picking a result', ( ) => {
    
        beforeAll(( ) => {

            mor.inputAttribute('mor verplicht *', 'verplichte tekst');
    
            mor.inputResult('Melding niet verwerkt');
    
        });
    
        it('the button to complete with should be enabled', ( ) => {

            expect(mor.caseCompleteButton.isEnabled()).toBe(true);

        });

        describe('and when completing', ( ) => {

            beforeAll(( ) => {
        
                mor.clickCompleteButton();
        
            });
        
            it('it should redirect to the home page', ( ) => {

                expect(browser.getCurrentUrl()).toMatch('/mor/zaken/open');

            });

            it('the open caseList should not contain the case', ( ) => {

                expect(mor.listCases(1)).not.toContain(138);

            });
        
        });
    
    });

});

describe('when opening the mor app and opening an open case and trying to complete it without all required attributes visible', ( ) => {

    beforeAll(( ) => {

        navigate.as('mor', '/mor/');

        mor.openCase(139);

        mor.clickTransitionButton();

    });

    it('should display a warning', ( ) => {
        
        expect($('.case-detail-view__field-config-error-message').isDisplayed()).toBe(true);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
