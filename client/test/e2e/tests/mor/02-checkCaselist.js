import mor from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app', ( ) => {

    beforeAll(( ) => {

        navigate.as('mor', '/mor/');

    });

    it('there should be a header', ( ) => {

        expect(mor.header.isPresent()).toBe(true);

    });

    it('the header should contain the configured background image', ( ) => {

        expect(mor.header.$('.mor-nav__bg').getAttribute('style')).toContain('auroraobjects');

    });

    it('the header should contain the configured title', ( ) => {

        expect(mor.getHeaderText()).toEqual('Melding openbare ruimte');

    });

    it('the header should contain two navigation buttons', ( ) => {

        expect(mor.tabs.all(by.css('a')).count()).toEqual(2);

    });

    it('the footer should contain the configured link', ( ) => {

        expect(mor.supportlink.getAttribute('href')).toEqual('https://www.nissan.nl/');

    });

    describe('and when on the open view', ( ) => {
    
        beforeAll(( ) => {
    
            navigate.as('mor', '/mor/zaken/open');
    
        });
    
        it('the view should contain two caseLists', ( ) => {

            expect(mor.countCaselists()).toEqual(2);

        });

        it('the first caseList should contain the right cases', ( ) => {

            let expected = [136, 137, 138, 139];

            expect(mor.listCases(1)).toEqual(expected);

        });

        it('the second caseList should contain the right cases', ( ) => {

            let expected = [132, 133, 134];

            expect(mor.listCases(2)).toEqual(expected);

        });
    
    });

    describe('and when on the closed view', ( ) => {
    
        beforeAll(( ) => {
    
            navigate.as('mor', '/mor/zaken/afgehandeld');
    
        });
    
        it('the view should contain one caseList', ( ) => {

            expect(mor.countCaselists()).toEqual(1);

        });

        it('the caseList should contain the right cases', ( ) => {

            let expected = [150, 149, 148, 147, 146, 145, 144, 143, 142, 141];

            expect(mor.listCases(1)).toEqual(expected);

        });

        it('the view should contain an option to retrieve more cases', ( ) => {

            expect($('.case-group-loadmore button').isPresent()).toBe(true);

        });

        describe('and when loading more cases', ( ) => {

            beforeAll(( ) => {

                mor.loadMoreCases();

            });

            it('the caseList should contain the right cases', ( ) => {

                expect(mor.listCases(1)).toContain(140);

            });

        });
    
    });

    describe('and when entering a searchTerm', ( ) => {
    
        beforeAll(( ) => {
    
            navigate.as('mor', '/mor/zaken/open');

            mor.openSearch();

            mor.enterSearchTerm('aap');
    
        });
    
        it('there should be search results matching the case results', ( ) => {

            let expected = ['aap', 'aap noot', 'aap mies', 'aap noot mies'];

            expect(mor.getSearchResults()).toEqual(expected);

        });

        afterAll(( ) => {

            mor.selectSearchTerm();

            mor.closeSearch();

        });
    
    });

    describe('and when searching with one term', ( ) => {
    
        beforeAll(( ) => {
    
            navigate.as('mor', '/mor/zaken/open');

            mor.search('aap');
    
        });

        it('the first caseList should contain the right cases', ( ) => {

            let expected = [136, 137, 139];

            expect(mor.listCases(1)).toEqual(expected);

        });

        it('the second caseList should contain the right cases', ( ) => {

            let expected = [132, 133];

            expect(mor.listCases(2)).toEqual(expected);

        });

        afterAll(( ) => {

            mor.closeSearch();

        });
    
    });

    describe('and when searching with multiple terms', ( ) => {
    
        beforeAll(( ) => {
    
            navigate.as('mor', '/mor/zaken/open');

            mor.search(['aap', 'mies']);
    
        });

        it('the first caseList should contain the right cases', ( ) => {

            let expected = [137, 139];

            expect(mor.listCases(1)).toEqual(expected);

        });

        it('the second caseList should contain the right cases', ( ) => {

            let expected = [133];

            expect(mor.listCases(2)).toEqual(expected);

        });

        afterAll(( ) => {

            mor.closeSearch();

        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
