import loginPage from './../../../functions/intern/loginPage';

describe('when logging in', ( ) => {

    beforeAll(( ) => {

        loginPage.logout();

    });

    it('should have a form', ( ) => {

        expect(loginPage.loginForm.isPresent()).toBe(true);

    });

    it('should have a username input field', ( ) => {

        expect(loginPage.usernameField.isPresent()).toBe(true);

    });

    it('should have a password input field', ( ) => {

        expect(loginPage.passwordField.isPresent()).toBe(true);

    });

    it('should have a submit button', ( ) => {

        expect(loginPage.submit.isPresent()).toBe(true);

    });

    describe('when logging in with valid credentials', ( ) => {

        beforeAll(( ) => {

            loginPage.login();

        });

        it('should redirect to the application', ( ) => {

            expect(browser.getCurrentUrl()).toMatch(/intern/);

        });

    });

    describe('when logging in with invalid credentials', ( ) => {

        beforeAll(( ) => {

            loginPage.logout();

            loginPage.login('admin', 'wrong password');

        });

        it('should stay on the login page', ( ) => {

            expect(browser.getCurrentUrl()).not.toMatch(/intern/);

        });

        it('should give a warning for the wrong login details', ( ) => {

            expect($('.id_wrong').isPresent()).toBe(true);

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
