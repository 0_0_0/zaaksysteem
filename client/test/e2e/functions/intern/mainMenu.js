let mainMenu = {

    toggle: ( ) => {

        $('.top-bar-menu-button').click();

    },

    toggleNotifications: ( ) => {

        let notificationButton = '[zs-tooltip="Notificaties"]',
            notifications = '.notifications';

        mainMenu.toggle();

        $(notifications).isDisplayed()
            .then(

                isDisplayed => {
                    return isDisplayed ?
                        isDisplayed
                        : Promise.reject();
                },
                ( ) => $(notificationButton).click()
                
            );

    }

};

module.exports = mainMenu;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
