let caseDocuments = {

    checkDocument: ( documentName ) => new Promise((resolve) => {

        const allDocuments = element.all(by.css('.object-type-file'));

        allDocuments.each(aDocument => {
            aDocument.$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)').getText().then(name => {
                if (name === documentName) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }),

    getDocumentDescription: ( aDocument ) => new Promise((resolve) => {

        aDocument.$('.list-view-entity-desc').getText().then(description => {
            resolve(description);
        });

    }),

    getDocumentOrigin: ( aDocument ) => new Promise((resolve) => {

        aDocument.$('.list-view-entity-origin').getText().then(origin => {
            resolve(origin);
        });

    }),

    getDocumentOriginDate: ( aDocument ) => new Promise((resolve) => {

        aDocument.$('.list-view-entity-origin-date').getText().then(originDate => {
            resolve(originDate);
        });

    }),

    checkDocumentSettings: ( documentData ) => new Promise((resolve) => {

        const allDocuments = element.all(by.css('.object-type-file'));

        allDocuments.each(aDocument => {
            aDocument.$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)').getText().then(name => {
                if (name === documentData.documentName) {
                    if ( caseDocuments.getDocumentDescription(aDocument) === documentData.settings.description
                        && caseDocuments.getDocumentOrigin(aDocument) === documentData.settings.origin
                        && caseDocuments.getDocumentOriginDate(aDocument) === documentData.settings.originDate ) {
                        resolve(true);
                    }

                }
            });
        });

    }),

    getDocumentLabels: ( documentName ) => new Promise((resolve) => {

        const allDocuments = element.all(by.css('.object-type-file'));

        allDocuments.each(aDocument => {
            aDocument.$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)').getText().then(name => {
                if (name === documentName) {
                    aDocument.$('.list-view-entity-label').getText().then(label => {
                        resolve(label);
                    });
                }
            });
        });

    })

};

module.exports = caseDocuments;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
