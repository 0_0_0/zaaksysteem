let caseChecklist = {

    create: ( itemText ) => {

        let checklistItemAdd = $('.checklist-item-add input');

        checklistItemAdd.sendKeys(itemText);
        checklistItemAdd.sendKeys(protractor.Key.ENTER);

    },

    delete: ( itemNumber ) => {

        $(`.check-list li:nth-child(${itemNumber}) .sidebar-item-action button`).click();

    },

    toggle: ( itemNumber ) => {

        $(`.check-list li:nth-child(${itemNumber}) input`).click();

    }

};

module.exports = caseChecklist;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
