import select from './../../common/select';
import caseAttribute from './../../common/input/caseAttribute';
import waitForElement from './../../common/waitForElement';

let casePhase = {

    advance: ( ) => {

        $('.phase-advance-button').click();

    },

    getAdvanceButtonText: ( ) => {

        return $('.phase-advance-button-label').getText().then(text => {
            return text.toLowerCase();
        });

    },

    getLockButtonText: ( ) => {

        return $('.phase-header-status button.phase-unlock-button').getText().then(text => {
            return text.toLowerCase();
        });

    },

    checkObjectButtonPresence: ( objectTypeName ) => new Promise((resolve) => {

        const allButtons = element.all(by.css(`[data-name="object.${objectTypeName}"] .case-mutation-add-item button`));
        let classes = [];

        allButtons.each(button => {

            button.getText().then(text => {

                classes.push(text.toLowerCase());

            });

        });

        resolve(classes);

    }),

    startObjectMutation: ( objectTypeName, type ) => {

        const allButtons = element.all(by.css(`[data-name="object.${objectTypeName}"] button`));

        allButtons.each(button => {

            button.getText().then(text => {

                if ( text.toLowerCase() === type ) {

                    button.click();

                }

            });

        });

    },

    performObjectMutationAction: type => {

        let cancel = element.all(by.css('zs-case-object-mutation-form .form-actions button:nth-child(1)')),
            addMutation = element.all(by.css('zs-case-object-mutation-form .form-actions button:nth-child(2)'));

            if ( type === 'cancel' ) {

                cancel.click();

            } else if ( type === 'add' ) {

                addMutation.click();

            }

    },

    createObjectMutationCreate: ( objectTypeName, type, data ) => {

        casePhase.startObjectMutation(objectTypeName, type);

        caseAttribute.inputAttributes(data);

        casePhase.performObjectMutationAction('add');

    },

    createObjectMutationMutate: ( objectTypeName, type, objectName, data ) => {

        casePhase.startObjectMutation(objectTypeName, type);

        select.firstSuggestion($('zs-case-object-mutation-form').$('vorm-field input'), objectName);

        waitForElement('zs-case-object-mutation-form [data-name="objectmutatie_titel"]');

        caseAttribute.inputAttributes(data);

        casePhase.performObjectMutationAction('add');

    },

    createObjectMutationDelete: ( objectTypeName, type, objectName ) => {

        casePhase.startObjectMutation(objectTypeName, type);

        select.firstSuggestion($('zs-case-object-mutation-form').$('vorm-field input'), objectName);

        casePhase.performObjectMutationAction('add');

    },

    editObjectMutation: ( objectTypeName, number, data ) => {

        $(`[data-name="object.${objectTypeName}"] .case-mutation:nth-child(${number}) button.edit`).click();

        caseAttribute.inputAttributes(data);

        casePhase.performObjectMutationAction('add');

    },

    deleteObjectMutation: ( objectTypeName, number ) => {

        $(`[data-name="object.${objectTypeName}"] .case-mutation:nth-child(${number}) button.delete`).click();

        $('.confirm-button').click();

    },

    unlock: ( ) => {

        $('.phase-header-status button.phase-unlock-button').click();

        $('.confirm-button').click();

    },

    lock: ( ) => {

        $('.phase-header-status button.phase-unlock-button').click();

    }

};

module.exports = casePhase;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
