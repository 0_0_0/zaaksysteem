import closeDialog from './../../common/closeDialog';

let caseMenu = {

    openAdditionalInformation: ( ) => {

        $('[data-name="about"]').click();

    },

    getSummaryValue: valueName => {

        return $(`.case-summary-list [zs-tooltip="${valueName}"] .case-summary-list-item-label`).getText();

    },

    getAboutValue: valueName => {

        let aboutInformationNames = element.all(by.css('.case-info-group .case-info-group-field')),
            result,
            fieldValue;

        caseMenu.openAdditionalInformation();

        result = aboutInformationNames.filter((elm) => {
            return elm.$('.case-info-group-field-label').getText().then((text) => {
                return text === `${valueName}`;
            });
        }).first();

        fieldValue = result.$('.case-info-group-field-value').getText();

        closeDialog();

        return fieldValue;

    },

    updateConfidentiality: confidentiality => {

        let confidentialityButton = $('zs-case-summary [data-name="confidentiality"] button'),
        confidentialitySetting = $('.modal-confidentiality'),
        option;

        if ( confidentiality === 'vertrouwelijk' ) {
            option = 3;
        } else if ( confidentiality === 'intern' ) {
            option = 2;
        } else if ( confidentiality === 'openbaar' ) {
            option = 1;
        }

        confidentialityButton.click();

        confidentialitySetting.$(`li:nth-child(${option}) input`).click();

    }

};

module.exports = caseMenu;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
