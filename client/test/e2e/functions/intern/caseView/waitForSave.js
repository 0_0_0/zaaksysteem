export default ( ) => {

    browser.driver.wait( () => {
        return browser.driver.isElementPresent(by.css('.snack-pending')).then( (el) => {
            return el === true;
        }, 30000);
    })
    .then( () => {
        browser.driver.wait( () => {
            return browser.driver.isElementPresent(by.css('.snack-pending')).then( (el) => {
                return el === false;
            });
        }, 30000);
    });

};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
