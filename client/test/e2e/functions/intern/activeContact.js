let activeContact = {

    el: $('zs-active-subject'),

    exit: () => {

        activeContact.el.$('button[zs-tooltip="Deactiveer contact"]').click();

    }

};

module.exports = activeContact;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
