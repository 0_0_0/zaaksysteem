import plusMenu from './plusMenu';

let documentintake = {

    startCaseRegistration: ( documentName ) => {

        const intakeDocuments = element.all(by.css('.object-type-file'));

        intakeDocuments.filter(intakeDocument => {
            return intakeDocument.$('.list-view-entity-name').getText().then(name => {
                return name === documentName;
            });
        }).each(element => {
            element.$('.intake-document-register').click();
        });

    },

    createCase: ( documentName, data ) => {

        documentintake.startCaseRegistration(documentName);

        plusMenu.fillCreateCase(data);

    }

};

module.exports = documentintake;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
