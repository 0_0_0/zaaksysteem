import waitForElement from './../common/waitForElement';
import inputFileSimple from './../common/input/inputFileSimple';

let catalogus = {

    dialog: $('.ui-dialog'),

    toggleActionMenu: ( ) => {

        $('.block-header-actions').click();

    },

    waitForDialog: ( ) => {

        waitForElement('.ui-dialog');

    },

    openAction: ( action, aDialogAction = true ) => {

        catalogus.toggleActionMenu();

        const options = element.all(by.css('.block-header-actions .buttons a'));

        options.filter(option => {
            return option.getText().then(text => {
                return text === action;
            });
        }).click();

        if (aDialogAction) {
            catalogus.waitForDialog();
        }

    },

    closeDialog: ( ) => {

        $('.ui-dialog .ui-dialog-titlebar-close .ui-icon-closethick').click();

    },

    closeModal: ( ) => {

        $('.modal button.modal-close .mdi-close').click();

    },

    submitAction: ( ) => {

        catalogus.dialog.$('[type="submit"]').click();

        waitForElement('.system-message-current');

    },

    createFolder: ( title ) => {

        catalogus.openAction('Categorie');

        catalogus.dialog.$('[name="naam"]').sendKeys(title);

        catalogus.submitAction();

    },

    waitForMagicstring: ( ) => {

        browser.driver.wait( () => {
            return $('[name="kenmerk_magic_string"]').getAttribute('value').then( (text) => {
                return text !== '';
            });
        });

    },

    createCaseAttribute: ( data ) => {

        catalogus.openAction('Kenmerk');

        $('[name="kenmerk_naam"]').sendKeys(data.name);

        $('[name="kenmerk_type"]').sendKeys(data.type);

        catalogus.waitForMagicstring();

        if (data.options) {

            data.options.forEach(option => {

                $('.multiple-options [name="addopt"] textarea').sendKeys(option);

                $('.multiple-options [name="addopt"] button').click();

            });

        }

        catalogus.submitAction();

    },

    createEmailTemplate: ( data ) => {

        catalogus.openAction('E-mail', false);

        $('[data-ng-model="label"]').sendKeys(data.name);

        $('[data-ng-model="subject"]').sendKeys(data.subject);

        $$('[data-ng-model="message"]').sendKeys(data.content);

        $('[name="emailTemplateForm"] .form-actions button').click();
        
    },

    createDocumentTemplate: ( data ) => {

        catalogus.openAction('Sjabloon');

        $('[name="naam"]').sendKeys(data.name);

        inputFileSimple($('[type="file"]'), 'casenumber.odt');

        catalogus.submitAction();
        
    },

    startCreateCasetype: ( ) => {

        catalogus.openAction('Zaaktype', false);
        
    },

    startCreateObjecttype: ( ) => {

        catalogus.openAction('Objecttype', false);
        
    },

    startItemAction: (name, action, aDialogAction = true) => {

        const rows = element.all(by.css('.table-closed tr'));

        rows.filter(row => {
            return row.getText().then(text => {
                return text.indexOf(name) !== -1;
            });
        }).each(row => {
            row.$('.ezra_dropdown_init').click();

            const itemActions = row.all(by.css('.ezra_actie_container a'));

            itemActions.filter(itemAction => {
                return itemAction.getText().then(text => {
                    return text === action;
                });
            }).click();

        });

        if (aDialogAction) {
            catalogus.waitForDialog();
        }

    },

    startEditItem: ( name, aDialogAction = true ) => {

        catalogus.startItemAction(name, 'Bewerken', aDialogAction);

    },

    deleteItem: ( name, reason = 'Reason' ) => {

        catalogus.startItemAction(name, 'Verwijderen');

        $('[name="commit-message"]').sendKeys(reason);

        catalogus.submitAction();

    }

};

module.exports = catalogus;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
