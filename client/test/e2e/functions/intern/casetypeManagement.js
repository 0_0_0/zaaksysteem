import waitForElement from './../common/waitForElement';

let casetypeManagement = {

    enterBasicAttribute: ( attribute, value ) => {

        $(`[name="${attribute}"]`).clear().sendKeys(value);

    },

    enterName: ( name ) => {

        casetypeManagement.enterBasicAttribute('node.titel', name);

    },

    enterIdentification: ( id ) => {

        casetypeManagement.enterBasicAttribute('node.code', id);

    },

    enterPrincipleNational: ( principle ) => {

        casetypeManagement.enterBasicAttribute('definitie.grondslag', principle);

    },

    enterTermNational: ( term ) => {

        casetypeManagement.enterBasicAttribute('definitie.afhandeltermijn', term);

    },

    enterTermService: ( term ) => {

        casetypeManagement.enterBasicAttribute('definitie.servicenorm', term);

    },

    goTo: ( tabName ) => {

        const tabs = element.all(by.css('.voortgang a'));

        tabs.filter(tab => {
            return tab.getText().then(text => {
                return text === tabName;
            });
        }).click();

    },

    publish: ( ) => {

        casetypeManagement.goTo('afronden');

        waitForElement('[value="basisattributen"]');

        $('[value="basisattributen"]').click();

        $('[value="Publiceren"]').click();

    }

};

module.exports = casetypeManagement;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
