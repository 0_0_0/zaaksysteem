let objectView = {

    getTitle: ( ) => {

        return $('.object-view-header-label').getText();

    },

    getValue: attributeName => new Promise((resolve) => {

        const attributes = element.all(by.css('.object-field-list .object-field'));

        attributes.each(attribute => {

            attribute.$('.object-field-label').getText().then(label => {

                if ( label === attributeName ) {

                    resolve(attribute.$('.object-field-value').getText());

                }

            });

        });

    }),

    getValues: data => {

        return data.map(label => {

            return objectView.getValue(label);

        });

    }


};

module.exports = objectView;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
