let universalSearch = {

    el: $('zs-universal-search'),

    activate: () => {

        universalSearch.el.$('input').click();

    },

    activateByKey: type => {

        if ( type === 'basic' ) {

            browser.actions()
                .keyDown(protractor.Key.CONTROL)
                .sendKeys('/')
                .keyDown(protractor.Key.CONTROL)
                .perform();

        } else if ( type === 'actions' ) {

            browser.actions()
                .keyDown(protractor.Key.CONTROL)
                .keyDown(protractor.Key.SHIFT)
                .sendKeys('/')
                .keyDown(protractor.Key.CONTROL)
                .keyDown(protractor.Key.SHIFT)
                .perform();

        }

    },

    search: input => {

        universalSearch.activate();

        universalSearch.el.$('input').sendKeys(input);

        browser.sleep(1000);

    },

    exit: ( ) => {

        universalSearch.el.$('.spot-enlighter-back-button').click();

    },

    openResult: title => {

        let results = universalSearch.el.all(by.css('.suggestion-list-item'));

        results.filter(elem => {
            return elem.$('.suggestion-text-title').getText().then(text => {
                return text === title;
            });
        }).click();

    },

    getMoreResults: ( ) => {

        universalSearch.el.$('.suggestion-list-group-more-button').click();

    },

    activateContact: title => {

        let results = universalSearch.el.all(by.css('.suggestion-list-item'));

        results.each(result => {

            result.$('.suggestion-text-title').getText().then(text => {

                if (text === title) {

                    result.$('.suggestion-actions button').click();

                }

            });

        });

    },

    countResults: ( ) => new Promise((resolve) => {

        const suggestions = universalSearch.el.all(by.css('.suggestion-list-item'));

        resolve(suggestions.count());

    }),

    getResults: () => new Promise((resolve) => {

        let results = universalSearch.el.all(by.css('.suggestion-list-item')),
            resultArray = [];

        results.each(result => {

            const resultAttributes = {};

            result.$('.suggestion-text-title').getText().then(text => {

                resultAttributes.name = text;

            });

            result.$('.suggestion > zs-icon').getAttribute('icon-type').then(iconType => {

                resultAttributes.icon = iconType;

            });

            result.$('a').getAttribute('href').then(link => {

                let path = link ? link.replace(browser.baseUrl, '') : undefined;

                resultAttributes.link = path;

            });

            resultArray.push(resultAttributes);

        });

        resolve(resultArray);

    }),

    openFilterMenu: ( ) => {

        $('zs-dropdown-menu button').click();

    },

    selectFilter: filter => {

        let filters = universalSearch.el.$('zs-dropdown-menu').all(by.css('.popup-menu-list-item'));

        universalSearch.openFilterMenu();
        
        filters.filter(elem => {
            return elem.getText().then(text => {
                return text === filter;
            });
        }).click();

    }

};

module.exports = universalSearch;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
