import select from './../common/select';
import mouse from './../common/mouse';
import repeat from './../common/repeat';

let dashboard = {

    resizeWidget: (widgetTitle, corner, x, y) => {

        //corner should be nw, ne, sw, se

        let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
            cornerHandle = widget.$(`.handle-${corner}`);

        mouse.over(widget.$('.handle-se'));

        browser.driver.actions()
            .dragAndDrop(cornerHandle, { x, y })
            .mouseUp()
            .perform();

        },

    moveWidget: (widgetTitle, x, y) => {

        browser.driver.actions()
            .dragAndDrop(element(by.cssContainingText('.widget-header-title', widgetTitle)), { x, y })
            .mouseUp()
            .perform();

    },

    deleteWidget: (widgetTitle) => {

        element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')).$('.widget-header-remove-button').click();

    },

    acceptCase: (caseNumber) => {

        let caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`);

        caseRow.$('.case-intake-action-take-on').click();

    },

    refuseCase: (caseNumber) => {

        let caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`),
            confirmButton = $('.zs-modal-body .confirm-button');

        caseRow.$('.case-intake-action-reject').click();

        confirmButton.click();

    },

    addFavoriteCasetype: (casetype) => {

        select.firstSuggestion($('.widget-favorite-suggest input'), casetype);

        browser.sleep(1000);

    },

    removeFavoriteCasetype: (casetype) => {

        let itemToRemove = element(by.cssContainingText('.widget-favorite-link', casetype)).element(by.xpath('..'));

        itemToRemove.$('.widget-favorite-remove').click();

    },

    startFavoriteCasetype: (casetype) => {

        element(by.cssContainingText('.widget-favorite-link', casetype)).click();

    },

    filterWidget: (widgetTitle, text) => {

        let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
        widgetFilterButton = widget.$('.widget-header-filter button'),
        widgetFilter = widget.$('.widget-header-filter input');

        widgetFilterButton.click();

        widgetFilter.sendKeys(text);

    },

    navigateWidgetPages: (widgetTitle, navType, numberOfClicks = 1) => {

        let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
            widgetPaginationButton = widget.$(`.pagination-buttons .pagination-button-${navType}`);

        repeat(numberOfClicks)(() =>
            widgetPaginationButton.click()
        );

    },

    setWidgetMaxResults: (widgetTitle, numberOfPages) => {

        let widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')),
        widgetPaging = widget.$('.pagination select');

        widgetPaging.sendKeys(numberOfPages);

    },

    getWidgetCases: (widgetTitle) => new Promise((resolve) => {

        const casesInWidget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')).all(by.css('.zs-table-row'));
        let caseNumbers = [];

        casesInWidget.each(caseInWidget => {

            caseInWidget.$('[column-id="case.number"]').getText().then((caseNumber) => {
                caseNumbers.push(caseNumber);
            });

        });

        resolve(caseNumbers);

    }),

    getWidgetCasetypes: ( ) => new Promise((resolve) => {

        const casetypesInWidget = element.all(by.css('[data-name=""] .widget-favorite-link'));
        let casetypeNames = [];

        casetypesInWidget.each(casetypeInWidget => {

            casetypeInWidget.getText().then((casetypeName) => {
                casetypeNames.push(casetypeName);
            });

        });

        resolve(casetypeNames);

    }),

    getIntakeIconType: (caseNumber) => {

        return $(`.table-row[href="/intern/zaak/${caseNumber}"] zs-case-intake-action-list > zs-icon`).getAttribute('icon-type');

    }

};

module.exports = dashboard;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
