import e2epasswords from './../../e2epasswords.json';

let loginPage = {

    loginForm: $('#loginwrap form'),
    usernameField: $('#id_username'),
    passwordField: $('#id_password'),
    submit: $('form input[type=submit]'),

    getPassword: (username) => {

        return e2epasswords[username];

    },

    logout: () => {

        browser.get('/auth/logout');

    },

    login: (username = 'admin', wrongPassword) => {

        let password = wrongPassword ? wrongPassword : loginPage.getPassword(username);

        loginPage.usernameField.sendKeys(username);
        loginPage.passwordField.sendKeys(password);
        loginPage.submit.click();

    }

};

module.exports = loginPage;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
