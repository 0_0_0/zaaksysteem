import select from './../common/select';
import repeat from './../common/repeat';

let objecttypeManagement = {

    enterName: ( name ) => {

        $('[name="name"]').sendKeys(name);

    },

    enterTitle: ( title ) => {

        $('[name="title_template"]').sendKeys(title);

    },

    enterCategory: ( category ) => {

        $('[name="category_id"]').sendKeys(category);

    },

    addCaseAttribute: ( attributeName ) => {

        const elementToSearchIn = $('.form-field-name-attributes .spot-enlighter-wrapper input');
        const results = element.all(by.css('.spot-enlighter-menu li'));

        select.byText(elementToSearchIn, attributeName, results, attributeName);

    },

    setCaseAttribute: ( data ) => {

        if (data.index) {
            $('.object-type-attribute-list-item-cell-index input').click();
        }

        if (data.title) {
            $('.object-type-attribute-list-item-cell-title input').sendKeys(data.title);
        }

        if (data.required) {
            $('.object-type-attribute-list-item-cell-required input').click();
        }

    },

    goNext: ( type ) => {

        const nextButtons = element.all(by.css('.object-type-edit-actions-continue button'));

        if (type === 'publish') {
            nextButtons.get(1).click();
        } else {
            nextButtons.get(0).click();
        }

    },

    publish: ( forwardClicks ) => {

        repeat(forwardClicks)(() =>
            objecttypeManagement.goNext()
        );

        const componentsCheckboxes = element.all(by.css('.checkbox-list li input'));

        componentsCheckboxes.get(0).click();

        $('[name="modification_rationale"]').sendKeys('Reason');

        objecttypeManagement.goNext('publish');

    }

};

module.exports = objecttypeManagement;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
