let contactView = {

    openContactInfo: ( ) => {

        $('[id="ui-id-12"]').click();

    },

    getEmailAddress: ( ) => {

        return $('[name="npc-email"]').getAttribute('value');

    },

    getPhoneNumber: ( ) => {

        return $('[name="npc-telefoonnummer"]').getAttribute('value');
        
    },

    getMobileNumber: ( ) => {

        return $('[name="npc-mobiel"]').getAttribute('value');
        
    }

};

module.exports = contactView;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
