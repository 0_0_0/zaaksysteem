import waitForElement from './../common/waitForElement';
import mouse from './../common/mouse';

let mor = {

    header: $('mor-nav'),
    tabs: $('.mor-nav__tabs'),
    activeTab: $('mor-app ui-view'),
    buttonBar: $('.mor-nav__button-bar'),
    searchUnit: $('.mor-nav__search'),
    results: element.all(by.css('[data-name="result"] vorm-radio-group label')),
    supportlink: $('.case-list-view__supportlink a'),
    caseGroup: element.all(by.css('mor-app ui-view .case-group')),
    caseViewButton: $('case-detail-view .case-detail-view__button-container button'),
    caseCompleteButton: $('case-complete-view .case-detail-view__button-container button'),
    loadMore: $('.case-group-loadmore button'),

    getHeaderText: ( ) => {

        return mor.header.$('.mor-nav__title h1').getText();

    },

    openSearch: ( ) => {

        mor.buttonBar.$('[data-name="search"]').click();

    },

    enterSearchTerm: searchTerm => {

        mor.searchUnit.$('input').sendKeys(searchTerm);

    },

    selectSearchTerm: ( ) => {

        element.all(by.css('zs-suggestion-list .suggestion')).first().click();

    },

    getSearchResults: ( ) => new Promise((resolve) => {

        const searchResultItems = element.all(by.css('zs-suggestion-list .suggestion'));
        const searchResults = [];

        searchResultItems.each(searchResultItem => {

            searchResultItem.getText().then(searchResult => {

                searchResults.push(searchResult);

            });

        });

        resolve(searchResults);

    }),

    search: searchTerm => {

        let searchTerms = searchTerm.constructor === Array ? searchTerm : [ searchTerm ];

        mor.openSearch();

        for ( const term in searchTerms ) {

            mor.enterSearchTerm(searchTerms[term]);

            mor.selectSearchTerm();
            
        }

    },

    closeSearch: ( ) => {

        mor.searchUnit.$('.mor-nav__clearbutton').click();

    },

    logout: ( ) => {

        mor.buttonBar.$('[data-name="logout"]').click();

    },

    openView: view => {

        mor.tabs.$(`a[href="/mor/zaken/${view}"]`).click();

    },

    countCaselists: ( ) => {

        return mor.activeTab.all(by.css('.case-group')).count();
        
    },

    listCases: ( view = 1 ) => new Promise((resolve) => {

        const caseItems = mor.caseGroup.get(view - 1).all(by.css('case-list-item-list-item'));
        const cases = [];

        caseItems.each(caseItem => {

            caseItem.$('a').getAttribute('href').then(href => {

                let caseNumber = href.substring(href.indexOf('!') + 9, href.length - 1);

                cases.push(parseInt(caseNumber, 10));

            });


        });

        resolve(cases);

    }),

    loadMoreCases: ( ) => {

        mouse.scroll(0, 850);

        mor.loadMore.click();

    },

    openCase: caseNumber => {

        $(`case-list-item-list-item a[href*="/!melding/${caseNumber}/"]`).click();

        waitForElement('.case-detail-view__body');

    },

    clickTransitionButton: ( ) => {

        mor.caseViewButton.click();

    },

    clickCompleteButton: ( ) => {

        mor.caseCompleteButton.click();

    },

    getTransitionButtonText: ( ) => {

        return mor.caseViewButton.getText().then(text => {

            return text.toLowerCase();

        });

    },

    inputAttribute: ( name, value ) => {

        let attribute = $(`vorm-field[data-label="${name}"]`);

        attribute.getAttribute('data-type').then(dataType => {

            switch (dataType) {

                case 'option':
                attribute.$(`input[value="${value}"]`).click();
                break;

                case 'text':
                attribute.$('input').sendKeys(value);
                break;

                case 'textarea':
                attribute.$('textarea').sendKeys(value);
                break;

                default:
                break;

            }

        });

    },

    clearAttribute: ( name ) => {

        let attribute = $(`vorm-field[data-label="${name}"]`);

        attribute.getAttribute('data-type').then(dataType => {

            switch (dataType) {

                case 'option':
                //impossible
                break;

                case 'text':
                attribute.$('input').clear();
                break;

                case 'textarea':
                attribute.$('textarea').clear();
                break;

                default:
                break;

            }

        });

    },

    inputAttributes: data => {

        for ( const index in data ) {

            mor.inputAttribute(data[index].name, data[index].value);

        }

    },

    inputResult: result => {

        let resultOptions = element.all(by.css('[data-name="result"] .vorm-radio-option'));

        resultOptions.each(resultOption => {

            resultOption.$('span').getText().then(text => {

                if ( text === result ) {

                    resultOption.$('input').click();

                }

            });

        });

    },

    completeCase: ( result, data ) => {

        mor.clickTransitionButton();

        mor.inputAttributes(data);

        mor.inputResult(result);

        mor.clickCompleteButton();

    },

    checkValue: ( name, expectedValue ) => new Promise((resolve) => {
        let attributes = element.all(by.css('.case-detail-view__body > .case-detail-view__list .case-detail-view__item'));

        attributes.each(attribute => {

            attribute.$('.case-detail-view__label').getText().then(text => {

                if ( text === name ) {

                    attribute.$('.case-detail-view__value').getText().then(value => {

                        resolve( value === expectedValue );

                    });

                }

            });

        });

    }),

    checkValues: data => new Promise((resolve) => {

        for ( const index in data ) {

            mor.checkValue(data[index].name, data[index].value).then(value => {

                if ( !value ) {

                    resolve(value);

                } else if ( index + 1 === data.length ) {

                    resolve(value);

                }

            });

        }

    }),

    closeCaseView: ( ) => {

        $('.case-detail-view__header .mdi-arrow-left').click();

    },

    closeCaseCompleteView: ( ) => {

        $('.casecomplete-modal .case-detail-view__header .mdi-arrow-left').click();

    }

};

module.exports = mor;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
