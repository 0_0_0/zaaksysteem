let xhrRequests = {

    getUsername: ( ) => {

        return browser.driver.executeAsyncScript(function() {
            
            let callback = arguments[arguments.length - 1],
                xhr = new XMLHttpRequest();

            xhr.open('GET', '/api/v1/session/current', true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    callback(xhr.responseText);
                }
            };
            xhr.send('');

        }).then(str => {

            let displayname = JSON.parse(str).result.instance.logged_in_user === null ? undefined : JSON.parse(str).result.instance.logged_in_user.display_name.toLowerCase();

            return displayname;

        });

    }

};

module.exports = xhrRequests;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
