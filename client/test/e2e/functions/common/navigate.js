import loginPage from './../intern/loginPage';
import xhrRequests from './auth/xhrRequests';

let nav = {

    as: ( username = 'admin', destination = '/intern/', wrongPassword ) => {

        xhrRequests.getUsername().then(loggedInUser => {

            browser.getCurrentUrl().then(locationFull => {

                let location = locationFull.replace('https://testbase-instance.dev.zaaksysteem.nl', '');

                if ( (loggedInUser !== username && loggedInUser !== undefined) || wrongPassword ) {

                    loginPage.logout();

                }

                if ( (loggedInUser !== username || loggedInUser === undefined) || wrongPassword ) {

                    if ( destination !== '/intern/') {

                        nav.to(destination);

                    }

                    loginPage.login(username, wrongPassword);

                } else if ( destination !== location ) {

                    nav.to(destination);

                }

            });

        });

    },

    to: ( desiredDestination = '/intern/' ) => {

        let destination = Number.isInteger(desiredDestination) ? `/intern/zaak/${desiredDestination}` : desiredDestination;

        browser.get(destination).catch(() => {

            return browser.switchTo().alert().then(alert => {

                alert.accept();

                return browser.get(destination);

            });

        });

    },

    logout: ( ) => {

        xhrRequests.getUsername().then(loggedInUser => {

            if ( loggedInUser !== undefined ) {

                loginPage.logout();

            }

        });

    }

};

module.exports = nav;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
