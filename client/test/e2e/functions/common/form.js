import select from './select';
import repeat from './repeat';
import inputDate from './input/inputDate';

let form = {

    registrationForm: $('zs-case-registration'),
    skipOption: $('.case-register-ignore-required-fields'),
    reuseValuesBar: $('zs-case-reuse-values'),
    assignSelf: $('[data-name="me"]'),
    assignCoworker: $('[data-name="coworker"]'),
    assignDepartment: $('[data-name="org-unit"]'),
    requestorEmailAddress: $('[data-name="$email"]'),
    requestorPhoneNumber: $('[data-name="$landline"]'),
    requestorMobileNumber: $('[data-name="$mobile"]'),

    navigate: direction => {

        $(`button[data-name="${direction}"]`).click();

    },

    goNext: ( times = 1 ) => {

        repeat(times, () => form.navigate('next') );

    },

    goBack: ( times = 1 ) => {

        repeat(times, () => form.navigate('previous') );

    },

    openRegisteredCase: ( ) => {

        $('zs-snackbar a').click();

    },

    assign: ( type, data ) => {

        $(`vorm-allocation [data-name="${type}"]`).click();

        switch (type) {

            case 'me':
            break;

            case 'coworker':
                select.firstSuggestion(form.registrationForm.$('[data-name="coworker"] .suggestion-input'), data.assignee);
                if ( data.changeDepartment ) {
                    form.registrationForm.$('[data-name="change_department"] input').click();
                }
                if ( data.sendEmail ) {
                    form.registrationForm.$('[data-name="inform_assignee"] input').click();
                }
                break;

            case 'org-unit':
                if ( data.department ) {
                    form.registrationForm.$('[data-name="department-picker"] select').sendKeys(data.department);
                }
                if ( data.role ) {
                    form.registrationForm.$('[data-name="role-picker"] select').sendKeys(data.role);
                }
                break;

            default:
                break;

        }

    },

    getAssignmentType: ( ) => new Promise((resolve) => {

        let assignmentOptions = element.all(by.css('.allocation-picker-option'));

        assignmentOptions.filter(assignmentOption => {
            return assignmentOption.getAttribute('class').then(attributeClass => {
                return attributeClass.includes('selected');
            });
        }).each(element => {
            element.getAttribute('data-name').then(dataName => {
                resolve(dataName);
            });
        });

    }),

    getAssignmentDepartment: ( ) => new Promise((resolve) => {

        let assignmentDepartment = $('vorm-allocation [data-name="department-picker"] select');

        assignmentDepartment.$('option:checked').getText().then(department => {
            resolve(department);
        });

    }),

    getAssignmentRole: ( ) => new Promise((resolve) => {

        let assignmentRole = $('vorm-allocation [data-name="role-picker"] select');

        assignmentRole.$('option:checked').getText().then(role => {
            resolve(role);
        });

    }),

    getAssignment: ( ) => {

        return Promise.all([
            form.getAssignmentType(),
            form.getAssignmentDepartment(),
            form.getAssignmentRole()
        ]);

    },

    addSubject: data => {

        let relationForm = $('zs-vorm-related-subject-form form');

        $('[data-name="related-subject-add"]').click();

        if ( data.subjectType ) {

            let subjectType = data.subjectType === 'organisation' ? 'bedrijf' : 'natuurlijk_persoon';

            relationForm.$(`[data-name="relation_type"] input[value="${subjectType}"]`).click();

        }

        select.firstSuggestion(relationForm.$('[data-name="related_subject"] input'), data.subjectToRelate);

        if ( data.role ) {

            relationForm.$('[data-name="related_subject_role"] select').sendKeys(data.role);

        }

        if ( data.roleOther ) {

            relationForm.$('[data-name="related_subject_role_freeform"] input').sendKeys(data.roleOther);

        }

        if ( data.authorisation ) {

            relationForm.$('[data-name="pip_authorized"] input').click();

        }

        if ( data.notification ) {

            relationForm.$('[data-name="notify_subject"] input').click();

        }

        relationForm.submit();

    },

    changeConfidentiality: confidentiality => {

        form.registrationForm.$('[data-name="$confidentiality"] select').sendKeys(confidentiality);

    },

    getRequestorEmailAddress: ( ) => {

        return form.requestorEmailAddress.$('input').getAttribute('value');

    },

    getRequestorPhoneNumber: ( ) => {

        return form.requestorPhoneNumber.$('input').getAttribute('value');

    },

    getRequestorMobileNumber: ( ) => {

        return form.requestorMobileNumber.$('input').getAttribute('value');

    },

    changeRequestorEmailAddress: emailAddress => {

        form.requestorEmailAddress.$('input').clear().sendKeys(emailAddress);

    },

    changeRequestorPhoneNumber: phoneNumber => {

        form.requestorPhoneNumber.$('input').clear().sendKeys(phoneNumber);

    },

    changeRequestorMobileNumber: mobileNumber => {

        form.requestorMobileNumber.$('input').clear().sendKeys(mobileNumber);

    },

    reuseValues: ( ) => {

        form.reuseValuesBar.$('button').click();

    },

    setDocumentintakeSettings: ( settings ) => {

        if ( settings.documentName ) {

            $('[step-name="file"] [data-name="name"] input').clear().sendKeys(settings.documentName);

        }

        if ( settings.description ) {

            $('[step-name="file"] [data-name="description"] input').clear().sendKeys(settings.description);

        }

        if ( settings.origin ) {

            $('[step-name="file"] [data-name="origin"] select').sendKeys(settings.origin);

        }

        if ( settings.originDate ) {

            inputDate($('[step-name="file"] [data-name="origin_date"]'), settings.originDate);

        }

    },

    setDocumentintakeCasedocument: ( caseDocument ) => {

        $('[step-name="file"] [data-name="case_document"] select').sendKeys(caseDocument);

    }

};

module.exports = form;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
