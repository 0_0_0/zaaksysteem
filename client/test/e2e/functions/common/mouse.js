let mouse = {

    out: ( ) => {

        browser.actions()
            .mouseMove(element(by.css('body')), { x: -1, y: -1 })
            .perform();

    },

    over: element => {

        browser.actions()
            .mouseMove(element, { x: 0, y: 0 })
            .perform();

        browser.actions()
            .mouseMove(element, { x: 1, y: 1 })
            .perform();

    },

    overCreateButton: ( ) => {

        mouse.over($('.top-bar-create-case'));

    },

    scroll: ( x, y ) => {

        browser.executeScript(`window.scrollTo(${x},${y});`);

    }

};

module.exports = mouse;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
