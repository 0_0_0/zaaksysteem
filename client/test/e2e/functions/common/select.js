let select = {

    byText: ( elementToSearchIn, searchValue, resultElements, expectedResult ) => {

        elementToSearchIn.sendKeys(searchValue);

        resultElements.filter(result => {
            return result.getText().then(text => {
                return text === expectedResult;
            });
        }).click();

    },

    first: ( elementToSearchIn, searchValue, resultElements ) => {

        elementToSearchIn.sendKeys(searchValue);

        resultElements.get(0).click();

    },

    firstSuggestion: ( elementToSearchIn, searchValue ) => {

        const resultElements = element.all(by.css('.suggestion-list-item'));

        select.first(elementToSearchIn, searchValue, resultElements);

    }

};

module.exports = select;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
