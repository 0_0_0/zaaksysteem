import random from 'lodash/random';
import select from './../select';
import inputDate from './inputDate';
import inputFile from './inputFile';

let caseAttribute = {

    getAttributeType: attribute => {

        return attribute.$('vorm-field').getAttribute('class').then((attributeClass) => {

            return attribute.$('vorm-field').getAttribute('data-type').then((dataType) => {

                return attribute.$('vorm-field').getAttribute('data-multiple').then((multiple) => {

                    let multipleType = multiple === '1' ? '' : 'multiple',
                        type = dataType ? `${dataType}${multipleType}` : attributeClass.replace(/ng-isolate-scope|attribute-type-|object-suggest-multiple|empty|valid| /g, '');

                    return type;

                });

            });

        });

    },

    clearAttribute: attribute => {

        caseAttribute.getAttributeType(attribute).then(attributeType => {

            switch ( attributeType ) {

                case 'bag_adres':
                case 'bag_openbareruimte':
                case 'bag_straat_adres':
                case 'bag_adressenmultiple':
                case 'bag_openbareruimtesmultiple':
                case 'bag_straat_adressenmultiple':
                case 'geolatlon':
                case 'googlemaps':
                case 'bankaccount':
                case 'date':
                case 'email':
                case 'image_from_url':
                case 'text':
                case 'text_uc':
                case 'numeric':
                case 'url':
                case 'valuta':
                case 'valutaex':
                case 'valutaex21':
                case 'valutaex6':
                case 'valutain':
                case 'valutain21':
                case 'valutain6':
                case 'numericmultiple':
                case 'textmultiple':
                case 'text_ucmultiple':
                case 'option':
                case 'richtext':
                case 'select':
                case 'selectmultiple':
                case 'textarea':
                case 'textareamultiple': {

                    let clearButtons = attribute.all(by.css('.mdi-close'));

                    clearButtons.count().then(count => {

                        browser.actions()
                            .mouseMove(attribute, { x: 1, y: 1 })
                            .perform();

                        for (let i = count; i > 0; i-- ) {

                            clearButtons.get(i - 1).click();

                        }

                    });

                    break;
                }

                case 'filemultiple': {

                    let clearButtons = attribute.all(by.css('.value-item .mdi-close'));

                    clearButtons.count().then(count => {

                        browser.actions()
                            .mouseMove(attribute, { x: 1, y: 1 })
                            .perform();

                        for (let i = count - 1; i > 0; i-- ) {

                            clearButtons.get(i - 1).click();

                        }

                    });

                    break;
                }

                case 'checkbox': {
                    let checkedBoxes = attribute.all(by.css('input:checked'));

                    checkedBoxes.count().then(count => {

                        for (let i = count; i > 0; i-- ) {

                            checkedBoxes.get(i - i).click();

                        }

                    });

                    break;
                }

                default:
                break;

            }

        });

    },

    inputAttribute: ( attribute, input, clear = false ) => {

        if ( clear ) {
            caseAttribute.clearAttribute(attribute);
        }
        
        caseAttribute.getAttributeType(attribute).then(attributeType => {

            switch (attributeType) {

                case 'bag_adres':
                case 'bag_openbareruimte':
                case 'bag_straat_adres':
                case 'bag_adressenmultiple':
                case 'bag_openbareruimtesmultiple':
                case 'bag_straat_adressenmultiple': {
                    const valuesToInput = input && input.constructor === Array ? input : [ input ];

                    for (const index of valuesToInput) {
                        select.firstSuggestion(attribute.$('input'), index);
                    }
                    break;
                }

                case 'bankaccount': {
                    let inputIban = input ? input : 'GB82WEST12345698765432';

                    attribute.$('input').sendKeys(inputIban);
                
                    break;
                }

                case 'checkbox': {
                    const checkboxesToInput = input && input.constructor === Array ? input : [ input ];
                    const checkboxes = attribute.all(by.css('[type="checkbox"]'));

                    if ( input ) {

                        for ( const index in checkboxesToInput ) {

                            attribute.$(`label:nth-child(${checkboxesToInput[index]}) [type="checkbox"]`).click();

                        }

                    } else {

                        checkboxes.each(checkbox => {

                            const boolean = random(0, 1);

                            if ( boolean ) {

                                checkbox.click();

                            }

                        });

                    }

                    break;
                }

                case 'date': {
                    inputDate(attribute, input);

                    break;
                }

                case 'email': {
                    let inputEmail = input ? input : 'test@test.nl';

                    attribute.$('input').sendKeys(inputEmail);

                    break;
                }

                case 'filemultiple': {
                    inputFile(attribute, input);

                    break;
                }

                case 'geolatlon':
                case 'googlemaps': {
                    let inputAddress = input ? input : 'straat',
                        results = attribute.all(by.css('.suggestion-list button'));

                    attribute.$('input').sendKeys(inputAddress);

                    browser.waitForAngular();

                    results.first().click();
                
                    break;
                }

                case 'image_from_url': {
                    let inputImageUrl = input ? input : 'https://wiki.zaaksysteem.nl/skins/common/images/bussum.png';

                    attribute.$('input').sendKeys(inputImageUrl);
                
                    break;
                }

                case 'numeric': {
                    let inputNumber = input ? input : '1234';

                    attribute.$('input').sendKeys(inputNumber);
                
                    break;
                }

                case 'numericmultiple': {
                    const inputNumber = input ? input : ['1', '2', '3', '4'];

                    for (const index in inputNumber) {
                        attribute.$(`li:nth-child(${Number(index) + 1}) input`).sendKeys(inputNumber[index]);
                        
                        if ( inputNumber.length - 1 > index ) {
                        
                            attribute.$('.vorm-field-add-button').click();
                        
                        }

                    }
                
                    break;
                }

                case 'option': {
                    let options = attribute.all(by.css('[type="radio"]')),
                        inputOption,
                        randomOption;

                    options.count().then((optionCount) => {

                        randomOption = random(1, optionCount);

                        inputOption = input ? input : randomOption;

                        attribute.$(`label:nth-child(${inputOption}) input`).click();

                    });
                
                    break;
                }

                case 'richtext': {
                    let inputText = input ? input : 'this is random text\nand another line';

                    attribute.$('.rich-text-editor.dummy').click();

                    browser.sleep(2000);

                    attribute.$('.ql-editor').sendKeys(inputText);
                
                    break;
                }

                case 'select': {
                    let inputSelect = input ? input : 1;

                    attribute.$('select').click();

                    attribute.$(`select option:nth-child(${inputSelect + 1})`).click();

                    break;
                }

                case 'selectmultiple': {
                    let inputSelect = input ? input : [1, 2, 3, 4];

                    for (const index in inputSelect) {

                        attribute.$(`li:nth-child(${Number(index) + 1}) select`).click();

                        attribute.$(`li:nth-child(${Number(index) + 1}) select option:nth-child(${inputSelect[index] + 1})`).click();

                        if ( inputSelect.length - 1 > index ) {

                            attribute.$('.vorm-field-add-button').click();

                        }

                    }

                    break;
                }

                case 'text':
                case 'text_uc': {
                    let inputText = input ? input : 'this is random text';

                    attribute.$('input').sendKeys(inputText);
                
                    break;
                }

                case 'textmultiple':
                case 'text_ucmultiple': {
                    let inputText = input ? input : ['this', 'is', 'random', 'text'];

                    for (const index in inputText) {

                        attribute.$(`li:nth-child(${Number(index) + 1}) input`).sendKeys(inputText[index]);

                        if ( inputText.length - 1 > index ) {

                            attribute.$('.vorm-field-add-button').click();

                        }

                    }

                    break;
                }

                case 'textarea': {
                    let inputText = input ? input : 'this is random text\nand another line';

                    attribute.$('textarea').sendKeys(inputText);
                
                    break;
                }

                case 'textareamultiple': {
                    let inputText = input ? input : ['this is random text\nand another line', 'and another text\nand another line'];

                    for (const index in inputText) {

                        attribute.$(`li:nth-child(${Number(index) + 1}) textarea`).sendKeys(inputText[index]);

                        if ( inputText.length - 1 > index ) {

                            attribute.$('.vorm-field-add-button').click();

                        }

                    }
                
                    break;
                }

                case 'url': {
                    let inputUrl = input ? input : 'https://www.google.com';

                    attribute.$('input').sendKeys(inputUrl);
                
                    break;
                }

                case 'valuta':
                case 'valutaex':
                case 'valutaex21':
                case 'valutaex6':
                case 'valutain':
                case 'valutain21':
                case 'valutain6': {
                    let inputValuta = input ? input : '12.34';

                    attribute.$('input').sendKeys(inputValuta);
                
                    break;
                }

                default:
                break;
            }

        });

    },

    inputAttributes: ( data = [] ) => {

        for (const index in data ) {

            let clear = data[index].clear ? data[index].clear : false;

            caseAttribute.inputAttribute(data[index].attribute, data[index].input, clear);

        }

    },

    status: attribute => new Promise((resolve) => {

        attribute.$('ul.disabled').isPresent().then(presence => resolve(!presence));

    }),

    statuses: attributes => new Promise((resolve) => {

        let statuses = [];

        attributes.each(attr => {
            caseAttribute.status(attr).then(status => {
                statuses.push(status);
            });
        });

        resolve(statuses);

    }),

    getValue: attribute => {

        return caseAttribute.getAttributeType(attribute).then(attributeType => {

            switch ( attributeType ) {

                case 'bag_adres':
                case 'bag_openbareruimte':
                case 'bag_straat_adres':
                case 'bag_adressenmultiple':
                case 'bag_openbareruimtesmultiple':
                case 'bag_straat_adressenmultiple':
                case 'geolatlon':
                case 'googlemaps':
                return attribute.$('.value-list').getText();

                case 'bankaccount':
                case 'date':
                case 'email':
                case 'image_from_url':
                case 'text':
                case 'text_uc':
                case 'numeric':
                case 'url':
                case 'valuta':
                case 'valutaex':
                case 'valutaex21':
                case 'valutaex6':
                case 'valutain':
                case 'valutain21':
                case 'valutain6':
                return attribute.$('.value-list input').getAttribute('value');

                case 'checkbox': {
                    const checkboxes = attribute.all(by.css('input'));
                    const boxesChecked = [];

                    checkboxes.each(checkbox => {

                        checkbox.getAttribute('checked').then(checked => {

                            if ( checked ) {
                                boxesChecked.push(true);
                            } else {
                                boxesChecked.push(false);
                            }

                        });

                    });

                    return boxesChecked;

                }

                case 'filemultiple': {
                    const files = attribute.all(by.css('.value-list .file-list-item'));
                    const fileNames = [];

                    files.each(file => {

                        file.getText().then(fileName => {

                            fileNames.push(fileName);

                        });

                    });

                    return fileNames;

                }

                case 'numericmultiple':
                case 'textmultiple':
                case 'text_ucmultiple': {
                    let fields = attribute.all(by.css('input'));
                    let fieldValues = [];

                    fields.each(field => {

                        field.getAttribute('value').then(value => {

                            fieldValues.push(value);

                        });

                    });

                    return fieldValues;

                }

                case 'option': {
                    const checkedOption = attribute.$('input:checked');

                    return checkedOption.getAttribute('value');

                }

                case 'richtext':
                return attribute.$('.value-list .ql-editor').getText();

                case 'select':
                return attribute.$('.value-list select').getAttribute('value');

                case 'selectmultiple': {
                    const selects = attribute.all(by.css('select'));
                    const selectValues = [];

                    selects.each(selectOptions => {

                        selectOptions.getAttribute('value').then(value => {

                            selectValues.push(value);

                        });

                    });

                    return selectValues;

                }

                case 'textarea':
                return attribute.$('.value-list textarea').getAttribute('value');

                case 'textareamultiple': {
                    const fields = attribute.all(by.css('textarea'));
                    const fieldValues = [];

                    fields.each(field => {

                        field.getAttribute('value').then(value => {

                            fieldValues.push(value);

                        });

                    });

                    return fieldValues;

                }

                default:
                break;

            }

        });

    },

    getClosedValue: attribute => {

        return attribute.$('.value-list').getText();

    },

    getValutaDisplay: attribute => {

        return attribute.$('zs-btw-display').getText();

    }

};

module.exports = caseAttribute;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
