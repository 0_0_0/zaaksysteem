export default ( textToLookUp, elementCss ) => {

    let elementsToCheck = element.all(by.css(elementCss));

    return elementsToCheck.filter(name => {
        return name.getText().then(text => {
            return text === textToLookUp;
        });
    }).then(filteredElements => {
        return filteredElements.length > 0;
    });
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
