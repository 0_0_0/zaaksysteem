const {
	DEV_SERVER_HOSTNAME,
	DEV_SERVER_PORT,
} = require('./package-config');

var webpack = require('webpack'),
	path = require('path'),
	ExtractTextPlugin = require('extract-text-webpack-plugin'),
	ServiceWorkerCachePlugin = require('./ServiceWorkerCachePlugin'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	HtmlWebpackAlwaysWritePlugin = require('./HtmlWebpackAlwaysWritePlugin'),
	ProtractorTestRunnerPlugin = require('./test/e2e/ProtractorTestRunnerPlugin'),
	ForceCaseSensitivityPlugin = require('force-case-sensitivity-webpack-plugin'),
	NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin'),
	assign = require('lodash/assign'),
	merge = require('lodash/merge'),
	apps = require('./apps'),
	ip = require('ip'),
	mapValues = require('lodash/mapValues'),
	keyBy = require('lodash/keyBy'),
	find = require('lodash/find'),
	minimist = require('minimist');

var env = process.env.NODE_ENV || 'development',
	root,
	publicPath,
	isDevServer = !!find(process.argv, arg => arg.indexOf('webpack-dev-server') !== -1),
	isWatch = (process.argv.indexOf('-w') !== -1),
	devServerPrefix = isDevServer ?
		`https://${DEV_SERVER_HOSTNAME}:${DEV_SERVER_PORT}`
		: '',
	template,
	argv = minimist(process.argv.slice(2)),
	useHash = env === 'development' || !argv['use-hash'],
	templateFilename,
	base = argv.base || '',
	exports;

apps = argv.app ?
	[ argv.app ]
	: apps;

root = argv.root || path.join('..', 'root');
publicPath = (argv['public-path'] === undefined) ?
	'/assets/'
	: argv['public-path'];
template = useHash ?
	'[name]'
	: '[name].[hash]';
templateFilename = (argv['template-path'] !== undefined) ?
	path.join(root, argv['template-path'])
	: undefined;

exports = {
	devtool: env === 'production' ?
		'source-map'
		: 'cheap-module-eval-source-map',
	output: {
		path: path.join(__dirname, root, publicPath),
		publicPath: devServerPrefix + publicPath,
		filename: `${template}.js`
	},
	module: {
		rules: [
			{
				test: /.*\.js$/,
				loader: 'babel-loader',
				exclude: [ /node_modules/, /frontend/ ],
				options: {
					presets: [ 'babel-preset-es2015' ].map(require.resolve),
					plugins: [ 'babel-plugin-add-module-exports' ].map(require.resolve)
				}
			},
			{
				test: /.*\.html$/,
				// removing optional tags breaks HtmlWebpackPlugin script injection
				loader: 'html-loader?removeOptionalTags=false&removeDefaultAttributes=false',
				exclude: /node_modules/
			},
			{
				test: /\.scss$/,
				loader: env === 'production' ?
					ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader', 'autoprefixer-loader', 'sass-loader'] })
					: 'style-loader!css-loader?sourceMap!autoprefixer-loader!sass-loader?sourceMap',
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				loader: env === 'production' ?
					ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader'] })
					: 'style-loader!css-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?name=[name].[ext]&limit=10000'
			},
			{
				test: /\.swig$/,
				loader: 'swig-loader',
				exclude: /node_modules/
			}
		],
		noParse: [
			/[\/\\]node_modules[\/\\]openlayers/,
			/[\/\\]node_modules[\/\\](angular-mocks|angular-ui-router)/,
			/[\/\\]node_modules[\/\\]openlayers/,
			/[\/\\]node_modules[\/\\]api-check/,
			/[\/\\]node_modules[\/\\]email-regex/,
			/[\/\\]node_modules[\/\\]api-check/,
			/[\/\\]node_modules[\/\\]define/,
			/[\/\\]node_modules[\/\\]fuzzy/,
			/[\/\\]node_modules[\/\\]iban/,
			/[\/\\]node_modules[\/\\]is-promise/,
			/[\/\\]node_modules[\/\\]match-media/,
			/[\/\\]node_modules[\/\\]soma-events/,
			/[\/\\]node_modules[\/\\]word-ngrams/
		]
	}
};

var plugins = [
	new webpack.DefinePlugin({
		ENV: env,
		DEV: env === 'development',
		PROD: env === 'production',
		'process.env': {
			NODE_ENV: JSON.stringify(env),
			SUPPORT_PROXY_URL: JSON.stringify(process.env.SUPPORT_PROXY_URL || 'https://localhost:1050'),
			DISABLE_ANIMATION: JSON.stringify(!!process.env.DISABLE_ANIMATION)
		}
	}),
	new NamedModulesPlugin(),
	new HtmlWebpackAlwaysWritePlugin( { filename: templateFilename, stripPrefix: devServerPrefix, root, entries: apps }),
	new ForceCaseSensitivityPlugin()
];

plugins = plugins.concat(
	apps.map(app => {
		return new HtmlWebpackPlugin({
			filename: path.join(app, 'index.html'),
			template: `${path.join('.', 'src', app, 'index.ejs')}`,
			inject: 'body',
			chunks: [ app ],
			minify: false,
			title: 'Zaaksysteem',
			base: base || (`/${app}/`)
		});
	})
);

if (isDevServer) {
	exports = assign(
		{},
		exports,
		{
			devServer: {
				https: true,
				port: DEV_SERVER_PORT,
				host: DEV_SERVER_HOSTNAME,
			}
		}
	);
}

if (env === 'production') {
	plugins = plugins.concat(
		apps.map(
			app => {
				return new ServiceWorkerCachePlugin({
					root,
					publicPath,
					entries: [ app ],
					globs: [ path.join(app, 'index.html') ],
					options: {
						navigateFallback: `${publicPath}${app}/index.html`,
						navigateFallbackWhitelist: [ new RegExp('^\/' + app ) ] //eslint-disable-line
					}
				});
			}
		)
	);

	plugins = plugins.concat(
		new ExtractTextPlugin({
			filename: `${template}.css`,
			allChunks: true
		}),
		new webpack.optimize.UglifyJsPlugin({
			comments: false,
			sourceMap: true
		})
	);
}

// if (isWatch || isDevServer) {
// 	plugins = plugins.concat(
// 		new ProtractorTestRunnerPlugin({
// 			directories: [ path.join(__dirname, './test/e2e') ],
// 			mode: isWatch ? 'watch' : 'dev-server'
// 		})
// 	);
// }

module.exports = merge({}, exports, {
	plugins,
	entry: mapValues(
		keyBy(apps),
		( app ) => `./src/${app}/index.js`
	)
});
