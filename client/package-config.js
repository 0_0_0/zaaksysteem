const { env } = process;
const configExpression = /^npm_package_config_(.*)/;

const reducer = ( bucket, key ) => {
	const match = configExpression.exec(key);

	if (match) {
		const packageKey = match[1];

		bucket[packageKey] = env[key];
	}

	return bucket;
}

const packageConfig = Object
	.keys(env)
	.reduce(reducer, {});

module.exports = packageConfig;
