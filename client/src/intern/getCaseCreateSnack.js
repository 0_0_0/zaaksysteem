import propCheck from './../shared/util/propCheck';
import assign from 'lodash/fp/assign';

export default ( $state, values ) => {

	let message,
		actions = [],
		goToCaseAction =
			{
				type: 'link',
				link: $state.href('case', { caseId: values.caseNumber }),
				label: 'Zaak openen'
			};

	propCheck.throw(
		propCheck.shape({
			caseNumber: propCheck.number.optional,
			assigneeResult: propCheck.string.optional,
			userIsAssignee: propCheck.bool.optional,
			status: propCheck.string.optional,
			$state: propCheck.object
		}),
		assign({ $state }, values)
	);

	if (
		(values.caseNumber && $state.current.name.indexOf('case') === -1 )
		&& !values.userIsAssignee
	) {
		message = `Zaak geregistreerd onder zaaknummer ${values.caseNumber}`;
		if (values.assigneeResult === 'failed') {
			message += ', maar kon niet worden toegewezen aan een behandelaar.';
		}
		actions = actions.concat(goToCaseAction);
	} else {
		message = values.status === 'open' ?
			'Zaak is door u in behandeling genomen'
			: 'Zaak is geregistreerd';

		if (values.caseNumber) {
			message += ` onder zaaknummer ${values.caseNumber}`;
			if (!values.userIsAssignee) {
				actions = actions.concat(goToCaseAction);
			}
		}
	}

	return {
		message,
		actions
	};

};
