import angular from 'angular';
import vormFieldsetModule from './../../../../../../../../shared/vorm/vormFieldset';
import vormValidatorModule from './../../../../../../../../shared/vorm/util/vormValidator';
import resourceModule from './../../../../../../../../shared/api/resource';
import composedReducerModule from './../../../../../../../../shared/api/resource/composedReducer';
import zsIconModule from './../../../../../../../../shared/ui/zsIcon';
import controller from './CaseActionFormController';
import template from './template.html';

export default angular
	.module('zsCaseActionForm', [
		vormFieldsetModule,
		vormValidatorModule,
		composedReducerModule,
		resourceModule,
		zsIconModule
	])
	.component('zsCaseActionForm', {
		bindings: {
			action: '&',
			templates: '&',
			caseDocuments: '&',
			requestor: '&',
			phases: '&',
			formNote: '&',
			onActionSave: '&',
			onActionExecute: '&'
		},
		controller,
		template
	})
	.name;
