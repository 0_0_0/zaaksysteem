import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import isEqual from 'lodash/isEqual';
import omit from 'lodash/omit';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import serviceworkerModule from './../shared/util/serviceworker';
import zsVormTemplateModifierModule from './../shared/zs/vorm/zsVormTemplateModifier';
import shouldReloadModule from './../shared/util/route/shouldReload';
import zsSnackbarModule from './../shared/ui/zsSnackbar';
import zsUiViewProgressModule from './../shared/ui/zsNProgress/zsUiViewProgress';
import zsStorageModule from './../shared/util/zsStorage';
import stateNameInBodyModule from './../shared/util/route/stateNameInBody';
import isApiDenied from './../shared/api/isApiDenied';
import './styles.scss';

// require all actions to ensure they're available on page load

let context = require.context('./', true, /\/actions\.js$/),
	actionModules = context.keys().map(context);

if (DEV) { //eslint-disable-line
	require('lodash-migrate');
}

export default angular
	.module('Zaaksysteem.intern', [
		routing,
		resourceModule,
		serviceworkerModule,
		ngAnimate,
		zsVormTemplateModifierModule,
		shouldReloadModule,
		zsSnackbarModule,
		zsUiViewProgressModule,
		zsStorageModule,
		stateNameInBodyModule
	].concat(actionModules))
	.config([
		'apiCacherProvider', 'resourceProvider',
		( apiCacherProvider, resourceProvider ) => {
			zsResourceConfiguration(resourceProvider.configure);

			apiCacherProvider.setIsEqual(( a, b ) => {
				let firstVal = a,
					secondVal = b;

				if (firstVal && typeof firstVal !== 'string' && 'request_id' in firstVal) {
					firstVal = omit(firstVal, 'request_id');
				}

				if (secondVal && typeof secondVal !== 'string' && 'request_id' in secondVal) {
					secondVal = omit(secondVal, 'request_id');
				}

				return isEqual(firstVal, secondVal);
			});
		}])
	.config(['$compileProvider', ( $compileProvider ) => {
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|smb):/);
	}])
	.config(['$httpProvider', ( $httpProvider ) => {
		$httpProvider.defaults.withCredentials = true;
		$httpProvider.defaults.headers.common['X-Client-Type'] = 'web';
		$httpProvider.useApplyAsync(true);

		$httpProvider.interceptors.push([
			'zsStorage', '$window', '$q',
			( zsStorage, $window, $q ) => ({
				response: response => {
					if (response.config.url === '/api/v1/session/current') {
						if (!response.data.result.instance.logged_in_user) {
							zsStorage.clear();
							$window.location.href = `/auth/login?referer=${$window.location.pathname}`;

							return $q.reject(response);
						}
					}

					return $q.resolve(response);
				},
				responseError: rejection => {
					if (isApiDenied(rejection)) {
						zsStorage.clear();
						$window.location.href = `/auth/login?referer=${$window.location.pathname}`;
					}

					return $q.reject(rejection);
				}
			})
		]);
	}])
	// ocLazyLoad calls the ngModule config block twice due to
	// a bug in ocLazyLoad and angular-dragula defining ng as a
	// dependency, causing the $$animateJs service provider to be
	// overridden with the defaults, breaking animations
	// so we inject it at runtime immediately to instantiate
	// the factory
	.run(['$$animateJs', angular.noop])
	.run([
		'$window', 'serviceWorker', '$animate',
		( $window, serviceWorker, $animate ) => {
			let enable = PROD, //eslint-disable-line
				worker = serviceWorker('/intern/');

			$animate.enabled(!process.env.DISABLE_ANIMATION);

			// ZS-TODO: refactor pyramid of doom
			if (worker.isSupported()) {
				worker.isEnabled()
					.then(( enabled ) => {
						if (enabled !== enable) {
							return (enable ?
									worker.enable()
									: worker.disable()
										.then(() => {
											if (enabled) {
												$window.location.reload();
											}
										})
							);
						}
					});
			}
		}
	])
	.run([
		'$rootScope', '$window', '$location', '$document',
		( $rootScope, $window, $location, $document ) => {
			const whitelist = [
				/^\/externaldocument\//
			];

			function scrollToTop( element, scrollDuration ) {
				const cosParameter = (element.scrollTop / 2);
				let oldTimestamp = $window.performance.now();
				let scrollCount = 0;

				function step( newTimestamp ) {
					scrollCount += (Math.PI / (scrollDuration / (newTimestamp - oldTimestamp)));

					if (scrollCount >= Math.PI) {
						element.scrollTop = 0;
					}

					if (element.scrollTop === 0) {
						return;
					}

					element.scrollTop = Math.round(cosParameter + cosParameter * Math.cos(scrollCount));
					oldTimestamp = newTimestamp;
					$window.requestAnimationFrame(step);
				}

				$window.requestAnimationFrame(step);
			}

			$rootScope.$on('$stateChangeSuccess', () => {
				const path = $location.path();
				const forceScrollToTop = whitelist.some(
					pathExpression =>
						pathExpression.test(path)
				);

				if (forceScrollToTop) {
					scrollToTop($document[0].querySelector('div[ui-view]'), 300);
				}
			});
		}])
	.name;
