import adjustEngine from 'adjust-engine';
import defaultsDeep from 'lodash/defaultsDeep';
import flatten from 'lodash/flatten';
import includes from 'lodash/includes';

export default ( scope, element, document, $animate, preferredOptions = { } ) => {

	let enabled = false,
		adjuster,
		win = document[0].defaultView,
		options = defaultsDeep({}, preferredOptions, {
			attachment: 'top left',
			target: 'bottom left',
			flip: false,
			updateOn: 'default'
		}),
		unwatch,
		updateOn = flatten(
			options.updateOn.split(' ').map(
				prop => {
					return prop === 'default' ? [ 'scroll', 'resize' ] : prop;
				}
			)
		);

	adjuster = adjustEngine(options);

	let position = ( ) => {

		let elRect,
			reference = options.reference,
			referenceRect = reference[0].getBoundingClientRect(),
			pos;

		if (options.autosize) {
			element.css('width', `${referenceRect.width}px`);
		}

		elRect = element[0].getBoundingClientRect();

		pos = adjuster(elRect, referenceRect, { left: 0, top: 0, right: win.innerWidth, bottom: win.innerHeight });

		if (element[0].offsetParent) {

			let offsetRect = element[0].offsetParent.getBoundingClientRect();

			pos.top -= offsetRect.top;
			pos.left -= offsetRect.left;

		}

		element.css({
			top: `${pos.top}px`,
			left: `${pos.left}px`
		});

	};

	let onResize = ( ) => {
		position();
	};

	let onScroll = ( ) => {
		position();
	};

	return {
		enable: ( ) => {

			if (!enabled) {

				if (includes(updateOn, 'resize')) {
					win.addEventListener('resize', onResize);
				}

				if (includes(updateOn, 'scroll')) {
					document[0].addEventListener('scroll', onScroll, true);
				}

				if (includes(updateOn, 'watch')) {
					unwatch = scope.$watch(( ) => {
						position();
					});
				}
				
				if (includes(updateOn, 'animate')) {

					// we need to wait for $animate.addClass, or else the
					// element still has display: none set which means
					// we're unable to position it correctly

					let keys = 'addClass removeClass enter remove'.split(' ');

					let handleAnimEvent = ( ) => {

						keys.forEach(key => {
							$animate.off(key, element, handleAnimEvent);
						});

						position();

					};

					keys.forEach(key => {
						$animate.on(key, element, handleAnimEvent);
					});

				}

				position();

				enabled = true;
			}

		},
		disable: ( ) => {

			if (enabled) {

				win.removeEventListener('resize', onResize);
				document[0].removeEventListener('scroll', onScroll, true);

				if (unwatch) {
					unwatch();
					unwatch = null;
				}

				enabled = false;
			}

		},
		position,
		isEnabled: ( ) => enabled
	};

};
