import startsWith from 'lodash/startsWith';
import { toCurrency, toNumber } from '../../util/number';
import multipliers from './multipliers';

export default class BtwDisplayController {
	constructor( currencyFilter ) {
		this.currencyFilter = currencyFilter;
	}

	getBtwDescription() {
		return `${startsWith(this.btwType(), 'in') ? 'excl.' : 'incl.'} btw`;
	}

	getBtwValue() {
		const val = toNumber(this.value());
		const amount = (isNaN(val) || val === null) ?
			0
			: toCurrency(val * multipliers[this.btwType()]);

		return this.currencyFilter(amount);
	}
}
