import angular from 'angular';
import 'angular-mocks';
import savedSearchesServiceModule from '.';

describe('savedSearchesService', ( ) => {

	let savedSearchesService;

	beforeEach(angular.mock.module(savedSearchesServiceModule));

	beforeEach(angular.mock.inject([ 'savedSearchesService', ( ...rest ) => {

		[ savedSearchesService ] = rest;

	}]));

	it('should return request options based on a query', ( ) => {

		let opts = savedSearchesService.getRequestOptions();

		expect(opts.url).toBeDefined();
		expect(opts.params).toBeDefined();

		expect(angular.equals(savedSearchesService.getRequestOptions('foo'), opts)).toBe(false);

	});

	it('should add the predefined searches to a data set', ( ) => {

		expect(savedSearchesService.filter([]).length).toBe(savedSearchesService.getPredefinedSearches().length);

		expect(savedSearchesService.filter([], 'foo').length).toBe(0);
		expect(savedSearchesService.filter([], 'afdeling').length).toBe(1);

	});

});

