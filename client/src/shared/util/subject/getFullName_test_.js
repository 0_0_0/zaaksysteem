import { expect } from 'chai';
import getFullName from './getFullName';

describe('The `getFullName` function', () => {
	it('returns `display_name` if `subject_type` is not `person`', () => {
		const name = getFullName({
			instance: {
				subject_type: 'company',
				display_name: 'Foo corp.'
			}
		});

		expect(name).to.equal('Foo corp.');
	});

	describe('if the `subject_type` is `person`', () => {
		it(
			'returns `first_names` and `surname` joined by a space if `first_names` is not empty',
			() => {
				const name = getFullName({
					instance: {
						subject_type: 'person',
						display_name: 'Fred Foobar the third',
						subject: {
							instance: {
								first_names: 'Fred Foster',
								surname: 'Foobar'
							}
						}
					}
				});

				expect(name).to.equal('Fred Foster Foobar');
			});

		it('returns `surname` if `first_names` is empty', () => {
			const name = getFullName({
				instance: {
					subject_type: 'person',
					display_name: 'Fred Foobar',
					subject: {
						instance: {
							first_names: null,
							surname: 'Foobar'
						}
					}
				}
			});

			expect(name).to.equal('Foobar');
		});
	});
});
