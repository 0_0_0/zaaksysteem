import { expect } from 'chai';
import padLeft from './padLeft';

function typeErrorFactory( callback ) {
	const errors = [];
	const badTypes = [ 42, {}, [], () => {}, false, null, undefined ];

	badTypes.forEach(value => {
		try {
			callback(value);
		} catch (error) {
			if (error instanceof TypeError) {
				errors.push(error.message);
			}
		}
	});

	expect(errors.length).to.equal(badTypes.length);

	badTypes
		.map(value => typeof value)
		.forEach(( type, index ) => {
			expect(errors[index]).to.equal(`expected ${type} to be a string`);
		});

}

/**
 * @test {padLeft}
 */
describe('The `padLeft` function', () => {

	it('throws a type error if the first argument is not a string', () => {
		typeErrorFactory(value => padLeft(value, ''));
	});

	it('throws a type error if the second argument is not a string', () => {
		typeErrorFactory(value => padLeft('', value));
	});

	it('returns the pattern if the subject is empty', () => {
		expect(padLeft('', '0')).to.equal('0');
	});

	it('returns the subject if its length is >= the length of the pattern', () => {
		expect(padLeft('4', '0')).to.equal('4');
		expect(padLeft('42', '0')).to.equal('42');
	});

	it('pads the subject with the left hand side of the pattern', () => {
		expect(padLeft('4', '00')).to.equal('04');
		expect(padLeft('42', '+++')).to.equal('+42');
		expect(padLeft('777', '+-?!?')).to.equal('+-777');
	});

});
