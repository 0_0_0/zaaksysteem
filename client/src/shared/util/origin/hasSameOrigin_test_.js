import { expect } from 'chai';
import { hasSameOrigin } from '.';

describe('The `hasSameOrigin` function', () => {
	it('returns `true` if the URL starts with an absolute path', () => {
		expect(hasSameOrigin('/api')).to.equal(true);
	});

	it('returns `false` if the absolute path’s first segment is empty', () => {
		expect(hasSameOrigin('/')).to.equal(false);
	});

	it('return `false` if the URL is absolute', () => {
		expect(hasSameOrigin('https://example.org/api')).to.equal(false);
	});

	it('returns `false` if the URL is relative to the protocol', () => {
		expect(hasSameOrigin('//example.org/api')).to.equal(false);
	});
});
