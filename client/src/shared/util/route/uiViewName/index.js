import angular from 'angular';
import getViewName from './../getViewName';

export default
	angular.module('uiViewName', [
	])
		.directive('uiViewName', [ '$interpolate', ( $interpolate ) => {

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					element.addClass(getViewName($interpolate, scope, element, attrs).replace('@', ''));

				}

			};

		}])
		.name;
