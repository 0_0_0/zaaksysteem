import { expect } from 'chai';
import { formatZipcode, NBSP } from './format';

describe('The `formatZipcode` function', () => {
	it('inserts a non breaking space if no space is present', () => {
		expect(formatZipcode('1234AA')).to.equal(`1234${NBSP}AA`);
	});

	it('replaces a plain space character with a non breaking space', () => {
		expect(formatZipcode('1234 AA')).to.equal(`1234${NBSP}AA`);
	});

	it('does not mutate already formatted data', () => {
		expect(formatZipcode(`1234${NBSP}AA`)).to.equal(`1234${NBSP}AA`);
	});
});
