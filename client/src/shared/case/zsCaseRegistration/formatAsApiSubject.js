export default ( subject ) => {

	let type = 'subject',
		reference = subject.reference;

	return {
		type,
		reference
	};

};
