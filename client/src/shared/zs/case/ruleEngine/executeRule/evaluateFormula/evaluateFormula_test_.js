import { expect } from 'chai';
import evaluateFormula from '.';

describe('evaluateFormula', ( ) => {
	it('should return the result of the formula', ( ) => {
		expect(evaluateFormula('foo * 5', {
			'attribute.foo': 3
		})).to.equal(15);
		expect(evaluateFormula('foo * bar', {
			'attribute.foo': 3,
			'attribute.bar': 4
		})).to.equal(12);
		expect(evaluateFormula('foo / bar', {
			'attribute.foo': 4,
			'attribute.bar': 2
		})).to.equal(2);
		expect(evaluateFormula('foo - bar', {
			'attribute.foo': 4,
			'attribute.bar': 1
		})).to.equal(3);
		expect(evaluateFormula('bar - foo', {
			'attribute.foo': 4,
			'attribute.bar': 1
		})).to.equal(-3);
	});

	it('should normalize non-numeric values to 0', ( ) => {
		expect(evaluateFormula('bar - foo', {
			'attribute.foo': 4,
			'attribute.bar': NaN
		})).to.equal(-4);
		expect(evaluateFormula('bar - foo + blah', {
			'attribute.foo': 2,
			'attribute.bar': NaN
		})).to.equal(-2);
	});

	it('should accept a numeric string with decimal comma in the data', ( ) => {
		expect(evaluateFormula('foo * 2', {
			'attribute.foo': '4,5'
		})).to.equal(9);
	});

	it('should normalize infinity to null', ( ) => {
		expect(evaluateFormula('3 / 0')).to.equal(null);
		expect(evaluateFormula('-3 / 0')).to.equal(null);
	});

	it('should round off to two decimals', ( ) => {
		expect(evaluateFormula('4 / 3')).to.equal(1.33);
	});

	it('should catch evaluation errors and normalize to null', ( ) => {
		expect(evaluateFormula('3-2.2.2')).to.equal(null);
	});
});
