import angular from 'angular';
import 'angular-mocks';
import inputModule from './../input';

xdescribe('vormFormField', ( ) => {

	let $compile,
		$rootScope,
		ctrl,
		fieldCtrl,
		scope,
		el;

	beforeEach(angular.mock.module(inputModule));

	beforeEach(angular.mock.inject([ '$compile', '$rootScope', ( ...rest ) => {

		[ $compile, $rootScope ] = rest;

		scope = $rootScope.$new();

	}]));


	let compileWith = ( config ) => {

		scope.config = config;

		el = angular.element('<vorm-field-template name="value" data-config="config"></vorm-field>');

		$compile(el)(scope);

		scope.$digest();

		ctrl = el.find('vorm-form-field').controller('vormFormField');

		fieldCtrl = el.controller('vormField');
	};

	describe('with a single nested form', ( ) => {

		beforeEach(( ) => {
			
			compileWith({
				type: 'form',
				data: {
					fields: [
						{
							name: 'foo',
							type: 'text',
							required: true
						},
						{
							name: 'bar',
							type: 'text',
							required: false
						}
					]
				}
			});

		});

		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

			expect(fieldCtrl).toBeDefined();

		});

		it('should be empty when all values are', ( ) => {

			fieldCtrl.setValue({});

			scope.$digest();

			expect(fieldCtrl.isEmpty()).toBe(true);

			fieldCtrl.setValue({ foo: 'a' });

			scope.$digest();

			expect(fieldCtrl.isEmpty()).toBe(false);

			fieldCtrl.setValue({ foo: null });

			scope.$digest();

			expect(fieldCtrl.isEmpty()).toBe(true);

		});

		it('should be valid when all values are', ( ) => {

			fieldCtrl.setValue({ });

			scope.$digest();

			expect(fieldCtrl.isValid()).toBe(false);

			fieldCtrl.setValue({ foo: 'a' });

			scope.$digest();

			expect(fieldCtrl.isValid()).toBe(true);

		});

		it('should set the value of the model when the form value changes', ( ) => {

			let formCtrl = el.find('vorm-form').controller('vormForm'),
				field = formCtrl.getFields()[0],
				model = field.getModels()[0];

			expect(fieldCtrl.getValue()).toEqual(null);

			model.$setViewValue('foo');

			expect(fieldCtrl.getValue()).toEqual( { foo: 'foo', bar: undefined });

		});

	});

	describe('with more than one level of nested forms', ( ) => {

		beforeEach( ( ) => {
			
			compileWith({
				name: 'outer',
				type: 'form',
				required: true,
				data: {
					fields: [
						{
							name: 'foo',
							type: 'text',
							required: true
						},
						{
							name: 'inner',
							type: 'form',
							data: {
								fields: [
									{
										name: 'bar',
										type: 'text',
										required: false
									},
									{
										name: 'baz',
										type: 'text',
										required: true
									}
								]
							},
							required: true
						}
					]
				}
			});

		});

		it('should not be valid if the nested form is invalid', ( ) => {

			expect(fieldCtrl.isValid()).toBe(false);

			fieldCtrl.setValue( { foo: 'bar', inner: {} });

			scope.$digest();

			expect(fieldCtrl.isValid()).toBe(false);

			fieldCtrl.setValue( { foo: 'bar', inner: { bar: 'foo', baz: undefined } });

			scope.$digest();

			expect(fieldCtrl.isValid()).toBe(false);

			fieldCtrl.setValue( { foo: 'bar', inner: { bar: 'foo', baz: 'foo' } });

			scope.$digest();

		});
		

	});
	

});
