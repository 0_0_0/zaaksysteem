import angular from 'angular';
import 'angular-mocks';
import vormRadioGroupModule from '.';

xdescribe('vormRadioGroup', ( ) => {

	let ctrl,
		fieldCtrl,
		scope,
		el;

	beforeEach(angular.mock.module(vormRadioGroupModule));

	beforeEach(angular.mock.inject([ '$compile', '$rootScope', ( $compile, $rootScope ) => {

		scope = $rootScope.$new();

		scope.config = {
			type: 'radio',
			data: {
				options: [
					{
						value: 'foo',
						label: 'foo'
					},
					{
						value: 'bar',
						label: 'bar'
					}
				]
			}
		};

		el = angular.element('<vorm-field-template name="value" data-config="config"></vorm-field>');

		$compile(el)(scope);

		scope.$digest();

		ctrl = el.find('vorm-radio-group').controller('vormRadioGroup');

		fieldCtrl = el.controller('vormField');

	}]));

	it('should have a controller', ( ) => {

		expect(ctrl).toBeDefined();

	});

	it('should return the checked state based on the model value', ( ) => {

		fieldCtrl.setValue('foo');

		scope.$digest();

		expect(ctrl.isChecked('foo')).toBe(true);

		expect(ctrl.isChecked('bar')).toBe(false);

	});

	it('should change the model value if a radio button is clicked', ( ) => {

		ctrl.handleRadioClick('foo');

		expect(fieldCtrl.getValue()).toBe('foo');

	});

});
