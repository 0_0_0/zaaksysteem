import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
 
export default
	angular.module('vorm.types.select', [
			vormTemplateServiceModule
		])
			.run([ 'vormTemplateService', function ( vormTemplateService ) {
				
				const el = angular.element(`
					<select
						ng-model
						ng-options="option.value as option.label for option in vm.invokeData('options')"
						id="{{::vm.getInputId()}}"
					>

						<option
							value=""
							ng-show="!!vm.invokeData('notSelectedLabel')"
						>
							{{vm.invokeData('notSelectedLabel')}}
						</option>
					</select>
				`);
				
				vormTemplateService.registerType('select', {
					control: el
				});
				
			}])
			.name;
