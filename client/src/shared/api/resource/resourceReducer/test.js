import angular from 'angular';
import 'angular-mocks';
import resourceReducerModule from '.';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import noop from 'lodash/noop';

describe('resourceReducer', ( ) => {

	let data = ({ foo: 'bar' }),
		configureSpies = ( resourceReducerProvider ) => {

		return mapValues(
			mapValues(
				keyBy(
					'next prev cursor limit totalRows totalPages data error merge'.split(' ')
				),
				( ) => {
					return ( ) => data;
				}
			),
			( fn, key ) => {

				let spy = jasmine.createSpy(key);

				spy.and.callFake(fn);

				resourceReducerProvider.configure[key](spy);

				return spy;
			}
		);
	};

	describe('without configuration', ( ) => {

		let reducer,
			$log;

		beforeEach(angular.mock.module(resourceReducerModule));

		beforeEach(angular.mock.inject([ '$log', 'resourceReducer', ( log, resourceReducer ) => {

			$log = log;

			reducer = resourceReducer();

		}]));

		it('should throw an error when pagination is attempted', ( ) => {

			spyOn($log, 'error');

			reducer.next();

			expect($log.error).toHaveBeenCalled();

			$log.error.calls.reset();

			// data and error are exceptions

			reducer.data();

			expect($log.error).not.toHaveBeenCalled();

		});

		describe('when frozen', ( ) => {

			let spy = jasmine.createSpy('reduce').and.returnValue(data);

			beforeEach( ( ) => {

				reducer.frozen(true);

				reducer.reduce(spy);

			});

			it('should not call the reducers', ( ) => {

				reducer.data();

				expect(spy).not.toHaveBeenCalled();

				reducer.frozen(false);

				reducer.data();

				expect(spy).toHaveBeenCalled();

			});

			it('should return the result from the cache', ( ) => {

				reducer.frozen(false);

				reducer.data();

				expect(spy).toHaveBeenCalled();

				spy.calls.reset();

				expect(reducer.data()).toEqual(data);

				expect(spy).not.toHaveBeenCalled();

			});

		});

	});

	describe('with configuration', ( ) => {

		let spies,
			reducer,
			args = [ { foo: 'bar' } ];

		beforeEach(angular.mock.module(resourceReducerModule, [ 'resourceReducerProvider', ( resourceReducerProvider ) => {

			spies = configureSpies(resourceReducerProvider);

		}]));


		beforeEach(angular.mock.inject(['resourceReducer', ( resourceReducer ) => {

			reducer = resourceReducer({ args });

		}]));

		it('should call the configured reducers', ( ) => {

			reducer.next();

			expect(spies.next).toHaveBeenCalled();

			expect(spies.next.calls.argsFor(0)).toEqual([ ...args, reducer.$source ]);

		});

		it('should return the result of the reducers', ( ) => {

			expect(reducer.next()).toEqual(data);

		});

		it('should pass on the arguments given to the reducer', ( ) => {

			let arg = 5;

			reducer.next(arg);

			expect(spies.next).toHaveBeenCalled();

			expect(spies.next.calls.argsFor(0)).toEqual([ ...args, reducer.$source, arg ]);

		});

		it('should add a reducer to data', ( ) => {

			let spy = jasmine.createSpy('data'),
				spy2 = jasmine.createSpy('data2');

			reducer.reduce(spy);

			reducer.data();

			expect(spy).toHaveBeenCalled();

			reducer.reduce(spy2);

			reducer.data();

			expect(spy2).toHaveBeenCalled();

		});

		it('should add a reducer to the method given', ( ) => {

			let key = 'next',
				spy = jasmine.createSpy(key);

			reducer.reduce(key, spy);

			reducer[key]();

			expect(spy).toHaveBeenCalled();

		});

		it('should return itself when reduce is called', ( ) => {

			expect(reducer.reduce(noop)).toBe(reducer);

		});

		it('should remove a reducer function when unreduce is called', ( ) => {

			let spy = jasmine.createSpy('data');

			reducer.reduce(spy);

			reducer.data();

			expect(spy).toHaveBeenCalled();

			reducer.unreduce(spy);

			spy.calls.reset();

			reducer.data();

			expect(spy).not.toHaveBeenCalled();


		});

		it('should only call the reducers once', ( ) => {

			let spy = jasmine.createSpy('data');

			reducer.reduce(spy);

			reducer.data();

			expect(spy).toHaveBeenCalled();

			spy.calls.reset();

			reducer.data();

			expect(spy).not.toHaveBeenCalled();

		});

		it('should re-evaluate every reducer when one is added', ( ) => {

			let spy = jasmine.createSpy('data');

			reducer.data();

			expect(spies.data).toHaveBeenCalled();

			spies.data.calls.reset();

			reducer.data();
			
			expect(spies.data).not.toHaveBeenCalled();

			reducer.reduce(spy);

			reducer.data();

			expect(spies.data).toHaveBeenCalled();

			expect(spy).toHaveBeenCalled();

		});

		it('should change the args and reset the cache', ( ) => {

			let nwArg = 'bar';

			reducer.data();

			expect(spies.data.calls.argsFor(0)[0]).toBe(args[0]);

			reducer.setArgs(nwArg);

			spies.data.calls.reset();

			reducer.data();

			expect(spies.data).toHaveBeenCalled();

			expect(spies.data.calls.argsFor(0)[0]).toBe(nwArg);

		});

		describe('with a throwing reducer', ( ) => {

			let spy;

			beforeEach(( ) => {

				spy = jasmine.createSpy();

				spy.and.throwError();

				reducer.$source = data;

				reducer.reduce(spy);

			});

			it('should not throw', ( ) => {

				expect(reducer.data).not.toThrow();

			});

			it('should return null', ( ) => {

				expect(reducer.data()).toBeNull();

			});

			it('should try again', ( ) => {

				reducer.data();

				expect(spy).toHaveBeenCalled();

				spy.calls.reset();

				reducer.data();

				expect(spy).toHaveBeenCalled();

			});

		});

	});

	describe('when stateful', ( ) => {

		let spies,
			reducer;

		beforeEach(angular.mock.module(resourceReducerModule, [ 'resourceReducerProvider', ( resourceReducerProvider ) => {

			spies = configureSpies(resourceReducerProvider);

		}]));


		beforeEach(angular.mock.inject(['resourceReducer', ( resourceReducer ) => {

			reducer = resourceReducer({ stateful: true });

		}]));

		it('should call the reducers every time', ( ) => {

			expect(spies.data).not.toHaveBeenCalled();

			reducer.data();

			expect(spies.data).toHaveBeenCalled();

			spies.data.calls.reset();

			reducer.data();

			expect(spies.data).toHaveBeenCalled();

		});

	});

});
