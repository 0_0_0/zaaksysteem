import { expect } from 'chai';
import isApiDenied, {
	CONFIGURATION_INCOMPLETE,
	isIncomplete,
	isUnauthorized
} from './isApiDenied';


const dataIncomplete = {
	result: {
		instance: {
			type: CONFIGURATION_INCOMPLETE
		}
	}
};

describe('The `isApiDenied` module', () => {
	describe('exports a default function that', () => {
		it('returns `true` if the status is 401', () => {
			const reason = {
				status: 401,
				config: {
					url: '/our/api'
				}
			};

			expect(isApiDenied(reason)).to.equal(true);
		});

		it('returns `true` if the URL has the same origin and the configuration is incomplete', () => {
			const reason = {
				status: 200,
				config: {
					url: '/our/api'
				},
				data: dataIncomplete
			};

			expect(isApiDenied(reason)).to.equal(true);
		});

		it(
			'returns `false` if the URL has another origin and happens to have a response object that resembles ours',
			() => {
				const reason = {
					status: 200,
					config: {
						url: '//their/api'
					},
					data: dataIncomplete
				};

				expect(isApiDenied(reason)).to.equal(false);
			});

		it('returns `false` if the URL has another origin', () => {
			const reason = {
				status: 401,
				config: {
					url: '//their/api'
				}
			};

			expect(isApiDenied(reason)).to.equal(false);
		});
	});

	describe('exports a utility function that', () => {
		it('returns `false` if the URL has the same origin and type is not incomplete', () => {
			expect(isIncomplete('NIL', '/my/api')).to.equal(false);
		});

		it('returns `true` if the URL has the same origin and type is incomplete', () => {
			expect(isIncomplete(CONFIGURATION_INCOMPLETE, '/my/api')).to.equal(true);
		});

		it('returns `false` if the URL has another origin and type is not incomplete', () => {
			expect(isIncomplete('NIL', '//their/api')).to.equal(false);
		});

		it('returns `false` if the URL has another origin and type is incomplete', () => {
			expect(isIncomplete(CONFIGURATION_INCOMPLETE, '//their/api')).to.equal(false);
		});
	});

	describe('exports a utility function that', () => {
		it('returns `true` if the URL has the same origin and the status is 401', () => {
			expect(isUnauthorized(401, '/my/api')).to.equal(true);
		});

		it('returns `false` if the URL has the same origin and the status is not 401', () => {
			expect(isUnauthorized(200, '/my/api')).to.equal(false);
		});

		it('returns `false` if the URL has another origin and the status is 401', () => {
			expect(isUnauthorized(401, '//their/api')).to.equal(false);
		});
	});
});
