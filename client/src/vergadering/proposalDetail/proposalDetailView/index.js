import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import proposalNoteBarModule from './proposalNoteBar';
import proposalVoteBarModule from './proposalVoteBar';
import rwdServiceModule from '../../../shared/util/rwdService';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import zsPdfViewerModule from '../../../shared/ui/zsPdfViewer';
import zsModalModule from '../../../shared/ui/zsModal';
import sessionServiceModule from '../../../shared/user/sessionService';
import getAttributes from '../../shared/getAttributes';
import actionsModule from './actions';
import shortid from 'shortid';
import get from 'lodash/get';
import includes from 'lodash/includes';
import find from 'lodash/find';
import isArray from 'lodash/isArray';
import isEmpty from 'lodash/isEmpty';
import uniqBy from 'lodash/uniqBy';
import first from 'lodash/head';
import sortBy from 'lodash/sortBy';
import identity from 'lodash/identity';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalDetailView', [
			composedReducerModule,
			angularUiRouterModule,
			proposalNoteBarModule,
			rwdServiceModule,
			auxiliaryRouteModule,
			sessionServiceModule,
			proposalVoteBarModule,
			actionsModule,
			snackbarServiceModule,
			zsPdfViewerModule,
			zsModalModule
		])
		.directive('proposalDetailView',
			[ '$sce', '$window', '$timeout', '$document', '$http', '$compile', '$state', 'dateFilter', 'composedReducer', 'auxiliaryRouteService', 'rwdService', 'sessionService', 'snackbarService', 'zsModal',
			( $sce, $window, $timeout, $document, $http, $compile, $state, dateFilter, composedReducer, auxiliaryRouteService, rwdService, sessionService, snackbarService, zsModal ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposal: '&',
					documents: '&',
					casetype: '&',
					notes: '&',
					onSaveNote: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						fieldReducer,
						styleReducer,
						proposalReducer,
						titleReducer,
						userResource = sessionService.createResource($scope),
						casetypeAttributesReducer,
						voteAttributesReducer,
						voteBarConfigReducer,
						voteBarConfigValueReducer,
						voteBarDataReducer,
						proposalResultsReducer,
						voteRightsReducer,
						noteReducer,
						noteStyleReducer,
						appConfigReducer,
						loading = false;

					appConfigReducer = composedReducer({ scope: $scope }, ctrl.appConfig )
						.reduce( config => config);

					casetypeAttributesReducer = composedReducer({ scope: $scope }, ctrl.casetype )
						.reduce( casetype => uniqBy(casetype.instance.phases.flatMap(phase => phase.fields), 'magic_string') );

					proposalReducer = composedReducer({ scope: $scope }, ctrl.proposal() )
						.reduce( proposal => proposal );

					voteAttributesReducer = composedReducer({ scope: $scope }, appConfigReducer, casetypeAttributesReducer )
						.reduce( ( config, casetypeAttributes ) => {

							let attributes = config.instance.interface_config.magic_strings_accept ? config.instance.interface_config.magic_strings_accept.concat(config.instance.interface_config.magic_strings_comment) : seamlessImmutable([]);

							return attributes.map( configAttribute => {
								return find( casetypeAttributes, ( attribute ) => attribute.magic_string === configAttribute.object.column_name.replace('attribute.', '') );
							}).filter(identity);

						});

					voteBarConfigReducer = composedReducer({ scope: $scope }, voteAttributesReducer, userResource)
						.reduce( ( attributes, user ) => {

							let orgUnits = get(user, 'instance.logged_in_user.legacy.parent_ou_ids').concat( get(user, 'instance.logged_in_user.legacy.ou_id')),
								roles = get(user, 'instance.logged_in_user.legacy.role_ids');

							let findAttr = ( type ) => {
								return find(attributes, ( attribute ) => {

									if ( get(attribute, 'type') === type && attribute.permissions.length ) {
										return includes( orgUnits, first(attribute.permissions).group.instance.id)
											&& includes(roles, first(attribute.permissions).role.instance.id);
									}

									return false;

								});
							};

							return {
								choice: findAttr('select') || findAttr('option') || undefined,
								comment: findAttr('textarea')
							};

						});

					voteBarConfigValueReducer = composedReducer({ scope: $scope }, voteBarConfigReducer)
						.reduce( attributes => {

							return {
								choice: {
									magic_string: get(attributes, 'choice.magic_string'),
									values: sortBy(
										get(attributes, 'choice.values', [])
											.map( value => {
												return value.active ? value : null;
											})
											.filter(identity),
										( value ) => value.sort_order
									)
								},
								comment: attributes.comment
							};

						});

					voteBarDataReducer = composedReducer({ scope: $scope }, voteBarConfigReducer, proposalReducer )
						.reduce( ( attributes, proposal ) => {

							return attributes.choice !== undefined && attributes.comment !== undefined ? {
								choice: first(get(proposal, `instance.attributes.${attributes.choice.magic_string}`)) || '',
								comment: first(get(proposal, `instance.attributes.${attributes.comment.magic_string}`)) || ''
							} : null;
						});

					fieldReducer = composedReducer({ scope: $scope }, proposalReducer, appConfigReducer, casetypeAttributesReducer )
						.reduce( ( proposal, config, casetypeAttributes ) => {

							let fieldConfig = getAttributes(config).voorstel;

							return fieldConfig
								.filter( field => field.external_name.indexOf('voorstelbijlage') === -1)
								.asMutable()
								.map( field => {

									let attribute = get(field, 'internal_name.searchable_object_id'),
										label = field.internal_name.searchable_object_label_public || field.internal_name.searchable_object_label,
										value = first(proposal.instance.attributes[field.internal_name.searchable_object_id]) || '-',
										tpl = String(value) || '-',
										type = get(find(casetypeAttributes, ( casetypeAttribute ) => casetypeAttribute.magic_string === attribute), 'type');

									if ( isArray(value) ) {

										if (value.length > 1) {
											tpl = '<ul>';

											value.map( ( el ) => {
												tpl += `<li>${el}</li>`;
											});

											tpl += '</ul>';
										} else {
											tpl = `${value.join()}`;
										}
									}

									if (type === 'date') {
										tpl = String(dateFilter(value, 'dd MMM yyyy') || '-');
									}

									switch (label) {
										case 'zaaknummer':
										value = proposal.instance.number;
										tpl = String(value);
										break;

										case 'zaaktype':
										tpl = String(proposal.instance.casetype.instance.name || '-');
										break;

										case 'vertrouwelijkheid':
										tpl = String(proposal.instance.confidentiality.mapped);
										break;

										case 'aanvrager_naam':
										label = 'Aanvrager naam';
										tpl = String(`${proposal.instance.requestor.instance.subject.instance.first_names} ${proposal.instance.requestor.instance.subject.instance.surname}`);
										break;

										case 'resultaat':
										tpl = String(proposal.instance.result || '-');
										break;
									}

									return tpl !== '' && tpl !== '-' ?
									{
										id: shortid(),
										label: label.charAt(0).toUpperCase() + label.slice(1),
										value,
										template: $sce.trustAsHtml(tpl)
									} : false;

								}).filter(identity);

						});

					styleReducer = composedReducer( { scope: $scope }, appConfigReducer )
						.reduce( config => {

							return {
								'background-color': get(config, 'instance.interface_config.header_bgcolor', '#FFF')
							};

						});

					titleReducer = composedReducer({ scope: $scope }, proposalReducer, appConfigReducer )
						.reduce( ( proposal, config ) => {

							let columns = getAttributes( config ).voorstel,
								descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
								value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || proposal.instance.casetype.instance.name || '-';

							return `${proposal.instance.number}: ${value}`;
						});

					proposalResultsReducer = composedReducer({ scope: $scope }, proposalReducer, voteAttributesReducer)
						.reduce( ( proposal, attributes ) => {

							return attributes ?
								attributes
									.asMutable()
									.map( ( attribute ) => {

										let attributeName = attribute.magic_string,
											value = first(proposal.instance.attributes[attributeName]) || '',
											tpl = String(value) || '-';

											if ( isArray(value) ) {

												if (value.length > 1) {
													tpl = '<ul>';

													value.map( ( el ) => {
														tpl += `<li>${el}</li>`;
													});

													tpl += '</ul>';
												} else {
													tpl = `${value.join()}`;
												}
											}

										return {
											id: shortid(),
											label: attribute.public_label || attribute.label,
											value,
											template: $sce.trustAsHtml(tpl)
										};

							}) : null;

						});

					noteReducer = composedReducer({ scope: $scope }, ctrl.notes() )
						.reduce( notes => notes);

					noteStyleReducer = composedReducer({ scope: $scope }, noteReducer)
						.reduce( notes => {
							return {
								paddingTop: notes ? `${notes.length * 50 + 50}px` : '50px'
							};
						});

					ctrl.getNoteStyle = noteStyleReducer.data;

					ctrl.getNotes = noteReducer.data;

					ctrl.proposalResults = proposalResultsReducer.data;

					ctrl.voteBarData = voteBarDataReducer.data;

					ctrl.voteBarConfig = voteBarConfigValueReducer.data;

					ctrl.getFields = fieldReducer.data;

					ctrl.getAttachments = ctrl.documents;

					ctrl.getStyle = styleReducer.data;

					ctrl.getTitle = titleReducer.data;

					ctrl.proposalId = ( ) => ctrl.proposal().data().instance.number;

					ctrl.getProposalReference = ( ) => ctrl.proposal().data().reference;

					ctrl.isWriteModeEnabled = ( ) => appConfigReducer.data().instance.interface_config.access === 'rw' && userResource.state() === 'resolved';

					ctrl.isVotingBarEnabled = ( ) => {
						return appConfigReducer.data().instance.interface_config.access === 'rw'
							&& userResource.state() === 'resolved'
							&& voteBarConfigValueReducer.data();
					};

					voteRightsReducer = composedReducer( { scope: $scope }, proposalReducer, userResource )
						.reduce( ( proposal, user ) => {

							// If you are an administrator, you can vote regardless
							if (
								includes(user.instance.logged_in_user.system_roles, 'Administrator')
								|| includes(user.instance.logged_in_user.system_roles, 'Zaaksysteembeheerder')
							) {
								return true;
							}

							// If you are not, the proposal needs an assignee AND a coordinator.
							// Otherwise the backend will not accept case mutations of which voting is one
							if (
								isEmpty( proposal.instance.assignee)
								|| isEmpty(proposal.instance.coordinator)
							) {
								return false;
							}

							return true;

						});

					ctrl.isVotingEnabled = voteRightsReducer.data;

					ctrl.isCaseClosed = ( ) => proposalReducer.data().instance.status === 'resolved';

					ctrl.getFileUrl = ( documentId ) => `/api/v1/case/${ctrl.proposal().data().reference}/document/${documentId}/download`;

					ctrl.isLoading = ( ) => loading;

					ctrl.handleVote = ( proposalReference, voteData ) => {

						return ctrl.proposal().mutate('VOTE_FOR_PROPOSAL', {
							proposalReference,
							voteData,
							interfaceId: appConfigReducer.data().instance.id
						})
							.asPromise();

					};

					ctrl.handleNoteAction = ( proposalReference, noteContent, noteReference ) => {

						let type,
							actionObject = JSON.parse(
								JSON.stringify(
									{
										proposalReference,
										noteContent,
										noteReference,
										interfaceId: appConfigReducer.data().instance.id
									}
								)
							);

						loading = true;
						
						if (noteReference && noteContent) {
							type = 'UPDATE_PROPOSAL_NOTE';

						} else if (!noteContent) {
							type = 'DELETE_PROPOSAL_NOTE';

						} else {
							type = 'SAVE_NEW_PROPOSAL_NOTE';

						}

						return ctrl.notes().mutate(type, actionObject)
							.asPromise()
							.then( ( ) => {
								loading = false;
							});

					};

					ctrl.previewFile = ( file ) => {

						let modalScope = $scope.$new(),
							modal;

						modal = zsModal({
							title: `Bekijk ${file.instance.name}`,
							el: $compile(`<zs-pdf-viewer url="/zaak/${ctrl.proposalId()}/document/${file.reference}/download/pdf"></zs-pdf-viewer>`)(modalScope),
							classes: 'full-screen-modal preview-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							modalScope.$destroy();
						});

					};

					ctrl.handleFileClick = ( file, event ) => {

						event.preventDefault();

						let URL,
							blobUrl,
							anchor;

						snackbarService.wait(
							'Het bestand wordt gedownload',
							{
								promise:
									$http({
										url: ctrl.getFileUrl(file.reference),
										responseType: 'blob'
									})
										.then( ( response ) => {

											URL = 'URL' in $window ? $window.URL : $window.webkitURL;

											blobUrl = URL.createObjectURL(response.data, { type: 'text/bin' });
											anchor = angular.element('<a></a>');

											anchor.attr('href', blobUrl);
											anchor.attr('download', file.instance.name);

											$document.find('body').append(anchor);

											anchor[0].click();

											$timeout( ( ) => {
	
												anchor.remove();
												URL.revokeObjectURL(blobUrl);
	
											}, 300);

										}),
								then: ( ) => '',
								catch: ( ) => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
							}
						);
						

					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.goBack = ( ) => {
						$state.go('^');
					};

				}],
				controllerAs: 'proposalDetailView'

			};
		}
		])
		.name;
