import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from './../../shared/api/resource/composedReducer';
import objectListViewModule from './objectListView';
import savedSearchesServiceModule from './../../shared/ui/zsSpotEnlighter/savedSearchesService';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import first from 'lodash/first';
import find from 'lodash/find';
import get from 'lodash/get';
import zqlEscapeFilterModule from '../../shared/object/zql/zqlEscapeFilter';

export default {
	moduleName:
		angular.module('Zaaksysteem.pdc.objectList', [
			resourceModule,
			objectListViewModule,
			snackbarServiceModule,
			savedSearchesServiceModule,
			composedReducerModule,
			zqlEscapeFilterModule
		])
		.name,
	config: [
		{
			route: {
				url: '/:objectType',
				scope: {
					appConfig: '&'
				},
				template,
				restrict: 'E',
				title: [ 'config', ( config ) => {

					return config.data().instance.interface_config.title;

				}],
				bindToController: true,
				resolve: {
					objects: [
						'$rootScope', '$q', '$state', '$stateParams', 'resource', 'composedReducer', 'config', 'snackbarService', 'zqlEscapeFilter',
						( $rootScope, $q, $state, $stateParams, resource, composedReducer, config, snackbarService, zqlEscapeFilter ) => {

						let objectType,
							objectsResource,
							objectTypeReducer,
							currentObjectReducer;

						objectTypeReducer = composedReducer({ scope: $rootScope }, config)
							.reduce( ( configuration ) => {

								return get( first(configuration.instance.interface_config.objects), 'objecttype.object_type');

							});

						if ($stateParams.objectType !== '') {
							objectType = $stateParams.objectType;
						} else {
							objectType = objectTypeReducer.data();
						}

						currentObjectReducer = composedReducer( { scope: $rootScope }, config)
							.reduce( ( configuration ) => {
								return find( configuration.instance.interface_config.objects, ( object ) => {
									return object.objecttype.object_type === objectType;
								});
							});

						objectsResource = resource( ( ) => {

							let searchQuery = $stateParams.searchQuery ? `MATCHING ${zqlEscapeFilter($stateParams.searchQuery)}` : '';
								
							return {
								url: '/api/object/search',
								params: {
									zql: `SELECT {} FROM ${get(currentObjectReducer.data(), 'objecttype.object_type')} ${searchQuery} ORDER BY pdc_name ASC`,
									zapi_num_rows: $stateParams.paging || 30
								}
							};

						}, { scope: $rootScope })

							.reduce( ( requestOptions, data ) => {

								return data ? {
									label: `Zoeken in ${get(currentObjectReducer.data(), 'objecttype_subject_attribute')}`,
									objects: data
								} : seamlessImmutable({});

							});

						return objectsResource.asPromise()
							.then(( ) => objectsResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de lijstitems. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

						}

					]
				},
				params: {
					paging: {
						squash: true,
						value: 30
					},
					searchQuery: {
						squash: true,
						value: ''
					}
				},
				controller: [ '$timeout', '$state', '$stateParams', '$scope', 'objects', 'config', ( $timeout, $state, $stateParams, $scope, objects, config ) => {

					if ($stateParams.objectType === '') {
						$state.transitionTo('objectList', { objectType: get( first(config.data().instance.interface_config.objects), 'objecttype.object_type') }, { reload: true, notify: true } );
					}

					$scope.objects = objects;

					$scope.handleLoadMore = ( ) => {
						$state.go($state.current.name, { paging: ($stateParams.paging || 30) + 30 }, { inherit: true });
					};

				}],
				controllerAs: 'objectListController'
			},
			state: 'objectList'
		}
	]
};
