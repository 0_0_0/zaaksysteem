BEGIN;

CREATE TABLE zaak_authorisation (
    id SERIAL PRIMARY KEY NOT NULL,
    zaak_id INTEGER NOT NULL REFERENCES zaak(id),
    capability TEXT NOT NULL,
    entity_id TEXT NOT NULL,
    entity_type TEXT NOT NULL,
    scope TEXT NOT NULL
);

COMMIT;
