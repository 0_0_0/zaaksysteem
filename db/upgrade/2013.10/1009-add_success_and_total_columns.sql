BEGIN;

ALTER TABLE transaction ADD COLUMN success_count INTEGER ;
ALTER TABLE transaction ADD COLUMN total_count INTEGER ;

ALTER TABLE transaction ALTER COLUMN success_count SET DEFAULT 0;
ALTER TABLE transaction ALTER COLUMN total_count SET DEFAULT 0;
ALTER TABLE transaction ALTER COLUMN error_count SET DEFAULT 0;

COMMIT;
