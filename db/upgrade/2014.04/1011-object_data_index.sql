BEGIN;

-- This is the new index that makes object search lightning fast:
DROP INDEX IF EXISTS object_data_hstore_idx;
CREATE INDEX object_data_hstore_idx ON object_data USING GIN(index_hstore);

-- These indexes make "ORDER BY" faster. Also range searches.
DROP INDEX IF EXISTS object_data_completion_idx;
DROP INDEX IF EXISTS object_data_assignee_idx;
DROP INDEX IF EXISTS object_data_casetype_description_idx;
DROP INDEX IF EXISTS object_data_recipient_name_idx;
DROP INDEX IF EXISTS object_data_regdate_idx;
DROP INDEX IF EXISTS object_data_target_date_idx;
DROP INDEX IF EXISTS object_data_case_phase_idx;
DROP INDEX IF EXISTS object_data_case_number_idx;
DROP INDEX IF EXISTS object_data_casetype_name_idx;

CREATE OR REPLACE FUNCTION hstore_to_timestamp(IN date_field VARCHAR)
   RETURNS TIMESTAMP
   LANGUAGE SQL
   IMMUTABLE
AS
$body$
  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;
$body$;


CREATE INDEX object_data_completion_idx
    ON object_data((index_hstore->'case.date_of_completion'));

CREATE INDEX object_data_assignee_idx
    ON object_data((index_hstore->'case.assignee'));

CREATE INDEX object_data_casetype_description_idx
    ON object_data((index_hstore->'casetype.description'));

CREATE INDEX object_data_recipient_name_idx
    ON object_data((index_hstore->'recipient.full_name'));

CREATE INDEX object_data_regdate_idx
    ON object_data(hstore_to_timestamp(index_hstore->'case.date_of_registration'));

CREATE INDEX object_data_target_date_idx
    ON object_data(hstore_to_timestamp(index_hstore->'case.date_target'));

CREATE INDEX object_data_case_phase_idx
    ON object_data((index_hstore->'case.phase'));

CREATE INDEX object_data_case_number_idx
    ON object_data((index_hstore->'case.number'));

CREATE INDEX object_data_casetype_name_idx
    ON object_data((index_hstore->'casetype.name'));

COMMIT;
