---
--- !!! RUN AS A SUPERUSER !!!
---

-- This is required for 1005. It makes sure that uuid_generate_v4() is loaded/present
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SET SESSION AUTHORIZATION zaaksysteem;

BEGIN;

-- 1000-pip_feedback.sql
CREATE TABLE message (
	id SERIAL PRIMARY KEY,
	message TEXT NOT NULL,
	subject_id VARCHAR,
	logging_id INTEGER NOT NULL REFERENCES logging(id),
	is_read BOOLEAN DEFAULT 'f'
);

-- 1001-documentintake_per_user.sql
ALTER TABLE file ADD COLUMN intake_owner VARCHAR;

-- 1002-config-parameter-unique.sql
ALTER TABLE config ADD constraint parameter_unique UNIQUE(parameter);

-- 1003-directory-table-fix.sql
ALTER TABLE directory ALTER COLUMN name TYPE VARCHAR(255);
ALTER TABLE directory ALTER name SET NOT NULL;

ALTER TABLE directory ADD CONSTRAINT name_case_id UNIQUE (name, case_id);

ALTER TABLE directory ADD original_name TEXT;
UPDATE directory SET original_name = name;
ALTER TABLE directory ALTER original_name SET NOT NULL;

-- 1004-add_document_intake_configs.sql
INSERT INTO config (parameter, value, advanced) SELECT 'file_username_seperator', '-', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'file_username_seperator'
    )
;
INSERT INTO config (parameter, value, advanced) SELECT 'feedback_email_template_id', NULL, 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'feedback_email_template_id'
    )
;

-- 1005-subject.sql
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS subject CASCADE;

CREATE TABLE subject (
    id 			            SERIAL PRIMARY KEY,
    uuid                    UUID UNIQUE DEFAULT uuid_generate_v4(),
    subject_type            TEXT NOT NULL CHECK (
        subject_type ~ '(employee)'
    ),
    properties              TEXT DEFAULT '{}' NOT NULL,
    settings                TEXT DEFAULT '{}' NOT NULL
);

DROP TABLE IF EXISTS user_entity CASCADE;

CREATE TABLE user_entity (
    id                      SERIAL PRIMARY KEY,
    uuid 	                UUID DEFAULT uuid_generate_v4(),
    source_interface_id     INTEGER REFERENCES interface(id),
    source_identifier       TEXT NOT NULL,
    active                  BOOLEAN DEFAULT 'f' NOT NULL,
    subject_id              INTEGER REFERENCES subject(id),
    date_created            TIMESTAMP WITHOUT TIME ZONE,
    date_deleted            TIMESTAMP WITHOUT TIME ZONE
);

-- 1006-subject-interface.sql
INSERT INTO interface (name, active, max_retries, interface_config, multiple, module) (
    SELECT 'LDAP Authenticatie',true,'10','{}', true, 'authldap' WHERE NOT EXISTS (
        SELECT * FROM interface WHERE module = 'authldap'
    )
);

-- 1007-subject-add_username_to_subject.sql
ALTER TABLE subject ADD COLUMN username VARCHAR;
UPDATE subject SET username = md5(random()::TEXT);
ALTER TABLE subject ALTER COLUMN username SET NOT NULL;

-- 1008-subject-add_properties_to_user_entity.sql
ALTER TABLE user_entity ADD COLUMN properties TEXT DEFAULT '{}';

-- 1009-bibkenmerk-values-active.sql
ALTER TABLE bibliotheek_kenmerken_values ADD COLUMN active BOOLEAN NOT NULL DEFAULT 't';
ALTER TABLE bibliotheek_kenmerken_values ADD COLUMN sort_order SERIAL;
ALTER TABLE bibliotheek_kenmerken_values ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);
ALTER TABLE bibliotheek_kenmerken ADD COLUMN version INTEGER NOT NULL default 1;

ALTER TABLE zaaktype_kenmerken ADD COLUMN version INTEGER;

-- 1010-add-zaaktype-kenmerk-required-permissions.sql
ALTER TABLE zaaktype_kenmerken add required_permissions text;

-- 1011-add-missing-config-inserts.sql
INSERT INTO config (parameter, value, advanced) SELECT 'document_intake_user', 'intake', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'document_intake_user'
    )
;
INSERT INTO config (parameter, value, advanced) SELECT 'users_can_change_password', 'off', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'users_can_change_password'
    )
;

-- 1020-add-constraint-scheduled-job-task.sql and 1032-fix-scheduled-jobs-constraint.sql
-- Explanation: 1020 wouldn't apply on some databases. So we were forced to combine the original
-- creation of the constraint and the fix for it.
ALTER TABLE scheduled_jobs DROP CONSTRAINT IF EXISTS scheduled_jobs_task_check;
ALTER TABLE scheduled_jobs ADD CONSTRAINT scheduled_jobs_task_check CHECK (((task)::TEXT ~ '^(case/update_kenmerk|case/mail)$'::TEXT));

-- 1030-zaaktype-kenmerken-add-pip-help.sql
ALTER TABLE zaaktype_kenmerken ADD help_pip TEXT;

-- 1031-zaaktype-kenmerk-rename_help_pip_help_extern.sql
ALTER TABLE zaaktype_kenmerken RENAME COLUMN help_pip TO help_extern;

COMMIT;