package Zaaksysteem::OverheidIO::BAG;
use Moose;

=head1 NAME

Zaaksysteem::OverheidIO::BAG - An Zaaksysteem OverheidIO BAG model

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::OverheidIO::Model;
    my $model = Zaaksysteem::OverheidIO::Model->new(
        schema => $schema
    );

=cut

use Zaaksysteem::Backend::Sysin::OverheidIO::BAG;
use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object

=cut

has schema => (
    is       => 'ro',
    isa      => "Zaaksysteem::Schema",
    required => 1,
);

has overheidio => (
    is => 'ro',
    isa => "Zaaksysteem::Backend::Sysin::OverheidIO::BAG",
    lazy => 1,
    builder => '_build_overheidio',
);

=head2 get_overheid_io_interface

Get the OverheidIO interface

=cut

sub get_overheid_io_interface {
    my $self = shift;

    my $rs = $self->schema->resultset('Interface')->search_active({module => 'overheidio'});
    my $interface = $rs->next;

    if ($interface) {
        if ($rs->next) {
            throw("overheidio/interface/multiple", "Multiple active OverheidIO interfaces found");
        }
        if ($interface->jpath('$.overheid_io_module_bag')) {
            return $interface;
        };
        throw("overheidio/interface/bag", "OverheidIO interface isn't configured for BAG");
    }
    throw("overheidio/interface/inactive", "No active OverheidIO interfaces found");
}

=head2 get_bag_objects_from_overheid_io

    my $query   = $c->parse_es_query_params;
    my $objects = $c->model('OverheidIO::BAG')
        ->get_bag_objects_from_overheid_io($query);

Returns an array ref with L<Zaaksysteem::Object::Types::Address> as
values based on the ES query. Only full text matches are supported at
this time.

=cut

define_profile get_bag_objects_from_overheid_io => (
    required => {
        query => 'HashRef',
    }
);

sig get_bag_objects_from_overheid_io => 'HashRef';

sub get_bag_objects_from_overheid_io {
    my ($self, $es_query) = @_;

    # validate the $es_query only;
    my $query = assert_profile($es_query)->valid->{ query };
    my $match = assert_profile($query, profile => {
        required => { match => 'HashRef' }
    })->valid->{ match };

    my $model = $self->overheidio;
    my $results = $model->search(undef,
        filter => {
            postcode             => $match->{zipcode},
            huisnummer           => $match->{street_number},
            huisnummertoevoeging => $match->{street_number_suffix},
            huisletter           => $match->{street_number_letter},
        }
    );
    return $self->_parse_overheid_to_address_objects($results);
}

sub _build_overheidio {
    my $self = shift;

    my $interface = $self->get_overheid_io_interface;
    my $model = Zaaksysteem::Backend::Sysin::OverheidIO::BAG->new(
        key => $interface->jpath('$.overheid_io_key'),
    );
    return $model;
}

sub _parse_overheid_to_address_objects {
    my ($self, $json_data) = @_;

    my $country = Zaaksysteem::Object::Types::CountryCode->new_from_name('Nederland');

    my $address_data = $json_data->{_embedded}{adres};
    my @address;
    foreach (@$address_data) {

        my $municpilality = Zaaksysteem::Object::Types::MunicipalityCode
            ->new_from_name($_->{gemeentenaam});

        push(@address, Zaaksysteem::Object::Types::Address->new(
            city          => $_->{woonplaatsnaam},
            street        => $_->{openbareruimtenaam},
            street_number => $_->{huisnummer},
            zipcode       => $_->{postcode},
            municipality  => $municpilality,
            country       => $country,

            $_->{huisnummertoevoeging} ?
                (street_number_suffix => $_->{huisnummertoevoeging}) : (),

            $_->{huisletter} ?
                (street_number_letter => $_->{huisletter}) : (),
        ));
    }
    return \@address;

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
