package Zaaksysteem::BR::Subject::Types::ExternalSubscription;
use Moose::Role;
use Zaaksysteem::Tools;

with 'Zaaksysteem::BR::Subject::Types::Roles::DB' => { -excludes => 'save_to_tables' };

=head1 NAME

Zaaksysteem::BR::Subject::Types::ExternalSubscription - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=head1 ATTRIBUTES

=head2 source_name

Implements the source_name C<ObjectSubscription> for this object.

=head2 mapping

Implements the attribute mapping for ObjectSubscription to ExternalSubscription

=cut

has source_name => (
    is      => 'ro',
    isa     => 'Str',
    default => 'ObjectSubscription',
);

has mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            id          => 'internal_identifier',
            external_id => 'external_identifier',
        };
    },
);

=head2 convert_rs_to_object_values

Modifies C<convert_rs_to_object_values> from the consuming role to map interface ID to UUID's

=cut

around 'convert_rs_to_object_values' => sub {
    my $orig = shift;
    my $self = shift;
    my $row  = shift;

    my $values = $self->$orig($row);

    $values->{config_interface_uuid} = $row->config_interface_id->uuid if $row->config_interface_id;
    $values->{interface_uuid}        = $row->interface_id->uuid;

    return $values;
};

=head2 convert_rs_to_object_values

Modifies C<convert_object_to_rs_values> from the consuming role to map interface UUID to ID's

=cut

around 'convert_object_to_rs_values' => sub {
    my $orig   = shift;
    my $self   = shift;
    my ($schema, $subject) = @_;

    my $values = $self->$orig(@_);

    my @uuid_search = ($self->interface_uuid);
    push(@uuid_search, $self->config_interface_uuid) if $self->config_interface_uuid;

    my $rs = $schema->resultset('Interface')->search_rs({uuid => \@uuid_search});

    my $stuf_family = 0;
    while (my $i = $rs->next) {
        if ($i->module eq 'stufconfig') {
            $values->{config_interface_id} = $i->get_column('id');
        }
        else {
            $values->{interface_id} = $i->get_column('id');
        }

        $stuf_family++ if $i->module =~ /^stuf/;
    }

    $values->{external_id} //= 'IN_PROGRESS';

    if ($stuf_family && !exists $values->{config_interface_id}) {
        warn dump_terse $self;
        throw('external_subscription/config_interface/missing',
            "Unable to convert object to resultset, no config interface found for StUF instance: " .
            dump_terse($self)
        );
    }

    if ($subject->subject_type eq 'person') {
        $values->{local_table} = 'NatuurlijkPersoon';
    }

    if ($subject->subject->can('_table_id')) {
        $values->{local_id} = $subject->subject->_table_id;
    }

    return $values;
};

=head2 new_from_related_row

    my $objects = Zaaksysteem::Object::Types::ExternalSubscription->new_from_row($schema->resultset('NatuurlijkPersoon')->first);

This functions returns a single subscription

=cut

sub new_from_related_row {
    my $class       = shift;
    my $rel_row     = shift;

    return unless $rel_row;
    my $os = $rel_row->subscription_id;
    return $os ? $class->new_from_row($os) : undef;
}

=head2 new_from_params

    $self->new_from_params(%opts)

Exactly the same as C<new>, with the exception that undef C<%opts> will be removed

=cut

sub new_from_params {
    my $class        = shift;
    my %opts        = @_;

    ### Do not allow undef params
    %opts           = map {
        $_ => $opts{$_}
    } grep({ defined $opts{$_} } keys %opts);

    return $class->new(%opts);
}

define_profile save_to_tables => (
    required => {
        schema  => 'Zaaksysteem::Schema',
        subject => 'Zaaksysteem::Object::Types::Subject',
    },
    optional => {
        subscription => 'Zaaksysteem::Object::Types::ExternalSubscription',
    },
);

sub save_to_tables {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    my $subject = $params->{subject};

    if ($subject->subject_type eq 'person') {
        my $np = $params->{schema}->resultset('NatuurlijkPersoon')->find($subject->subject->_table_id);
        my $subscription = $np->subscription_id;
        if (!$subject->has_external_subscription && $subscription) {
            $subscription->update({date_deleted => DateTime->now() });
            $np->disable_natuurlijk_persoon;
        }
        elsif ($subscription) {
            my $new = $params->{subscription} // $subject->external_subscription;

            if (defined $new->internal_identifier && $new->internal_identifier ne $subscription->id) {
                throw('subject/external_subscription/internal_id', "Internal subscription ID's do not match");
            }

            if ($new->config_interface_uuid ne $subscription->config_interface_id->uuid) {
                throw('subject/external_subscription/config_interface', "Config interface UUID do not match");
            }
            if ($new->interface_uuid ne $subscription->interface_id->uuid) {
                throw('subject/external_subscription/interface', "interface UUID do not match");
            }

            $subscription->update({external_id => $new->external_identifier, date_deleted => undef });
            $np->enable_natuurlijk_persoon;
        }
        else {
            my $new = $params->{subscription} // $subject->external_subscription;

            return 0 if !$new;

            my $values = $new->convert_object_to_rs_values($params->{schema}, $subject);

            my $row = $params->{schema}->resultset('ObjectSubscription')->create($values);

            # When/if we are here, the dude is authenticated and make
            # sure we activate the citizen
            $np->enable_natuurlijk_persoon;
        }
    }
    return 1;
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
