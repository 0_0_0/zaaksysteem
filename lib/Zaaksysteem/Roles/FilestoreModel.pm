package Zaaksysteem::Roles::FilestoreModel;
use Moose::Role;

use File::Spec::Functions qw(catdir);
use Zaaksysteem::Filestore::Model;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Roles::FilestoreModel - Accessor for the "Filestore" model

=head1 DESCRIPTION

This role implements one attribute, "filestore_model", which lazy-builds a
L<Zaaksysteem::Filestore::Model> instance based on configuration stored in the
associated L<Zaaksysteem::Schema>.

=head1 ATTRIBUTES

=head2 filestore_model

The L<Zaaksysteem::Filestore::Model> instance to use to store files.

=cut

has filestore_model => (
    isa     => 'Zaaksysteem::Filestore::Model',
    lazy    => 1,
    is      => 'ro',
    default => sub {
        my $self = shift;

        my $schema          = $self->result_source->schema;
        my $config          = $schema->default_resultset_attributes->{config};
        my $customer_config = $schema->customer_config;

        my $fs_config = $config->{"Plugin::Zaaksysteem::Filestore"};

        # If there's only one configured file store, it's returned as a hashref, not an arrayref of hashrefs.
        if (!$fs_config) {
            throw(
                "filestore_model/not_configured",
                "No filestore configuration found. Check zaaksysteem.conf for 'Plugin::Zaaksysteem::Filestore'."
            );
        }
        elsif (ref($fs_config) eq 'HASH') {
            $fs_config = [$fs_config];
        }

        return Zaaksysteem::Filestore::Model->new(
            local_base_path      => catdir($customer_config->{basedir}, 'storage'),
            storage_bucket       => $customer_config->{storage_bucket} // $customer_config->{instance_uuid},
            engine_configuration => $fs_config,
        );
    }
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
