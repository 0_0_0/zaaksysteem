package Zaaksysteem::CLI::NatuurlijkPersoon::Marry;
use Moose;

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::Marry - Marry Natuurlijk Personen.

=cut

use DateTime;
use Zaaksysteem::Tools;

extends 'Zaaksysteem::CLI';

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            my $options = $self->options;

            my $alpha = $self->get_natural_person_by_bsn($options->{bsn});

            my $beta = eval { $self->get_natural_person_by_bsn($options->{partner_bsn}) };

            if (!$beta) {
                $self->marry(
                    $alpha,
                    naamsgebruik          => $options->{naamsgebruik},
                    partner_a_nummer      => $options->{partner_a_nummer},
                    partner_bsn           => $options->{partner_bsn},
                    partner_voorvoegsel   => $options->{partner_voorvoegsel},
                    partner_geslachtsnaam => $options->{partner_geslachtsnaam},
                    datum_huwelijk        => $options->{datum} // DateTime->now(),
                );
            }
            else {
                $self->marry(
                    $alpha,
                    naamsgebruik          => $options->{naamsgebruik},
                    partner_a_nummer      => $beta->a_nummer,
                    partner_bsn           => $beta->bsn,
                    partner_voorvoegsel   => $beta->voorvoegsel,
                    partner_geslachtsnaam => $beta->geslachtsnaam,
                    datum_huwelijk        => $options->{datum} // DateTime->now(),
                );
            }

        }
    );

    return 1;
};

=head2 marry

Marry two natural persons.

=cut

define_profile marry => (
    required => {
        partner_bsn           => 'Int',
        partner_geslachtsnaam => 'Str',
    },
    optional => {
        partner_a_nummer    => 'Num',
        partner_voorvoegsel => 'Str',
        datum_huwelijk      => 'DateTime',
        naamsgebruik        => enum([qw[P V N E], ''])
    },
    field_filters => {
        naamsgebruik => ['uc']
    },
    defaults => { datum_huwelijk => sub { return DateTime->now() }, },
);

sub marry {
    my ($self, $np, %options) = @_;

    my $opts = assert_profile(\%options)->valid;

    $self->log->info(sprintf("Marry person %d with %d", $np->bsn, $opts->{partner_bsn}));

    $np->update(
        {
            aanduiding_naamgebruik      => $opts->{naamsgebruik},
            partner_a_nummer            => $opts->{partner_a_nummer},
            partner_burgerservicenummer => $opts->{partner_bsn},
            partner_voorvoegsel         => $opts->{partner_voorvoegsel},
            partner_geslachtsnaam       => $opts->{partner_geslachtsnaam},
            datum_huwelijk              => $opts->{datum_huwelijk},
        }
    );
}

=head2 get_natural_person_by_bsn

Get a natuurlijk persoon by virtue of the BSN.

=cut

sub get_natural_person_by_bsn {
    my $self = shift;
    my $bsn = shift;

    my $p = $self->schema->resultset('NatuurlijkPersoon')->find({burgerservicenummer => $bsn, deleted_on => undef});
    return $p if $p;
    throw("NaturalPerson/bsn/unknown", "Unable to find a natural person with BSN $bsn");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
