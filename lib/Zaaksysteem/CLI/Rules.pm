package Zaaksysteem::CLI::Rules;
use Moose;

=head1 NAME

Zaaksysteme::CLI::Rules - A rule migration script

=head1 DESCRIPTION

Migrate rules from the old style to a new style

=head1 SYNOPSIS

    # In a perl script on a machine where devops rule
    use Zaaksysteem::CLI::Rules;

    my $cli = Zaaksysteem::CLI::Rules->new();
    # Migrate the rules
    $cli->run();

=cut

extends 'Zaaksysteem::CLI';

use Zaaksysteem::Backend::Rules::Serializer;
use Zaaksysteem::Tools;

=head2 serializer

An object of the type L<Zaaksysteem::Backend::Rules::Serializer>. Lazy as hell

=cut

has serializer => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Backend::Rules::Serializer',
    default => sub {
        my $self = shift;
        return Zaaksysteem::Backend::Rules::Serializer->new(
            schema => $self->schema,
        );
    },
    lazy => 1,
);

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->assert_run;

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            my $rules = $self->schema->resultset('ZaaktypeRegel')->search_rs(
                undef,
                {order_by => { -asc => 'id' }},
            );

            while (my $rule = $rules->next) {
                $self->migrate_rule($rule);
            }

        }
    );
    return 1;

};

=head2 migrate_rule

Migrate a rule to the new structure

=cut

sub migrate_rule {
    my $self = shift;
    my $rule = shift;

    my $id = $rule->id;

    my $decoded = $self->serializer->decode($rule->settings);

    if (!$decoded) {
        $self->log->warn("Will not process empty rule $id");
        return;
    }

    my $json = $self->serializer->encode(
        $self->serializer->sanitize_rule($decoded)
    );

    $rule->update({ settings => $json });

    $self->log->debug("Updated rule $id");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
