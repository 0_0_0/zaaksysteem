package Zaaksysteem::Controller::API::Soap;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Types qw(NonEmptyStr);

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

with qw(MooseX::Log::Log4perl);

=head2 soap

C</api/soap>

Generic SOAP Endpoint

urn:Centric/ITS/GBA/v1.0/BOAanvraag/Aanvraag

=cut

sub soap : Chained('/') : PathPart('api/soap') : Args(0) : SOAP('DocumentLiteral') {
    my ($self, $c) = @_;

    my $soapaction = $c->req->header('soapaction') // '';
    $soapaction    =~ s/\s*^"//;
    $soapaction    =~ s/"\s*$//;

    if (!NonEmptyStr->check($soapaction)) {
        throw("api/soap/soap-action", "No or empty SOAPAction defined in header")
    }

    my $interface  = try {
        $self->select_interface_from_input(
            xml             => $c->stash->{soap}->envelope(),
            soapaction      => $soapaction,
            schema          => $c->model('DB')->schema,
        );
    } catch {
        $c->log->info('Error: ' . $_);
        if (blessed($_) && $_->isa('Zaaksysteem::Exception::Base')) {
            $c->stash->{soap}->fault(
                {
                    code   => '500',
                    reason => $_->type,
                    detail => $_->message
                }
            );
        } else {
            $c->stash->{soap}->fault(
                {
                    code   => '500',
                    reason => 'error/unknown',
                    detail => "$_"
                }
            );
        }

        $c->detach;
    };

    ### For sysin, make sure entry contains the working interface:
    $c->stash->{entry}      = $interface;
    $c->forward('/sysin/interface/soap/process_soap');
}

=head2 select_interface_from_input

    my $interface = $self->select_interface_from_input(
        xml             => '<xml><bunch /><of /><it />',
        soapaction      => 'urn:VENDOR/ITS/GBA/v1.0/BOAanvraag/Aanvraag'
        schema          => $c->model('DB')->schema,
    );

Returns a C<interface> row matching the given soapaction and given xml

All fields are required

=cut

define_profile select_interface_from_input => (
    required => {
        xml         => 'Str',
        soapaction  => 'Str',
        schema      => 'Any',
    },
);

sub select_interface_from_input {
    my $self    = shift;
    my $params  = assert_profile({ @_ })->valid;
    my $schema  = $params->{schema};

    my @list = Zaaksysteem::Backend::Sysin::Modules->list_modules_by_module_type($params->{schema}, 'soapapi');

    throw('api/soap/no_soap_modules', 'No modules installed of type "soapapi" in this zaaksysteem') unless @list;

    my @soapmodules = map ({ $_->name } @list);

    my @possible_interfaces = $schema->resultset('Interface')->search_active(
        { module => \@soapmodules },
    );

    for my $interface (@possible_interfaces) {
        my $valid = try {
            $interface->module_object->is_valid_soap_api(
                xml         => $params->{xml},
                soapaction  => $params->{soapaction},
                interface   => $interface,
            );
        } catch {
            if (blessed($_) && $_->isa('Zaaksysteem::Exception::Base')) {
                throw($_->type, $_->message);
            } else {
                throw('api/soap/unknown_interface_error', "$_");
            }
        };

        return $interface if $valid;
    }

    throw(
        'api/soap/unknown_soap_action',
        sprintf(
            "No interfaces found which match soapaction: '%s'",
            $params->{soapaction}
        )
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
