package Zaaksysteem::Tools::SysinModules;
use warnings;
use strict;

use Exporter qw(import);
use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES);

=head1 NAME

Zaaksysteem::Tools::SysinModules - Sugar for writing interface modules

=head1 DESCRIPTION

This module contains for common parts of interface module configurations, like
certificates.

=head1 SYNOPSIS

    use Zaaksysteem::Tools::SysinModules qw(:certificates);

    my @INTERFACE_CONFIG = (
        client_certificate(),
        ca_certificate(),
    );

    [rest of Backend::Sysin::Module boilerplate]

=head1 EXPORTS

=head2 :certificates

Import all "certificate" related (client_certificate, ca_certificate) methods.

=head2 :all

Import all functions from this module:

=over

=item client_certificate

=item client_private_key

=item ca_certificate

=item select_municipality_code

=back

=cut

our @EXPORT_OK = qw(client_certificate client_private_key ca_certificate select_municipality_code);

our %EXPORT_TAGS = (
    certificates => [qw(client_certificate client_private_key ca_certificate)],
    all          => \@EXPORT_OK,
);

=head2 client_certificate

Returns a configuration block for a client certificate. By default, the field is not required, and the name will be (interface_)"client_certificate".

This is generally used for incoming two way authenticated TLS connections.

Options:

=over

=item * name

Name to use instead of "client_certificate". Useful if multiple client
certificates need to be present in the configuration form.

=item * required

Boolean value, defaults to false.

=back

=cut

define_profile client_certificate => (
    optional => {
        name        => 'Str',
        required    => 'Bool',
        description => 'Str',
    },
    defaults => {
        name     => 'client_certificate',
        required => 0,
        description =>
            'Upload hier het certificaat van de software die deze koppeling zal aanroepen',
    },
);

sub client_certificate {
    my $opts = assert_profile({@_})->valid;

    return Zaaksysteem::ZAPI::Form::Field->new(
        name        => sprintf('interface_%s', $opts->{name}),
        type        => 'file',
        label       => 'Client certificate (inkomend; public key)',
        description => $opts->{description},
        required    => $opts->{required},
    ),
}

=head2 client_private_key

Returns a configuration block for a client private key/certificate combination
file. By default, the field is not required, and the name will be
(interface_)"client_private_key".

This is generally used for outgoing two way authenticated TLS connections.

Options:

=over

=item * name

Name to use instead of "client_private_key". Useful if multiple client
keys need to be present in the configuration form.

=item * required

Boolean value, defaults to false.

=back

=cut

define_profile client_private_key => (
    optional => {
        name        => 'Str',
        required    => 'Bool',
        description => 'Str',
    },
    defaults => {
        name     => 'client_private_key',
        required => 0,
        description =>
            'Upload hier de private key die bekend is bij de omgeving die deze koppeling aanroept.',
    },
);

sub client_private_key {
    my $opts = assert_profile({@_})->valid;

    return Zaaksysteem::ZAPI::Form::Field->new(
        name        => sprintf('interface_%s', $opts->{name}),
        type        => 'file',
        label       => 'Client certificate (uitgaand; private key)',
        description => $opts->{description},
        required    => $opts->{required},
    ),
}

=head2 ca_certificate

Returns a configuration block for a CA certificate to use when verifying
outgoing TLS connections.

This is a two-part field. The first part is named "ca_certificate_use_system",
which is a boolean option. If true, the "system" certificate store in
/etc/ssl/certs should be used. If false, the file from the second option
("ca_certificate") should be used.

This is generally used for verifying the server we connect to on outgoing TLS
connections.

Options:

=over

=item * name

Name to use instead of "ca_certificate". Useful if multiple CA certificates
need to be present in the configuration form.

This changes the name in both configuration options, so specifying name =
"foo", will lead to two options being returned, named "foo_use_system" and
"foo".

=item * description

Long description / help text to put under the "?" icon next to the configuration option.

=back

=cut

define_profile ca_certificate => (
    optional => {
        name        => 'Str',
        description => 'Str',
    },
    defaults => {
        name => 'ca_certificate',
        description =>
            'Upload hier het CA-certificaat dat gebruikt zal worden voor verificatie van uitgaande TLS-verbindingen.',
    },
);

sub ca_certificate {
    my $opts = assert_profile({@_})->valid;

    return (
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => sprintf('interface_%s_use_system', $opts->{name}),
            type        => 'checkbox',
            label       => 'Gebruik systeem-CA-certificaten',
            description => 'Zet deze optie aan als de basis-set van CA-certificaten gebruikt moet worden voor verificatie van de uitgaande TLS-verbinding.',
            required    => 0,
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => sprintf('interface_%s', $opts->{name}),
            type        => 'file',
            when        => sprintf('interface_%s_use_system === false', $opts->{name}),
            label       => 'CA-certificaat',
            description => $opts->{description},
            required    => 0,
        ),
    );
}

=head2 select_municipality_code

Creates a select box for the municipality code

=cut

sub select_municipality_code {
    return Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_municipality_code',
        type     => 'select',
        label    => 'Gemeentecode',
        required => 1,
        data     => {
            options => [
                sort({ $a->{label} cmp $b->{label} } map({
                            {
                                label => sprintf("%s (%04d)", RGBZ_GEMEENTECODES()->{$_}, $_),
                                value => $_,
                            }
                        } keys %{ RGBZ_GEMEENTECODES() })),
            ],
        },
    );
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
