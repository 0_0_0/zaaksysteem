use utf8;
package Zaaksysteem::Schema::BetrokkeneNotes;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BetrokkeneNotes

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<betrokkene_notes>

=cut

__PACKAGE__->table("betrokkene_notes");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'betrokkene_notes_id_seq'

=head2 betrokkene_exid

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene_type

  data_type: 'text'
  is_nullable: 1

=head2 betrokkene_from

  data_type: 'text'
  is_nullable: 1

=head2 ntype

  data_type: 'text'
  is_nullable: 1

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 message

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "betrokkene_notes_id_seq",
  },
  "betrokkene_exid",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene_type",
  { data_type => "text", is_nullable => 1 },
  "betrokkene_from",
  { data_type => "text", is_nullable => 1 },
  "ntype",
  { data_type => "text", is_nullable => 1 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "message",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Cfh1e/VX46B7F14blu4J0w

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

