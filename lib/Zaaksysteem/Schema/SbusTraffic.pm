use utf8;
package Zaaksysteem::Schema::SbusTraffic;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SbusTraffic

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<sbus_traffic>

=cut

__PACKAGE__->table("sbus_traffic");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'sbus_traffic_id_seq'

=head2 sbus_type

  data_type: 'text'
  is_nullable: 1

=head2 object

  data_type: 'text'
  is_nullable: 1

=head2 operation

  data_type: 'text'
  is_nullable: 1

=head2 input

  data_type: 'text'
  is_nullable: 1

=head2 input_raw

  data_type: 'text'
  is_nullable: 1

=head2 output

  data_type: 'text'
  is_nullable: 1

=head2 output_raw

  data_type: 'text'
  is_nullable: 1

=head2 error

  data_type: 'boolean'
  is_nullable: 1

=head2 error_message

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "sbus_traffic_id_seq",
  },
  "sbus_type",
  { data_type => "text", is_nullable => 1 },
  "object",
  { data_type => "text", is_nullable => 1 },
  "operation",
  { data_type => "text", is_nullable => 1 },
  "input",
  { data_type => "text", is_nullable => 1 },
  "input_raw",
  { data_type => "text", is_nullable => 1 },
  "output",
  { data_type => "text", is_nullable => 1 },
  "output_raw",
  { data_type => "text", is_nullable => 1 },
  "error",
  { data_type => "boolean", is_nullable => 1 },
  "error_message",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "modified",
  { data_type => "timestamp", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 sbus_loggings

Type: has_many

Related object: L<Zaaksysteem::Schema::SbusLogging>

=cut

__PACKAGE__->has_many(
  "sbus_loggings",
  "Zaaksysteem::Schema::SbusLogging",
  { "foreign.sbus_traffic_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nBvNZKGS2IYhgrYzBj+jrw

__PACKAGE__->add_columns('modified',
    { %{ __PACKAGE__->column_info('modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

