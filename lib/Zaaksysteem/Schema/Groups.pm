use utf8;
package Zaaksysteem::Schema::Groups;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Groups

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<groups>

=cut

__PACKAGE__->table("groups");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 path

  data_type: 'integer[]'
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "path",
  { data_type => "integer[]", is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1 },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 roles

Type: has_many

Related object: L<Zaaksysteem::Schema::Roles>

=cut

__PACKAGE__->has_many(
  "roles",
  "Zaaksysteem::Schema::Roles",
  { "foreign.parent_group_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LJ8m3GV5xvO+hTbWClpTZQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Groups::ResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Groups::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->add_columns('date_modified',
    { %{ __PACKAGE__->column_info('date_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
