package Zaaksysteem::Schema::ParkeergebiedKosten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ParkeergebiedKosten

=cut

__PACKAGE__->table("parkeergebied_kosten");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'parkeergebied_kosten_id_seq'

=head2 betrokkene_type

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 parkeergebied

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 parkeergebied_id

  data_type: 'integer'
  is_nullable: 1

=head2 aanvraag_soort

  data_type: 'integer'
  is_nullable: 1

=head2 geldigheid

  data_type: 'integer'
  is_nullable: 1

=head2 prijs

  data_type: 'real'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "parkeergebied_kosten_id_seq",
  },
  "betrokkene_type",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "parkeergebied",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "parkeergebied_id",
  { data_type => "integer", is_nullable => 1 },
  "aanvraag_soort",
  { data_type => "integer", is_nullable => 1 },
  "geldigheid",
  { data_type => "integer", is_nullable => 1 },
  "prijs",
  { data_type => "real", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:z+pBpEHhR+SnDf9FqCbpuA





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

