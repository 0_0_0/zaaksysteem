package Zaaksysteem::Backend::Sysin::Modules::AuthRestriction;

use Moose;

use Zaaksysteem::Tools;

use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::AuthRestriction - Additional safeguards for the authentication infrastructure

=head1 DESCRIPTION

=head1 METHODS

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_ip_restrictions',
        type => 'text',
        required => 1,
        label => 'IP Restricties',
        description => 'Geef hier de IPs op vanaf waar clients toegestaan zijn om in te loggen.',
        data => {
            placeholder => '127.0.0.1, 10.44.0.0/24'
        }
    )
];

use constant INTERFACE_ID => 'authrestriction';

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'Authenticatierestricties',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [],
    is_multiple => 1,
    is_manual => 1,
    retry_on_error => 1,
    allow_multiple_configurations => 0,
    is_casetype_interface => 0,
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() }, @_);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
