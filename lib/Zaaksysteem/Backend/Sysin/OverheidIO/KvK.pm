package Zaaksysteem::Backend::Sysin::OverheidIO::KvK;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::OverheidIO::KvK - A model to talk to Overheid.IO KvK

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::OverheidIO::KvK;
    my $overheidio = Zaaksysteem::Backend::Sysin::OverheidIO::KvK->new(
        key => "your developer key",
    );

=cut

extends 'Zaaksysteem::Backend::Sysin::OverheidIO::Model';

=head1 METHODS

=head2 _build_type

Builder for the C<type> attribute

=cut

sub _build_type {
    return 'kvk';
}

=head2 fieldnames

Builder for the C<fieldname> attribute
The names of the fields which the Overheid.IO will respond with

=cut

sub _build_fieldnames {
    my $self = shift;
    return [qw(
        dossiernummer
        handelsnaam
        huisnummer
        huisnummertoevoeging
        plaats
        postcode
        straat
        vestigingsnummer
    )];

}

=head2 _build_queryfields

Builder for the C<queryfields> attribute

=cut

sub _build_queryfields {
    my $self = shift;
    return [qw(
        dossiernummer
        handelsnaam
        vestigingsnummer
        subdossier
    )];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
