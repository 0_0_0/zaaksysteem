package Zaaksysteem::DB::Component::Documents;
use Moose;
use MooseX::NonMoose;
use Data::UUID;
use Params::Profile;

use Zaaksysteem::Constants;

extends 'DBIx::Class';

sub document_identifier {
    my $self    = shift;

    my $zaak_id = (
        ref($self->zaak_id)
            ? $self->zaak_id->id
            : $self->zaak_id
    );

    return $zaak_id . '.' . $self->id;
}


sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    my $ug = new Data::UUID;
    my $uuid = $ug->create();
    my $uuid_string = $ug->to_string($uuid);
    $self->{_column_data}->{uuid} = $uuid_string;

    $self->_set_search_string({insert => 1});
    $self->next::method(@_);
}


sub update {
    my $self    = shift;
    my $columns = shift;

    $self->_set_search_string;
    $self->next::method(@_);
}


sub _set_search_string {
    my ($self) = @_;

    my $search_string = $self->filename;

    if($self->id) {
        $search_string .= " " . $self->id;
    }

    $self->search_term($search_string);
    $self->search_order($search_string);
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 document_identifier

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

