package Zaaksysteem::Object::Types::Case::Route;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Case::Route - Type for 'intake-routing' of case
objects.

=head1 DESCRIPTION

When created, a L<case|Zaaksysteem::Object::Types::Case> instance receives a
route which instructs Zaaksysteem to show the object in a specific intake.

This type mainly wraps L<Zaaksysteem::Object::Types::Group> and
L<Zaaksysteem::Object::Types::Role>.

=head1 ATTRIBUTES

=head2 group

If set, references a L<group|Zaaksysteem::Object::Types::Group> instance.

=cut

has group => (
    is => 'rw',
    type => 'group',
    label => 'Group',
    embed => 1,
    traits => [qw[OR]],
    predicate => 'has_group'
);

=head2 role

If set, references a L<role|Zaaksysteem::Object::Types::Role> instance.

=cut

has role => (
    is => 'rw',
    type => 'role',
    label => 'Role',
    embed => 1,
    traits => [qw[OR]],
    predicate => 'has_role'
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns C<case/route>.

=cut

override type => sub { 'case/route' };

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>, returns a human-readable
description of the route.

=cut

override TO_STRING => sub {
    my $self = shift;

    my $group_name = $self->has_group ? $self->group->TO_STRING : 'Alle afdelingen';
    my $role_name = $self->has_role ? $self->role->TO_STRING : 'Alle rollen';

    return sprintf('%s, %s', $group_name, $role_name);
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
