package Zaaksysteem::Object::Types::Case::ACL;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(ACLScope ACLEntityType JSONBoolean ACLCapabilityList);
use Zaaksysteem::Tools;

override type => sub { 'case/acl' };

=head1 NAME

Zaaksysteem::Object::Types::Case::ACL - Implementing a class for a normalized Case ACL row

=head1 DESCRIPTION

An object class for acl's.

=head1 ATTRIBUTES

=head2 entity_type

The entity_id of this ACL, depending on the entity_type, this could be a role,group,combination, etc.

=cut

has entity_type => (
    is       => 'rw',
    isa      => ACLEntityType,
    traits   => [qw(OA)],
    label    => 'Entitype of ACL',
    required => 1,
);

=head2 entity_id

The entity_id of this ACL, depending on the entity_type, this could be a role,group,combination, etc.

=cut

has entity_id => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Entitype of ACL',
    required => 1,
);

=head2 scope

The scope of this acl, either type or instance

=cut

has scope => (
    is       => 'rw',
    isa      => ACLScope,
    traits   => [qw(OA)],
    label    => 'Scope of this ACL, on type or instance',
    required => 1,
);

=head2 capabilities

A list of capabilities for this ACL

=cut

has capabilities => (
    is       => 'rw',
    isa      => ACLCapabilityList,
    traits   => [qw(OA)],
    label    => 'A list of capabilities',
    required => 1,
);

=head2 read_only

The entity_id of this ACL, depending on the entity_type, this could be a role,group,combination, etc.

=cut

has read_only => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Cannot be altered',
    default  => 1,
    required => 0,
    coerce   => 1,
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
