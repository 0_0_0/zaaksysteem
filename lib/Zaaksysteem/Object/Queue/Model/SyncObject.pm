package Zaaksysteem::Object::Queue::Model::SyncObject;

use Moose::Role;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::SyncObject - Synchronize database rows
to L<Zaaksysteem::Object> objects.

=head1 DESCRIPTION

This role implements the C<sync_object> queue item. It's main use is the
simplified and asynchronous synchronisation of row->object data.

=head1 METHODS

=head2 sync_object

Synchronizes the row as configured by the provided data.

Requires the C<table>, and C<primary_key> fields.

=cut

define_profile sync_object => (
    required => {
        table => 'Str',
        primary_key => 'Defined'
    }
);

sub sync_object {
    my $self = shift;
    my $item = shift;

    my $data = assert_profile($item->data)->valid;

    my $rs = $self->object_model->schema->resultset($data->{ table });
    my $row = $rs->find($data->{ primary_key });

    unless (defined $row) {
        throw('queue/sync_object/object_source_row_not_found', sprintf(
            'Could not find "%s" by id "%s"',
            $data->{ table },
            $data->{ primary_key }
        ));
    }

    unless ($row->can('as_object')) {
        throw('queue/sync_object/object_source_not_supported', sprintf(
            'The component class for row %d in table "%s" does not implement the "as_object" interface',
            $data->{ primary_key },
            $data->{ table }
        ));
    }

    $self->object_model->save(object => $row->as_object);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
