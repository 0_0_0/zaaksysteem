package Zaaksysteem::Zorginstituut::Provider;
use Moose;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::UserAgent
);

=head1 NAME

Zaaksysteem::Zorginstituut::Provider - A provider class

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

use Zaaksysteem::Tools;
use HTTP::Request;
use HTTP::Headers;

=head2 di01_endpoint

An endpoint for di01 messages

=cut

has di01_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 build_request

Build a request

=cut

define_profile build_request => (
    required => {
        endpoint      => 'Str',
        xml           => 'Str',
        'soap-action' => 'Str',
    },
);

sub build_request {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $req = HTTP::Request->new(
        POST => $opts->{endpoint},
        HTTP::Headers->new(
            Content_Type => 'text/xml; charset=UTF-8',
            SOAPAction => $opts->{'soap-action'},
        ),
        $opts->{xml},
    );

    $self->log->trace($req->as_string);

    return $req;
}

=head2 send_request

Send a HTTP::Request with the defined user agent

=cut

sig send_request => 'HTTP::Request';

sub send_request {
    my $self = shift;
    my $req  = shift;

    my $res = $self->ua->request($req);
    return $self->assert_http_response($res);
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
