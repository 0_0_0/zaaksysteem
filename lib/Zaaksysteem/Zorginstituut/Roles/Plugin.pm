package Zaaksysteem::Zorginstituut::Roles::Plugin;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zorginstituut::Roles::Plugin - A role to define what a plugin module must have

=head1 DESCRIPTION

Defines that users of this role must implement the following functions:

=over

=item code

The code of message which the plugin role takes ownership of

=item description

The description of the message

=item label

A human readable label

=item shortname

A shortname

=item type

The type of message

=item version

The version of the message

=item process_soap

How the message is processed

=back

=cut

use Zaaksysteem::Tools qw(sig);

requires qw(
    code
    description
    label
    shortname
    type
    version

    answer_type

    du01_to_xml
);

sig code         => '=>Int';
sig description  => '=>Str';
sig label        => '=>Str';
sig shortname    => '=>Str';
sig type         => '=>Str';
sig version      => '=>Num';

sig answer_type => '=>Str';

sig du01_to_xml => 'XML::LibXML::XPathContext';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
