package Zaaksysteem::API::v1::Serializer::Reader::Favourite;

use Moose;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Favourites - Read rows of type L<Zaaksysteem::UserSettings::Favourites::Row>

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::Favourites->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::UserSettings::Favourites::Row> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_favourite';

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}


=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::UserSettings::Favourites::Row' }

=head2 read_favourite

Reader for widget object

=cut

sig read_favourite => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_favourite {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'favourite',
        reference => $object->id,
        instance => {
            id                      => $object->id,
            order                   => $object->order,
            reference_type          => $object->reference_type,
            reference_id            => $object->reference_id,
            label                   => $object->label,
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

