=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Appointment - Type definition for appointment objects

=head1 DESCRIPTION

This page documents the serialization of C<appointment> objects.

=head1 JSON

=begin javascript

{
    "type": "appointment",
    "reference": "5e9a6bf2-6f9a-43b8-9643-0144ef1df3a9",
    "instance": {
        "start_time": "2017-05-12T12:00:00Z",
        "end_time": "2017-05-12T13:00:00Z",
        "plugin_type": "spoof",
        "plugin_data": {
            "internal_appointment_data": "this can be anything"
        }
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 start_time E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Start date and time of the appointment.

=head2 end_time E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

End date and time of the appointment.

=head2 plugin_type E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Short name of the appointment provider used to create this appointment (e.g.
"qmaticv6", "supersaas")

=head2 plugin_data E<raquo> C<object>

Internal data the appointment provider can use to manage the appointment in the remote
system. This should not be touched by other users.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
