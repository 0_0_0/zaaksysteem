=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Session - Session API reference
documentation

=head1 NAMESPACE URL

    /api/v1/session

=head1 DESCRIPTION

This page documents the endpoints within the C<session> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET /current>

Returns a L<session|Zaaksysteem::Manual::API::V1::Types::Session> instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
