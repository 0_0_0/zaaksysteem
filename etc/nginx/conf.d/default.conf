server {
    listen 80 default_server;
    server_name development.zaaksysteem.nl localhost;

    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2 default_server;

    ssl_certificate /etc/nginx/ssl/server.crt;
    ssl_certificate_key /etc/nginx/ssl/server.key;
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_prefer_server_ciphers on;
    add_header Strict-Transport-Security max-age=15768000;

    rewrite ^/tpl/zaak_v1/nl_NL/css/.*?common.css$ /page/minified/common/css/common.css last;
    rewrite ^/html/(.*)$                           /html/nl/$1                          last;

    error_log /dev/stderr debug;

    location / {
        include fastcgi_params;
        fastcgi_param SCRIPT_NAME /;
        fastcgi_pass zs_backend;
    }

    # Rewriting magic for "BBV App" and "Intern" ("new frontend") stuff
    # and MOR
    location ~ ^/(?<app>bbv|intern|mor)$ {
        rewrite /(bbv|intern|mor)$ /$1/ last;
    }
    location ~ ^/(?<app>bbv|intern|mor) {
        rewrite /(bbv|intern|mor)(.*) /assets/$1$2 break;

        proxy_pass http://zs_frontend;
        proxy_redirect off;

        proxy_intercept_errors on;
        error_page 404 =200 /assets/$app/index.html;
    }

    location ~ ^/(assets|css|data|examples|favicon\.ico|flexpaper|fonts|html|images|js|offline|partials|pdf\.js-with-viewer|robots.txt|tpl|webodf) {
        proxy_pass http://zs_frontend;
        proxy_redirect off;
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ^~ /\.ht {
        deny all;
    }

    client_max_body_size 128M;
}

upstream zs_frontend {
    server frontend:9084;
}
upstream zs_backend {
    server backend:9083;
}
