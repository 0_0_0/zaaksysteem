package Zaaksysteem::Test::OverheidIO::BAG;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::OverheidIO::BAG - Test OverheidIO model for BAG

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::OverheidIO::BAG

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::OverheidIO::BAG;

sub test_overheid_io_bag {

    my $bag = Zaaksysteem::OverheidIO::BAG->new(
        schema => mock_dbix_schema,
    );

    # Mock the interface call, as it uses the schema object with all
    # kinds of nasty side effects for testing
    no warnings qw(redefine once);
    local *Zaaksysteem::OverheidIO::BAG::get_overheid_io_interface = sub {
        return mock_interface;
    };

    # Mock the OverheidIO call
    my $search_term;
    my %args;
    local *Zaaksysteem::Backend::Sysin::OverheidIO::BAG::search = sub {
        my $self = shift;
        $search_term = shift;
        %args = @_;
        return mock_overheid_io_bag->search;
    };
    use warnings;

    my $result = $bag->get_bag_objects_from_overheid_io(
        {
            query => { match => { zipcode => '1011PZ' }}
        }
    );

    is(@$result, 2, "Got two addresses");
    isa_ok($result->[0], "Zaaksysteem::Object::Types::Address");

    isa_ok($result->[0]->country, "Zaaksysteem::Object::Types::CountryCode");
    is($result->[0]->country->label, "Nederland", "Nederlands adres");

    isa_ok($result->[0]->municipality, "Zaaksysteem::Object::Types::MunicipalityCode");
    is($result->[0]->municipality->label, "Amsterdam", "Amsterdams adres");

    isa_ok($result->[1]->municipality, "Zaaksysteem::Object::Types::MunicipalityCode");
    is($result->[1]->municipality->label, "Hilversum", "Hilversums adres");

    is($search_term, undef, "No search terms defined");
    cmp_deeply(
        \%args,
        {
            filter => {
                postcode             => '1011PZ',
                huisletter           => undef,
                huisnummer           => undef,
                huisnummertoevoeging => undef
            }
        },
        "Only filters: minimumish required"
    );

    {
        $bag->get_bag_objects_from_overheid_io(
            {
                query => {
                    match => {
                        zipcode              => '1011PZ',
                        street_number        => 1,
                        street_number_suffix => 'b',
                        street_number_letter => 'a'
                    }
                }
            }
        );

        is($search_term, undef, "No search terms defined");

        cmp_deeply(
            \%args,
            {
                filter => {
                    postcode             => '1011PZ',
                    huisletter           => 'a',
                    huisnummer           => 1,
                    huisnummertoevoeging => 'b',
                }
            },
            "Only filters: with everything defined"
        );
    }

}


1;

__END__

=head1 NAME

Zaaksysteem::Test::OverheidIO::BAG - A OverheidIO BAG test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
