package Zaaksysteem::Test::Mocks;
use Exporter 'import';

use Mock::Quick;

use warnings;
use strict;
use feature 'state';
use DateTime;
use Scalar::Util qw/blessed/;
use Sub::Override;
use Test::Mock::One;
use Zaaksysteem::Tools::RandomData qw(generate_bsn);

our @EXPORT = qw(
    mock_one
    override

    mock_dbix_schema
    mock_dbix_resultset
    mock_dbix_component

    mock_moose_class

    mock_case

    mock_interface
    mock_transaction
    mock_transaction_record

    mock_overheid_io_bag

    mock_natuurlijk_persoon
    mock_betrokkene
);

=head1 NAME

Zaaksysteem::Test::Mocks - Basic test mocks for Zaaksysteem-specific objects/classes

=head1 DESCRIPTION


=head1 SYNOPSIS

    my $fake = mock_case();
    $fake->id;

=head1 EXPORTED FUNCTIONS

=head2 mock_one

Returns a L<Test::Mock::One> object

=cut

sub mock_one {
    return Test::Mock::One->new(@_);
}

=head2 override

Returns a L<Sub::Override> object

=cut

sub override {
    return Sub::Override->new(@_);
}

=head2 mock_case

Returns a mock case, that subclasses C<Zaaksysteem::Schema::Zaak> (so ISA
checks work)

=cut

sub mock_case {
    my $opts = shift;
    state $case_id_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::Zaak',
        -with_new => 1,

        id => ++$case_id_counter,

        trigger_logging => sub { return },
        create_message_for_behandelaar => sub { return },

        %$opts,

    );

    my $obj = $class->new(@_);

    return $obj;
}

=head2 mock_moose_class

    my $role = mock_moose_class(
        {
            roles   => ['Zaaksysteem::BR::Subject::Types::Person'],
        },
        {
            constructor_param1 => 'value1',
        }
    );

Mocks a Moose Role

=cut

sub mock_moose_class {
    my ($opts, $constructor_params)    = @_;
    $constructor_params //= {};

    return Moose::Meta::Class->create_anon_class(
        %$opts,
    )->new_object(%$constructor_params);
}

=head2 mock_dbix_schema

    my $schema = mock_dbix_schema();

Returns a mock schema, that has the following capabilities

=over

=item method: resultset

    $schema->resultset('Something');

=back

=cut

sub mock_dbix_schema {
    my $class = qclass(
        -with_new => 1,
        -subclass => 'Zaaksysteem::Schema',
        resultset => sub {
            my ($self, $name) = @_;

            return mock_dbix_resultset(resultset => $name);
        },
        default_resultset_attributes => {},
        storage => _mock_storage(),
    );

    return $class->package->new(@_);
}

sub _mock_storage {

    my $class = qclass(
        -subclass => 'DBIx::Class::Storage',
        -with_new => 1,

        dbh_do => sub { return 1 },
        is_datatype_numeric => sub { return 0 },

    );
    return $class->new(@_);
}

=head2 mock_dbix_resultset

    my $rs  = mock_dbix_resultset(resultset => 'Queue');

Mocks a resultset, and loads the resultset_class in it.

=cut

sub mock_dbix_resultset {
    my %opts        = @_;
    my $name        = $opts{resultset};

    my $class   = 'Zaaksysteem::Schema::' . $name;
    eval  "use $class;";

    my $zschema         = $class->new;
    my $resultset_class = $zschema->result_source->resultset_class;
    my $result_class    = $zschema->result_source->result_class;

    $zschema->result_source->schema(mock_dbix_schema);

    my @controls;
    push(@controls, qclass(
        -takeover       => $result_class,
        discard_changes => sub { return shift },        # Make sure we leave alone the database...
        update          => sub {
            my ($self, $params) = @_;
            return 1 unless $params;

            $self->$_($params->{$_}) for keys %$params;

            return $self;
        },
    ));

    my $rsclass = qclass(
        -subclass     => $resultset_class,
        -with_new     => 1,
        result_source => $zschema->result_source,
        result_class  => $result_class,
        control       => \@controls, # Keep the Mock override alive, as long as this rs lives
        create => sub {
            return mock_dbix_component(@_);
        },
        attrs => {},
    );

    return $rsclass->package->new();
}

=head2 mock_dbix_component

    my $row  = mock_dbix_component('Queue', { id => 55, label => 'blabla' });

Mocks a row, makes sure the component is loaded on it

=cut

sub mock_dbix_component {
    my ($rs, $params)   = @_;
    $rs                 = blessed($rs) ? $rs : mock_dbix_resultset(resultset => $rs);

    my $component       = $rs->new_result($params);
    $component->{_inflated_column} = $params;       ### When inflating, DBIx tries to connect to a DB. This skips it.

    return $component;
}

sub mock_betrokkene {
    my $opts = shift // {};
    state $betrokkene_id = 0;

    my $bsn = delete $opts->{bsn} // generate_bsn();
    my $class = qclass(
        -subclass => 'Zaaksysteem::Betrokkene::Object',
        -with_new => 1,

        id => ++$betrokkene_id,

        geslachtsaanduiding => 'M',
        bsn                 => $bsn,
        burgerservicenummer => $bsn,
        gm_np_id            => mock_natuurlijk_persoon({bsn => $bsn}),
        %{$opts},
    );

    return $class->new(@_);
}

sub mock_natuurlijk_persoon {
    my $opts = shift // {};
    state $natuurlijk_persoon_id = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::NatuurlijkPersoon',
        -with_new => 1,

        id => ++$natuurlijk_persoon_id,
        result_source => qobj( name => "natuurlijk_persoon", schema => mock_dbix_schema(), ),

        geslachtsaanduiding => 'M',
        bsn                 => generate_bsn(),
        geboortedatum       => DateTime->new(
            year => 1935,
            month => 7,
            day => 6,
            time_zone => 'Asia/Shanghai',
        ),
        geslachtsnaam => 'Gyatso',
        adres_id => {
            functie_adres => 'W',
            huisnummer => 90,
            postcode   => '1051JL',
            straatnaam => 'H.J.E Wenckebachweg',
            woonplaats => "Amsterdam",
        },

        as_object => qmeth {
            return 1;
        },

        _build_voorletters => qmeth {
            return 'T'
        },

        get_column => qmeth {
            my $self = shift;
            my $key = shift;
            if ($self->can($key)) {
                return $self->$key;
            }
            return;
        },

        %{$opts},
    );

    return $class->new(@_);
}

sub mock_transaction {
    state $transaction_id_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::Transaction',
        -with_new => 1,

        id => ++$transaction_id_counter,
    );

    my $obj = $class->new(@_);
    return $obj;
}

sub mock_transaction_record {
    state $transaction_record_counter = 0;

    my $class = qclass(
        -subclass => 'Zaaksysteem::Schema::TransactionRecord',
        -with_new => 1,

        id => ++$transaction_record_counter,
        -attributes => [ qw(input output) ],
    );

    my $obj = $class->new(@_);
    return $obj;
}

sub mock_interface {
    my $opts = shift;
    state $interface_id = 0;

    #use Data::Dumper;
    #die Dumper $opts->{get_mapped_attributes_from_case}->();

    my $class = qclass(
        -subclass => 'Zaaksysteem::Model::DB::Interface',
        -with_new => 1,

        id => ++$interface_id,
        -attributes => [ qw(input output) ],
        jpath => sub { return 'foo' },
        get_mapped_attributes_from_case => qmeth { return { } },
        get_config_interface => qmeth { return {} },

        %{$opts},
    );

    my $obj = $class->new(@_);
    return $obj;
}

sub mock_overheid_io_bag {

    return qclass(
        -subclass => 'Zaaksysteem::Backend::Sysin::OverheidIO::BAG',
        -with_new => 1,

        search => qmeth {
            return {
                _embedded => {
                    adres => [
                        {
                            gemeentenaam       => "Amsterdam",
                            woonplaatsnaam     => "Amsterdam",
                            openbareruimtenaam => "Testsuitestraat",
                            huisnummer         => '42',
                            postcode           => '1011PZ',
                        },
                        {
                            gemeentenaam         => "Hilversum",
                            woonplaatsnaam       => "Amsterdam",
                            openbareruimtenaam   => "Testsuitestraat",
                            huisnummer           => '42',
                            huisletter           => 'A',
                            huisnummertoevoeging => "HS",
                            postcode             => '1011PZ',
                        }
                    ],
                }
            };
        }
    )->new;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
