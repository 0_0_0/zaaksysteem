package Zaaksysteem::Test::Zaken::Model;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Model - Test Zaaksysteem::Test::Zaken::Model

=head1 DESCRIPTION

Test the new case model used by api/v1

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Model

=cut

use DateTime;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Test;
use Zaaksysteem::Tools::RandomData qw(:fake :uuid);
use List::Util qw(all);

use Zaaksysteem::Zaken::Model;

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Zaken::Model');
}

sub _subname {
    my $sub = shift;
    return "Zaaksysteem::Zaken::Model::$sub";
}

sub _get_model {

    my $model = Zaaksysteem::Zaken::Model->new(
        schema        => mock_one('X-Mock-ISA' => 'Zaaksysteem::Schema'),
        object_model  => mock_one('X-Mock-ISA' => 'Zaaksysteem::Object::Model'),
        subject_model => mock_one('X-Mock-ISA' => 'Zaaksysteem::BR::Subject'),
        queue_model   => mock_one('X-Mock-ISA' => 'Zaaksysteem::Object::Queue::Model'),
        queue         => mock_one('X-Mock-ISA' => 'Zaaksysteem::Queue'),
        @_,
    );
    isa_ok($model, "Zaaksysteem::Zaken::Model");
    return $model;
}

sub test_create_zaak {

    my $create_zaak_args;
    my $model = _get_model(
        schema => mock_one(
            create_zaak => sub { $create_zaak_args = shift }
        )
    );

    my $now = DateTime->now();
    my $override = override('DateTime::now' => sub { return $now->clone });

    my %args = (
        casetype        => mock_one(id => 400),
        confidentiality => 'public',
        source          => 'webform',
        requestor       => mock_one(old_subject_identifier => 10),
        values          => [],
    );

    my %expect = (
        zaaktype_id      => $args{casetype}->id,
        aanvraag_trigger => 'extern',
        confidentiality  => $args{confidentiality},
        contactkanaal    => $args{source},
        registratiedatum => $now,
        kenmerken        => $args{values},
        aanvragers       => [
            {
                betrokkene  => $args{requestor}->old_subject_identifier,
                rol         => 'Aanvrager',
                verificatie => 'n/a',
            }
        ],
    );

    $model->_create_case(\%args);
    cmp_deeply($create_zaak_args, \%expect,
        "Passed correct arguments to create_zaak"
    );

    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_one(
                    resultset => {
                        create_zaak => sub { die "Foo" }
                    }
                )
            );
            $model->_create_case(\%args);
        },
        qr#case/create_failed: Case creation failed, unable to continue.#,
        "Throws errors like it should",
    );
}

sub test_get_casetype_object {

    my $uuid = generate_uuid_v4;

    throws_ok(
        sub {
            my $object_model = mock_one(retrieve => sub { die "Foo" });
            my $model = _get_model(object_model => $object_model);

            $model->get_casetype_object($uuid);
        },
        qr#case/casetype/retrieval_fault:#,
        "Casetype with $uuid cannot be retreived"
    );

    throws_ok(
        sub {
            my $object_model = mock_one(retrieve => undef);
            my $model = _get_model(object_model => $object_model);
            $model->get_casetype_object($uuid);
        },
        qr#case/casetype_not_found#,
        "Casetype with $uuid cannot be found"
    );

    lives_ok(
        sub {
            my $object_model = mock_one(retrieve => 1);
            my $model = _get_model(object_model => $object_model);
            $model->get_casetype_object($uuid);
        },
        "Casetype with $uuid could be found"
    );
}

sub test_assert_casetype {

    my $model = _get_model;
    my $active = 0;
    my $casetype = mock_one(active => sub { return $active }, zaaktype_node_id => 1);

    my $override = override(
        _subname('_find_casetype_by_id') => sub { return $casetype });

    throws_ok(
        sub {
            $model->assert_casetype($casetype);
        },
        qr#api/v1/case/casetype/node/inactive:#,
        "Inactive casetype found"
    );

    lives_ok(
        sub {
            $active = 1;
            $model->assert_casetype($casetype);
        },
        "Casetype found and active"
    );

}

sub test_assert_values {

    my $model = _get_model;

    lives_ok(
        sub {
            $model->assert_values();
        },
        "Undef is allowed (creator doesn't fill in a thing)"
    );

    my %values = ();
    lives_ok(
        sub {
            $model->assert_values(\%values);
        },
        "Empty values are allowed"
    );

    %values = (
        foo => [qw(bar baz)],
        bar => [undef],
    );

    lives_ok(
        sub {
            $model->assert_values(\%values);
        },
        "Values wrapped in an array are values"
    );

    $values{bar} = { foo => 'bar' };
    throws_ok(
        sub {
            $model->assert_values(\%values);
        },
        qr#case/create/values/non_array:#,
        "HashRef's are not allowed"
    );

    $values{bar} = undef;
    throws_ok(
        sub {
            $model->assert_values(\%values);
        },
        qr#case/create/values/non_array:#,
        "undef values are also not allowed"
    );

    TODO: {
        local $TODO
            = "Fix me, this will break later on after getting case attributes and other DB queries";
        $values{bar} = [{}];
        throws_ok(
            sub {
                $model->assert_values(\%values);
            },
            qr#case/create/values/non_array:#,
            "nested values must be an array"
        );
    }
}

sub test_assert_requestor {

    my $model = _get_model;

    my $preset_client = 42;
    my $node          = mock_one(
        preset_client => sub { return $preset_client }
    );
    my $override = override(_subname('_get_subject_by_legacy_id') => sub { shift; return shift });

    lives_ok(
        sub {
            $model->_assert_requestor($node);
        },
        "assert_requestor found something",
    );

    throws_ok(
        sub {
            $preset_client = undef;
            $model->_assert_requestor($node);

        },
        qr#api/v1/case/requestor_unresolvable#,
        "Unable to find default requestor"
    );

}

sub test_find_assignee {

    my $model = _get_model;
    my $properties = { preset_owner_identifier => "assignee found" };

    my $node = mock_one(properties => sub { return $properties });
    my $override = override(
        _subname('_get_subject_by_legacy_id') => sub { shift; return shift });

    is($model->_find_assignee($node), "assignee found", "Assignee found");

    $properties = {};
    is($model->_find_assignee($node), undef, "No assignee found");
}

sub test_assert_routing {

    my $model = _get_model;
    my $override
        = override(_subname('_get_group') => sub { shift; return shift });
    $override->replace(
        _subname('_get_role') => sub { shift; return shift }
    );

    my ($group, $role)
        = $model->assert_routing(
        { route => { group_id => 42, role_id => 666 } });
    is($group, 42,  "Got the group");
    is($role,  666, "Got the role");

    throws_ok(
        sub {
            $model->assert_routing({ route => {} });
        },
        qr#Validation of profile failed#,
        "Routing information is required"
    );

    lives_ok(
        sub {
            my ($group, $role) = $model->assert_routing({});
            is($group, undef, "Got the group");
            is($role,  undef, "Got the role");
        },
        "No routing information is ok"
    );

}

sub test_get_case_attributes {

    my $rs = mock_one(search_rs => sub { return \@_ });
    my $model = _get_model;

    my $answer = $model->get_case_attributes($rs);
    my $expect = [
        {
            'bibliotheek_kenmerken_id'            => { '!=' => undef },
            'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    ];

    cmp_deeply($answer, $expect, "Got the case attributes");

    my ($s, $o, @ms);
    my $node = mock_one(
        search_by_magic_strings => sub {
            @ms = @_;
            return mock_one(
                search_rs => sub {
                    ($s, $o) = @_;
                }
            );
        }
    );

    $model->_get_case_attributes($node, [qw(magic)]);
    cmp_deeply(\@ms, ['magic'], "Search on magic strings");
    cmp_deeply($s, { 'zaak_status_id.status' => 1, }, "Correct search");
    cmp_deeply($o, { prefetch => 'zaak_status_id' }, "Correct options");

}

sub test_add_case_files {

    my @files;
    my $user = mock_one(
        betrokkene_identifier => 'betrokkene-medewerker-666');
    my $mock = mock_one(
        id            => 42,
        original_name => "Ryuk",
        resultset     => { file_create => sub { push(@files, shift) }, },
    );

    my $docs = {
        1 => { filestore => $mock, attribute => $mock },
        2 => { filestore => $mock, name      => "Foo", metadata => 'foo' },
    };

    my $case   = mock_one(id => 42);
    my $expect = [
        {
            'case_document_ids' => [42],
            'db_params'         => {
                'accepted'     => 1,
                'created_by'   => 'betrokkene-medewerker-666',
                'filestore_id' => 42
            },
            'disable_message' => 1,
            'name'            => 'Ryuk'
        },
        {
            'db_params' => {
                'accepted'     => 1,
                'created_by'   => 'betrokkene-medewerker-666',
                'filestore_id' => 42
            },
            'disable_message' => 1,
            'name'            => 'Foo',
            'metadata'        => 'foo',
        },
    ];

    my $model = _get_model(user => $user, schema => $mock);
    $model->_add_case_file($case, $docs->{1});
    $model->_add_case_file($case, $docs->{2});

    is(delete $files[0]->{db_params}{case_id}, 42, "Case ID for file 1 is correct");
    is(delete $files[1]->{db_params}{case_id}, 42, "Case ID for file 2 is correct");

    cmp_deeply(\@files, $expect, "Document numba two created");

    my $number = $model->add_case_files($case, $docs);
    is($number, 2, "One file added");
}

sub test_update_case_files {

    my (@files, @metadata);
    my $mock = mock_one(
        update_properties => sub { push(@files,    shift); },
        update_metadata   => sub { push(@metadata, shift); },
        original_name     => "Ruyk",
        id                => 42,
        as_object         => 666,
    );

    my $docs = {
        1 => { filestore => $mock, attribute => $mock, file => $mock },
        2 => {
            file      => $mock,
            filestore => $mock,
            name      => "Foo",
            metadata  => 'foo'
        },
    };
    my $case   = mock_one(id => 666);
    my $expect = [
        {
            case_document_ids => [42],
            accepted          => 1,
            subject           => 666,
        },
        {
            accepted => 1,
            subject  => 666,
            name     => "Foo",
        },
    ];

    my $model = _get_model(user => $mock);
    $model->_update_case_file($case, $docs->{1});
    $model->_update_case_file($case, $docs->{2});

    is(delete $files[0]->{case_id}, 666, "Case ID for file 1 is correct");
    is(delete $files[1]->{case_id}, 666, "Case ID for file 2 is correct");

    cmp_deeply(\@files, $expect,
        "Documents updated with the correct arguments");
    cmp_deeply(\@metadata, ['foo'], "Document metadata is correct");

    my $number = $model->update_case_files($case, $docs);
    is($number, 2, "Two files updated");

}

sub test_relate_case_subjects {

    my $model = _get_model;
    my $relate_args;
    my $case = mock_one(
        betrokkene_relateren => sub {
            $relate_args = shift;
        }
    );
    my $params = { related => [{ some => 'object' }] };

    $model->relate_case_subjects($case, $params);
    cmp_deeply($relate_args, { some => 'object' }, "betrokkene relateren");
}

sub test_build_queue {
    my $model = _get_model;
    isa_ok($model->_build_queue, "Zaaksysteem::Queue");
}

sub test_get_notification_template_from_config {

    my $id = 42;
    my $model = _get_model(schema => mock_one(resultset => { get => sub { $id } }));

    my $template_id = $model->_get_notification_template_from_config();
    is($template_id, 42, "Got the notification template from the config table");

    $id = undef;
    throws_ok(
        sub {
            $model->_get_notification_template_from_config();
        },
        qr#case/create/notifcation/failed#,
        "Unable to get the notifcation template from the config"
    );
}

sub test_get_notification_template {

    my $id = 42;
    my $model = _get_model(schema => mock_one(resultset => { find => sub { $id } }));
    my $template = $model->_get_notification_template();
    is($template, 42, "Got the notification template from the library");

    $id = undef;
    throws_ok(
        sub {
            $model->_get_notification_template();
        },
        qr#case/create/notifcation/failed/id_not_found#,
        "Unable to get the notifcation template from the config"
    );
}

sub test_send_case_assignment_mail {

    my $model = _get_model;
    my $args;
    my $case = mock_one(
        mailer => { send_case_notification => sub { $args = shift; }, },
        behandelaar_object => { email => sub { return 'foo@bar.nl' }, }
    );

    my $override = override(_subname('_get_notification_template') => sub { return 'foo' });

    $model->send_case_assignment_mail($case);
    cmp_deeply(
        $args,
        { notification => 'foo', recipient => 'foo@bar.nl' },
        "Send case notication to the correct person"
    );

    throws_ok(
        sub {
            $case = mock_one(email => sub { return });
            $model->send_case_assignment_mail($case);
        },
        qr#case/create/notifcation/recipient_email#,
        "No email send due to missing address"
    );
}

sub test_set_case_allocation {

    my $model = _get_model;
    my $hijack;
    my %compare_stuf;
    my $coo = 0; # Coordinator on case
    my $case = mock_one(
        case_actions => {
            hijack_allocation_action => sub { $hijack = shift; }
        },
        set_behandelaar => sub { $compare_stuf{behandelaar} = shift},
        coordinator     => sub { return $coo },
        set_coordinator => sub { $compare_stuf{coordinator} = shift},
        open_zaak       => sub { $compare_stuf{open_zaak} = shift},
        wijzig_route    => sub { $compare_stuf{route_change} = shift},
    );

    my $override = override(_subname('send_case_assignment_mail') => sub {
            $compare_stuf{send_email} = 1;
    });

    my $params = {
        route                 => { group => 42, role => 666 },
        send_assignment_email => 1,
    };

    $model->set_case_allocation($case, $params);
    cmp_deeply($hijack, { role => 666, group => 42 }, "Hijacked the case action");

    $hijack = undef;

    $params->{assignee} = mock_one(old_subject_identifier => 666);
    $params->{route}{group} = mock_one(group_id => 42);
    $params->{route}{role} = mock_one(role_id => 666);

    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
            coordinator  => 666,
            send_email   => 1,
            route_change => {
                route_ou                 => $params->{route}{group}->group_id,
                route_role               => $params->{route}{role}->role_id,
                change_only_route_fields => 1,
            }

        },
        "Routing works for assignee"
    );

    $coo = 1;
    $params->{send_assignment_email} = 0;
    %compare_stuf = ();

    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
            route_change => {
                route_ou                 => $params->{route}{group}->group_id,
                route_role               => $params->{route}{role}->role_id,
                change_only_route_fields => 1,
            }

        },
        "Did not change the coordinator and not sending the email"
    );

    delete $params->{route};
    %compare_stuf = ();
    $model->set_case_allocation($case, $params);
    is($hijack, undef, "Did not hijack the case routing");
    cmp_deeply(
        \%compare_stuf,
        {
            behandelaar  => 666,
        },
        "Only set the assignee",
    );

}

sub test_update_contact_details {

    my $model = _get_model;

    my $case = mock_one(
        aanvrager_object => mock_one(
            'X-Mock-ISA'    => 'Zaaksysteem::Betrokkene::Object::Medewerker',
            'X-Mock-Strict' => 1
        )
    );

    lives_ok(sub {
        $model->update_contact_details($case, {})
    }, "We are not calling anyone: tested by X-Mock-Strict");

    my %compare;

    $case = mock_one(
        aanvrager_object => mock_one(
            'X-Mock-ISA' => sub {
                my $type = shift;
                return $type eq 'Zaaksysteem::Betrokkene::Object::Medewerker'
                    ? 0
                    : 1;
            },
            'X-Mock-Strict' => 1,
            email           => sub { $compare{email} = shift },
            mobiel          => sub { $compare{gsm} = shift },
            telefoonnummer  => sub { $compare{phone} = shift },
        ),
    );
    $model->update_contact_details($case,
        { email_address => 'foo@bar', phone_number => 'baz', mobile_number => 'foo' });
    cmp_deeply(
        \%compare,
        { email => 'foo@bar', gsm => 'foo', phone => 'baz' },
        "Contact details updated"
    );
}

sub test_find_casetype_by_id {

    my $model = _get_model(schema => mock_one(resultset => { find => 1 }));

    lives_ok(
        sub {
            $model->_find_casetype_by_id(42);
        },
        "found the casetype"
    );
    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_one(
                    resultset => {
                        find => sub { die "Foo" }
                    }
                )
            );
            $model->_find_casetype_by_id(42);
        },
        qr#api/v1/case/casetype/node/retrieval_fault#,
        "Could not find the casetype by ID",
    );
}

sub test_prepare_case_arguments {

    my $model = _get_model;

    my %arguments = (
        casetype_id     => generate_uuid_v4,
        confidentiality => 'public',
        source          => 'webformulier',
        requestor       => {},
        values          => [],

    );

    my $override = override(_subname('assert_subjects') => sub { shift; return ( foo=> 'bar' )});
    my @methods = qw(get_casetype_object assert_values _get_case_attributes assert_case_attributes assert_documents get_document_attributes
        normalize_attribute_value
        assert_contact_details);
    foreach my $method (@methods) {
        $override->replace(_subname($method) => sub { shift; return $method });
    }
    $override->replace(
        _subname('assert_routing') => sub { return (42, 666) });
    $override->replace(
        _subname('assert_casetype') => sub { return mock_one() });
    $override->replace(
        _subname('_get_case_attributes') => sub { return mock_one() });
    $override->replace(
        _subname('assert_values') => sub { return  (foo => 'bar')});

    my $rv = $model->prepare_case_arguments(\%arguments);
    is(ref $rv, 'HASH', "Got a hash");

    # Test::Mock::One doesn't really cmp well
    delete $rv->{casetype};

    my $expect = {
        foo             => 'bar',
        confidentiality => 'public',
        contact_details => 'assert_contact_details',
        documents       => 'assert_documents',
        open            => 0,
        routing         => {
            group => 42,
            role  => 666
        },
        source => 'webformulier',
        values => 'assert_case_attributes',

        #  casetype  => bless({}, 'Test::Mock::One'),
    };

    cmp_deeply($rv, $expect, "Got the expected outcome of prepare_case_arguments");
}

sub test_assert_document_ids {

    my $model = _get_model;

    {
        my $uuids = $model->_assert_document_ids();
        is(@$uuids, 0, "No UUIDS's found");
    }

    throws_ok(
        sub {
            $model->_assert_document_ids("foo");
        },
        qr#case/file/uuid/syntax#,
        "No valid UUID"
    );

    {
        my $uuid  = generate_uuid_v4;
        my $uuids = $model->_assert_document_ids($uuid);
        cmp_deeply($uuids, [ $uuid ], "found the corect uuid");
    }

    {
        my $uuid   = generate_uuid_v4;
        my $uuid2  = generate_uuid_v4;

        my $uuids  = $model->_assert_document_ids([$uuid, $uuid, $uuid2]);
        cmp_deeply($uuids, [ $uuid, $uuid2 ], "found the corect uuids");
        is($uuids->[0], $uuid, "Found the same one");
        is($uuids->[1], $uuid2, "F");
    }

}

sub test_assert_file_attributes {
    my $model = _get_model;

    my $zaak = mock_one();
    my $attr = mock_one(magic_string => 'my magic string');
    my ($file1, $file2) = (generate_uuid_v4, generate_uuid_v4);
    my $values = { 'my magic string' => [ $file1, $file2 ]};
    my $documents = { };

    my $override = override(
        _subname('_get_filestore_by_uuid') => sub {
            shift;
            my $uuid = shift;
            if   ($uuid eq $file1) { return (1, 1) }
            else                   { return (1, 0) }
        }
    );

    $model->_assert_file_attribute(
        $zaak, $attr, $values, $documents,
    );

    subtest 'file add' => sub {
        is(keys %{ $documents->{add} }, 1, "One file update");
        ok(exists $documents->{add}{$file2}, "$file2 will been be added");

        is(keys %{$documents->{add}{$file2}}, 2, "found all keys");
        my $found = all { defined $documents->{add}{$file2}{$_} }
            qw(attribute filestore);
        ok($found, "found all things for a file add");
    };


    subtest 'file update' => sub {
        is(keys %{ $documents->{update} }, 1, "One file update");
        ok(exists $documents->{update}{$file1}, "$file1 will be updated");
        my $found = all { defined $documents->{update}{$file1}{$_} }
            qw(file attribute filestore);
        is(keys %{$documents->{update}{$file1}}, 3, "found all keys");
        ok($found, "found all things for a file update");
    };

    my @attr = ( $attr );
    my $called = 0;
    my $attrs = mock_one(next => sub { pop(@attr) });
    $override->replace( _subname('_assert_file_attribute') => sub { return $called++; });
    $model->_assert_file_attributes($attrs);
    is($called, 1, "Called _assert_file_attributes");


}

sub test_assert_file_metadata {
    my $model = _get_model;
    is($model->_assert_file_metadata, undef, "No metadata");
    cmp_deeply(
        $model->_assert_file_metadata(
            { origin => "Uitgaand", "origin_date" => '2017-08-30T16:02:25' }
        ),
        { origin => "Uitgaand", origin_date => '2017-08-30' },
        "Meta data found"
    );
}

sub test_assert_files {
    my $model = _get_model;

    my $file1 = generate_uuid_v4;
    my $file2 = generate_uuid_v4;
    my $file3 = generate_uuid_v4;
    my $file4 = generate_uuid_v4;

    my %documents = (
        add => {
            $file1 => {},
        },
        update => {
            $file2 => {},
        },
    );
    my @files = (
        { reference => $file1 },
        { reference => $file3, name => 'file3', metadata => { origin => 'Foo' } },
        { reference => $file2, metadata => {} },
        { reference => $file4, name => 'foo' },
    );

    my $case = mock_one();
    my $override = override(
        _subname('_get_filestore_by_uuid') => sub {
            shift;
            my $uuid = shift;
            if ($uuid eq $file2 || $uuid eq $file4) {
                return ("filestore: $uuid", "file: $uuid");
            }
            else { return ("filestore: $uuid", 0) }
        }
    );

    $model->_assert_files(\%documents, \@files, $case);

    cmp_deeply(
        \%documents,
        {
            add => {
                $file1 => {
                    name => undef,
                    metadata => undef,
                },
                $file3 => {
                    name => 'file3',
                    metadata => { origin => 'Foo' },
                    filestore => "filestore: $file3",
                },
            },
            update => {
                $file2 => {
                    metadata  => undef,
                    name => undef,
                },
                $file4 => {
                    file      => "file: $file4",
                    metadata  => undef,
                    filestore => "filestore: $file4",
                    name => 'foo',
                },
            },
        },
        "File assertions ok",
    );

}

sub test_generate_case_id {
    my $model = _get_model(
        schema => mock_one(
            resultset => sub {
                return mock_one(_generate_case_id => 42) if shift eq 'Zaak';
                die "Horrible death";
            }
        )
    );
    lives_ok(
        sub {
            $model->_generate_case_id;
        },
        "Calls _generate_case_id on schema->resultset('Zaak')"
    );
}


sub test_get_betrokkene_model_subject {
    my $model = _get_model;

    my $person = generate_bsn;
    my $company = generate_kvk;

    lives_ok(
        sub {
            $model->_get_betrokkene_model_subject('person', $person);
        },
        "Got a person"
    );

    lives_ok(
        sub {
            $model->_get_betrokkene_model_subject('company', { kvk_number => $company });
        },
        "Got a company"
    );

    throws_ok(
        sub {
            $model = _get_model(subject_model => mock_one(search => undef));
            $model->_get_betrokkene_model_subject('company',
                { kvk_number => $company });
        },
        qr#case/get_subject#,
        "Did not find a soul"
    );

    throws_ok(
        sub {
            $model->_get_betrokkene_model_subject('company',
                { kvk_number => 1 });
        },
        qr#invalid: kvk_number#,
        "KvK number isn't elfproef",
    );

    throws_ok(
        sub {
            $model->_get_betrokkene_model_subject('person', '012345678');
        },
        qr#invalid: bsn#,
        "BSN number isn't elfproef",
    );
}

sub test_get_employee_subject {
    my $model = _get_model(
        schema => mock_one(
            'X-Mock-Strict' => 1,
            resultset       => { find => { as_object => 1 } }
        )
    );

    lives_ok(
        sub {
            $model->_get_employee_subject(42);
        },
        "Called schema->resultset->search"
    );

    throws_ok(
        sub {
            my $model = _get_model(
                schema => mock_one(
                    'X-Mock-Strict' => 1,
                    resultset       => { find => undef }
                )
            );
            $model->_get_employee_subject(42);
        },
        qr#api/v1/case/subject_not_found#,
        "Unable to find employee"
    );
}

sub test_get_filestore_by_uuid {
    my $model = _get_model;


    my $case;
    my $uuid = generate_uuid_v4;

    throws_ok(
        sub {
            $model->_get_filestore_by_uuid("not a uuid");
        },
        qr#case/filestore/uuid/syntax#,
        "Not a UUID"
    );

    throws_ok(
        sub {
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        qr#api/v1/case/create/multiple_file_references#,
        "Too many files found"
    );

    throws_ok(
        sub {
            $model = _get_model(schema => mock_one(resultset => { find => undef }));
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        qr#case/file/reference/not_found#,
        "Too many files found"
    );

    my @files = ( mock_one() );
    $model = _get_model(
        schema => mock_one(
            resultset => {
                find => {
                    next => sub { pop @files }
                }
            }
        )
    );

    lives_ok(
        sub {
            $model->_get_filestore_by_uuid($uuid, $case);
        },
        "Got one file from the filestore"
    );

    my $args;
    $model = _get_model(schema => mock_one(
        search_rs => sub { $args = shift; return (mock_one(next => undef)) },
        next => sub { pop @files }
    ));
    my @answer = $model->_get_filestore_by_uuid($uuid, $case);
    is(@answer, 2, "Got file and filestore");
    cmp_deeply($args,  {case_id => undef}, "No case supplied in search");

    $case = mock_one(id => 42);
    @answer = $model->_get_filestore_by_uuid($uuid, $case);
    is(@answer, 2, "Got file and filestore");
    cmp_deeply($args,  {case_id => 42}, "Case supplied in search");

}

sub test_get_group {
    my $model = _get_model;
    lives_ok(
        sub {
            $model->_get_group;
        },
        "_get_group called"
    );
}

sub test_get_role {
    my $model = _get_model;
    lives_ok(
        sub {
            $model->_get_role;
        },
        "_get_role called"
    );
}

sub test_get_object_model_subject {
    my $model = _get_model;

    # I'm not really sure how to test this: patch welcome
    my $subject = mock_one;
    lives_ok(
        sub {
            $model->_get_object_model_subject($subject);
        },
        "_get_object_model_subject called"
    );
}

sub test_get_subject {
    my $model = _get_model;

    # Test pass through
    my $override = override(_subname('_get_object_model_subject') => sub { shift; return shift });
    $override->replace(_subname('_get_employee_subject') => sub { shift; return shift });
    $override->replace(_subname('_get_betrokkene_model_subject') => sub { shift; return \@_ });

    is(
        $model->_get_subject({ subject => "call subject" }),
        "call subject",
        "Correct pass through"
    );
    is(
        $model->_get_subject(
            { subject_type => "employee", subject_id => "call employee" }
        ),
        "call employee",
        "Call to _get_employee_subject"
    );
    cmp_deeply(
        $model->_get_subject(
            { subject_type => "person|company", subject_id => "the id" }
        ),
        ['person|company', 'the id'],
        "Call to _get_object_model_subject"
    );
}


sub test_get_subject_by_legacy_id {
    my $model = _get_model;

    lives_ok(
        sub {
            $model->_get_subject_by_legacy_id;
        },
        '_get_subject_by_legacy_id called'
    );
}

sub test_log_case_id {

    my $args;
    my $type;
    my $model = _get_model(schema =>
            mock_one(trigger => sub { ($type, $args) = @_;}));

    $model->_log_case_id(42);

    cmp_deeply(
        $args,
        {
            component    => 'zaak',
            component_id => 42,
            data         => {
                remote_system => 'Zaaksysteem',
                interface     => 'api/v1',
            },
        },
        "logging trigger executed"
    );

    is($type, 'case/id_requested', "type is correct");
}

sub test_assert_case_attributes {
    my $model = _get_model;

    my @next = ();
    my $rs = mock_one(next => sub { pop @next });
    my $attr_values = { foo => 'bar'};
    my $values = $model->assert_case_attributes($rs, $attr_values);
    cmp_deeply($values, [], "No values to assert");

    @next = (
        mock_one(
            magic_string => 'foo',
            get_column   => sub {
                my $i = shift;
                return 'foo' if $i eq 'bibliotheek_kenmerken_id';
            }
        ),
    );

    my $override = override(_subname('normalize_attribute_value') => sub { return "this is my value"});
    $values = $model->assert_case_attributes($rs, $attr_values);
    cmp_deeply($values, [{'foo' => 'this is my value'}], "Found values");

}

sub test_assert_contact_details {
    my $model = _get_model;
    my $rv = $model->assert_contact_details;
    cmp_deeply($rv, {}, "No contact details entered");

    $rv = $model->assert_contact_details({mobile_number => '0612345678'});
    cmp_deeply($rv, {mobile_number => '0612345678'}, "No contact details entered");
}

sub test_assert_documents {
    my $model = _get_model;

    my $override = override(_subname('_assert_file_attributes') => sub { return {foo => 'bar'} });
    $override->replace(_subname('_assert_files') => sub { return {bar => 'baz'} });

    my ($attrs, $values, $files, $case) = (mock_one, mock_one, mock_one, mock_one);
    my $rv = $model->assert_documents($attrs, $values, $files, $case);
    cmp_deeply($rv, { bar => 'baz' }, "called assert_files");

    $rv = $model->assert_documents($attrs, $values, undef, $case);
    cmp_deeply($rv, { foo => 'bar' }, "No files, so last call is _assert_file_attributes");
}

sub test_assert_subjects {
    my $model = _get_model;

    my $node = mock_one('X-Mock-ISA' => "Zaaksysteem::Schema::ZaaktypeNode");
    my $subjects = {};

    my %rv = $model->assert_subjects($node, $subjects);

    # TODO: Mind boggling code..
    # Fetch the input params from Postman

    my @types = qw(assignee related requestor);
    is(keys %rv, @types, "have all requestor types");
    my $all = all { exists $rv{$_} } @types;
    ok($all, "Found them all by name");

}

sub test_create_delayed_case {
    my $model = _get_model(user => mock_one(id => 666));

    my $override = override(_subname('_generate_case_id') => sub { return 42 });

    my $case_id;
    $override->replace(_subname('_log_case_id') => sub { shift; $case_id = shift; return });

    my $log_args;
    $override->replace(_subname('add_qitem') => sub { shift; $log_args = {@_}; return 1 });

    my $qitem = $model->create_delayed_case(
        { my => { very => { special => 'args' } } });

    is($qitem, 1, "We have a qitem");
    is($case_id, 42, "Generated case ID is logged");

    cmp_deeply(
        $log_args,
        {
            data => {
                _subject_id => 666,
                case_id     => 42,
                create_args => { my => { very => { special => 'args' } } }
            },
            label => 'Create case 42',
            type  => 'create_case',
        },
        "Queue item created with the correct arguments"
    );

}

sub test_get_document_attributes {
    my $model = _get_model;

    my $rs = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken',
        search_rs => sub { return shift },
    );
    my $args = $model->get_document_attributes($rs);
    cmp_deeply($args, { 'bibliotheek_kenmerken_id.value_type' => 'file' }, "correct search query");
}

sub test_normalize_attribute_value {
    my $model = _get_model;

    my $attr = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::DB::Component::ZaaktypeKenmerken',
        search_rs => sub { return shift },
    );

    my $value = $model->normalize_attribute_value($attr);
    is($value, undef, "No value is a undef value");

    $value = $model->normalize_attribute_value($attr, []);
    is($value, undef, "Empty array value is a undef value");

    $value = $model->normalize_attribute_value($attr, [[ 'nope' ]]);
    cmp_deeply($value, [ 'nope' ], "Multi attribute support");

    throws_ok(
        sub {
            $model->normalize_attribute_value($attr, [ 'nope' ]);
        },
        qr#api/v1/case/incorrect_values#,
        "Multiple attribute support validation"
    );

}

sub test_create_case {
    my $model = _get_model;

    my %zaak_methods_called = ( dc => 0, t => 0);

    my $zaak = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Zaken::ComponentZaak',
        discard_changes => sub { return $zaak_methods_called{dc}++ },
        _touch          => sub { return $zaak_methods_called{t}++ }
    );

    my ($create_params, $update_contact_details, $related_case_subjects,
        $set_case_allocation, $add_case_files, $update_case_files,
        $exec_actions);
    my $override = override(_subname('_create_case') =>
            sub { shift; $create_params = shift; return $zaak });

    # Start mocking the world
    $override->replace(_subname('update_contact_details') =>
            sub { $update_contact_details = pop});

    $override->replace(_subname('relate_case_subjects') =>
            sub { $related_case_subjects = pop });

    $override->replace(_subname('set_case_allocation') =>
            sub { $set_case_allocation = pop });

    $override->replace(
        _subname('add_case_files') => sub { $add_case_files = pop });

    $override->replace(
        _subname('update_case_files') => sub { $update_case_files = pop });

    # TODO: Add test for this method - out of scope for v1 delayed case
    $override->replace(_subname('execute_phase_actions') => sub { shift; shift; $exec_actions = shift});

    my %args = (
        contact_details => { foon => 'number' },
        related         => "related subjects",
        route           => {
            role  => 'behandelaar',
            group => 'Mintlab'
        },
        assignee  => 'behandelaar',
        open      => 'ja',
        documents => {
            add    => { file => 'add' },
            update => { file => 'update' }
        },
    );

    my %copy = %args;

    $model->create_case(\%args);

    cmp_deeply($create_params, \%copy, "Creation params are ok");

    cmp_deeply($update_contact_details, {foon => 'number'}, "updated contact details for subject");

    cmp_deeply($related_case_subjects, \%copy, "updated contact details for subject");

    cmp_deeply($set_case_allocation, \%copy, "set case allocation");

    cmp_deeply($add_case_files, { file => 'add' }, "Added case files");

    cmp_deeply($update_case_files, { file => 'update' },
        "Updated case files");

    cmp_deeply($exec_actions, { phase => 1, skip_assignment => 1}, 'Executed phase actions');
    cmp_deeply(\%zaak_methods_called, { t => 1, dc => 1}, 'Zaak discard_changes and touched');

    delete $args{assignee};
    $model->create_case(\%args);
    cmp_deeply($exec_actions, { phase => 1, skip_assignment => 0}, 'Executed phase actions and without skipping assignment');
}

# TODO: Improve these tests
sub test_add_qitem {
    my $model = _get_model;
    $model->add_qitem;
}

sub test_create_qitem {
    my $model = _get_model;
    $model->create_qitem;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
