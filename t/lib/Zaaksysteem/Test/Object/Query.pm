package Zaaksysteem::Test::Object::Query;

use Zaaksysteem::Test;

use Zaaksysteem::Object::Query;

=head1 NAME

Zaaksysteem::Test::Object::Query - Object query tests

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Query

=cut

sub test_helpers {
    my $query = qb('case');

    isa_ok($query, 'Zaaksysteem::Object::Query', 'qb() helper produces query instance');
    is($query->type, 'case', 'qb("type") sets correct type');
    ok(!$query->has_cond, 'qb() with single parameter has no condition');
    ok(!$query->has_sort, 'qb() with single parameter has no sort');

}

sub test_qb_eq {
    my $expr = qb_eq('field', 'value');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Equal',
    );

    my @exprs = $expr->all_expressions;

    ok(scalar(@exprs) == 2, 'equality produces expression instances');

    isa_ok(
        $exprs[0],
        'Zaaksysteem::Object::Query::Expression::Field',
        'eq->expressions[0]'
    );

    isa_ok(
        $exprs[1],
        'Zaaksysteem::Object::Query::Expression::Literal',
        'eq->expressions[0]'
    );
}

sub test_qb_and {
    my $expr = qb_and();

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Conjunction',
    );

    ok(scalar($expr->all_expressions) == 0, 'no arguments give no expressions');

    $expr = qb_and(qb_and(), qb_and());

    ok(scalar($expr->all_expressions) == 2, 'arguments give expressions');
}

sub test_qb_or {
    my $expr = qb_or();

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Disjunction',
    );

    ok(scalar($expr->all_expressions) == 0, 'no arguments give no expressions');

    $expr = qb_or(qb_or(), qb_or());

    ok(scalar($expr->all_expressions) == 2, 'arguments give expressions');
}

sub test_qb_not {
    my $expr = qb_not(qb_and());

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Inversion',
    );

    isa_ok(
        $expr->expression,
        'Zaaksysteem::Object::Query::Expression::Conjunction',
        'not->expression'
    );
}

sub test_qb_in {
    my $expr = qb_in('field', []);

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::MemberRelation',
    );

    isa_ok(
        $expr->expression,
        'Zaaksysteem::Object::Query::Expression::Field',
        'in->expression'
    );

    isa_ok(
        $expr->set,
        'Zaaksysteem::Object::Query::Expression::Set',
        'in->set'
    )
}

sub test_qb_neq {
    my $expr = qb_neq('field', 'value');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Inversion',
    );

    isa_ok(
        $expr->expression,
        'Zaaksysteem::Object::Query::Expression::Equal',
        'neq->expression'
    );
}

sub test_qb_like {
    my $expr = qb_like('field', 'value');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::ContainsString',
    );

    isa_ok(
        $expr->string,
        'Zaaksysteem::Object::Query::Expression::Field',
        'like->string'
    );

    isa_ok(
        $expr->match,
        'Zaaksysteem::Object::Query::Expression::Literal',
        'like->match'
    );

    is($expr->mode, 'infix', 'default substr match is infix');

    $expr = qb_like('field', 'value', 'prefix');

    is($expr->mode, 'prefix', 'prefix substr match mode can be set');
}

sub test_qb_re {
    my $expr = qb_re('field', 'regex');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Regex'
    );

    isa_ok(
        $expr->string,
        'Zaaksysteem::Object::Query::Expression::Field',
        'String argument interpreted as field'
    );

    is($expr->pattern, 'regex', 'String argument interpreted as pattern');
}

sub test_qb_field {
    my $expr = qb_field('field');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Field',
    );

    is($expr->name, 'field', 'String argument interpreted as fieldname');
}

sub test_qb_lit {
    my $expr = qb_lit('type', 'value');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Literal',
    );

    is($expr->type, 'type', 'String argument interpreted as value type');
    is($expr->value, 'value', 'String argument interpreted as value');
}

sub test_qb_set {
    my $expr = qb_set();

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Expression::Set'
    );

    ok(scalar($expr->all_expressions) == 0, 'empty set has no expressions');

    $expr = qb_set(qb_lit('type', 'value'), qb_lit('type', 'other'));

    ok(scalar($expr->all_expressions) == 2, 'two literals == two expressions');
}

sub test_qb_sort {
    my $expr = qb_sort('field');

    isa_ok(
        $expr,
        'Zaaksysteem::Object::Query::Sort',
    );

    isa_ok(
        $expr->expression,
        'Zaaksysteem::Object::Query::Expression::Field',
        'String argument interpreted as field reference'
    );

    ok(!$expr->reverse, 'Default sort is non-inverted');

    $expr = qb_sort('field', 'desc');

    ok($expr->reverse, 'String argument inverts sort');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
