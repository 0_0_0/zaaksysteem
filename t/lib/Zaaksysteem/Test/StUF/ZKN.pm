package Zaaksysteem::Test::StUF::ZKN;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::StUF::ZKN - Test the plugin modules of ZS::StUF::ZKN

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::ZKN

=cut

use UUID::Tiny ':std';
use IO::All;
use Mock::Quick qw(qmeth);

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Test::XML qw(:all);

use Zaaksysteem::Tools qw(dump_terse);
use Zaaksysteem::Tools::RandomData qw(generate_bsn);

use Zaaksysteem::XML::Generator::StUF0310;
use Zaaksysteem::StUF::0310::Processor;

sub test_get_natuurlijk_persoon {

    my $model = Zaaksysteem::StUF::0310::Processor->new(

        # Not so very strong type checking on this processor..
        betrokkene_model => mock_moose_class(),
        object_model     => mock_moose_class(),
        record           => mock_moose_class(),

        schema    => mock_dbix_schema(),
        interface => mock_interface(),

        municipality_code => '1332',
    );

    my $bsn = generate_bsn();

    lives_ok(
        sub {

            no warnings qw(redefine once);
            local *Zaaksysteem::StUF::0310::Processor::_assert_stuf_interface
                = sub { return mock_interface() };
            local *Zaaksysteem::StUF::0310::Processor::_find_bsn
                = sub { return undef };
            local *Zaaksysteem::BR::Subject::search        = sub { return 1 };
            local *Zaaksysteem::BR::Subject::remote_import = sub { return 1 };
            use warnings;

            $model->_get_natuurlijk_persoon($bsn);
        },
        '_get_natuurlijk_persoon works fine'
    );

}

sub test_write_case {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $bsn       = generate_bsn();
    my $case      = mock_case();

    my $now  = DateTime->now();
    my $data = {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Lk01',
            entiteittype     => 'ZAK',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
        parameters => {
            mutatiesoort      => 'T',
            indicatorOvername => 'V',
        },
        object => [
            {
                identificatie      => { _ => sprintf('%05d', $case->id) },
                omschrijving       => 'Foo',
                startdatum         => { _ => $now->strftime('%Y%m%d') },
                registratiedatum   => { _ => $now->strftime('%Y%m%d') },
                zaakniveau         => 1,
                deelzakenIndicatie => "N",

                isVan => {
                    entiteittype => 'ZAKZKT',
                    gerelateerde => {
                        entiteittype => 'ZKT',
                        omschrijving => 'Onbekend',
                        code         => 100,
                        ingangsdatum => { _ => '20160101' },
                    },
                },
                heeftAlsInitiator => {
                    gerelateerde =>
                        { natuurlijkPersoon => { 'inp.bsn' => $bsn, } },
                },
            },
        ],
    };

    my $method = 'write_case';

    my $xml = $generator->$method(writer => $data,);

    # ZOMG, how many XSD's do we need?
    my $xml_schema = XML::Compile::Schema->new(
        [

            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/0301/stuf0301mtom.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_ent_basis.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_simpleTypes.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/gml/bag-gml.xsd
                share/wsdl/stuf/xlink/xlinks.xsd
                share/wsdl/stuf/xmlmime/xmlmime.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_ent_basis.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_ent_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_stuf_mutatie.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'zakLk01',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

sub test_creeer_zaak_id {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $bsn       = generate_bsn();

    my $now  = DateTime->now();
    my $data = {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Di02',
            functie          => 'genereerZaakidentificatie',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
    };

    my $method = 'generate_case_id';

    my $xml = $generator->$method(writer => $data,);

    # ZOMG, how many XSD's do we need?
    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_stuf_zs-dms.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_zs-dms.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'genereerZaakIdentificatie_Di02',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
