package Zaaksysteem::Test::BR::Controlpanel;

use Zaaksysteem::Test;
use Mock::Quick;

my $PACKAGE = 'Zaaksysteem::BR::Controlpanel';

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::BR::Controlpanel

=head2 test_

=cut

sub test__get_full_raw_fqdn {
    my $self    = shift;

    use_ok($PACKAGE);

    ### Mock controlpanel and instance object
    my $controlpanel = qobj(
        get_object_attribute => qmeth {
            return unless $_[1] eq 'customer_type';

            return qobj(
                value => 'government'
            );
        }
    );

    my $instance = qobj(
        get_object_attribute => qmeth {
            return unless $_[1] eq 'customer_type';

            return qobj(
                value => 'commercial'
            );
        }
    );

    throws_ok(
        sub {
            Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
                host    => undef,
            ),
        },
        qr/Validation of profile/,
        "Throws on missing params"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234.zaaksysteem.net",
            customer_type   => "commercial",
            controlpanel    => $controlpanel
        ),
        'test1234.zaaksysteem.net',
        "Check return of valid given FQDN"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234",
            customer_type   => "commercial",
            controlpanel    => $controlpanel
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from customer type settings'

    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234",
            customer_type   => "government",
            controlpanel    => $controlpanel
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from customer type settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from controlpanel settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel,
            instance        => $instance,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from instance settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host            => "test1234",
            customer_type   => 'commercial',
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from customer_type settings'
    );
}

sub test__verify_host_or_fqdn {
    my $self    = shift;

    use_ok($PACKAGE);

    ### Mock controlpanel and instance object
    my $controlpanel = qobj(
        get_object_attribute => qmeth {
            return unless $_[1] eq 'customer_type';

            return qobj(
                value => 'government'
            );
        }
    );

    my $instance = qobj(
        get_object_attribute => qmeth {
            return unless $_[1] eq 'customer_type';

            return qobj(
                value => 'commercial'
            );
        }
    );

    my $omodel_data = {
        count => 0
    };

    my $objectmodel = qobj(
        count => qmeth {
            return $omodel_data->{count};
        }
    );

    throws_ok(
        sub {
            Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
                host    => undef,
            ),
        },
        qr/Validation of profile/,
        "Throws on missing params"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234.zaaksysteem.net",
            customer_type   => "commercial",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        "Check return of valid given FQDN"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            customer_type   => "commercial",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from customer type settings'

    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            customer_type   => "government",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from customer type settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from controlpanel settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel,
            instance        => $instance,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from instance settings'
    );

    ### Check messages
    {
        my $messages = {};
        local $omodel_data->{count} = 1;

        is(
            Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
                host            => "test1234",
                controlpanel    => $controlpanel,
                instance        => $instance,
                objectmodel     => $objectmodel,
                messages        => $messages,
            ),
            undef,
            'Domain already exist'
        );

        like($messages->{fqdn}, qr/domain.*in use/,'Got domain already exist error message');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "wiki",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/reserved/,'Reserved hostname checking');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "bla.bla",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/does not contain a dot/,'single word hostname check');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test.zaaksysteem.not",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/does not contain a dot/,'single word hostname check');

    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.