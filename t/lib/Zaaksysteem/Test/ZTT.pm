package Zaaksysteem::Test::ZTT;
use Zaaksysteem::Test;

use Zaaksysteem::ZTT;

sub test_ztt_join_basic {
    my $ztt = Zaaksysteem::ZTT->new();
    $ztt->add_context({magic => 'string'});

    {
        my $processed = $ztt->process_template("[[ magic ]]");
        is($processed->string, 'string', 'Simple string gets processed');
    }

    {
        my $processed = $ztt->process_template('[[ join(magic, ",", "+ ") ]]');
        is($processed->string, 'string', 'join() on a single string returns the string');
    }
    
}

sub test_ztt_join_array {
    my $ztt = Zaaksysteem::ZTT->new();
    $ztt->add_context({
        magic0 => [],
        magic1 => ['string1'],
        magic2 => ['string1', 'string2'],
        magic3 => ['string1', 'string2', 'string3'],
    });

    {
        my $processed = $ztt->process_template("[[ magic2 ]]");

        is(
            $processed->string,
            "string1, \nstring2",
            'Plain array gets processed to string with commas and newlines'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic0, ",") ]]');

        is(
            $processed->string,
            "",
            'join() on empty array leads to empty result string'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic1, ", ", " + ") ]]');
        is(
            $processed->string,
            "string1",
            'join() on an array with 1 element works properly (no separators)'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic2, ", ", " + ") ]]');
        is(
            $processed->string,
            "string1 + string2",
            'join() on an array with 2 elements works properly (only the last separator is used)'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic3, ", ", " + ") ]]');
        is(
            $processed->string,
            "string1, string2 + string3",
            'join() on an array with 3 elements works properly (both separators used)'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic3, ", ") ]]');
        is(
            $processed->string,
            "string1, string2, string3",
            'join() on an array with 3 elements works properly (default final separator = normal separator)'
        );
    }

    {
        my $processed = $ztt->process_template('[[ join(magic3) ]]');
        is(
            $processed->string,
            "string1string2string3",
            'join() on an array with 3 elements works properly (default separators)'
        );
    }
}

sub test_ztt_join_horror {
    my $strange = { "strange" => "data" };

    my $ztt = Zaaksysteem::ZTT->new();
    $ztt->add_context({
        magic1 => $strange,
        magic2 => undef,
        magic3 => Zaaksysteem::Test::ZTT::Dummy->new(),
        magic4 => [
            Zaaksysteem::Test::ZTT::Dummy->new(),
            Zaaksysteem::Test::ZTT::Dummy->new(),
            Zaaksysteem::Test::ZTT::Dummy->new(),
        ],
    });

    {
        my $processed = $ztt->process_template('[[ join(magic1, "X" ]]');
        is($processed->string, '[[ join(magic1, "X" ]]', 'ZTT does not process hashrefs');
    }

    {
        my $processed = $ztt->process_template('[[ join(magic2, "X") ]]');
        is($processed->string, '', 'join(undef) returns an empty string');
    }

    {
        my $processed = $ztt->process_template('[[ join(magic3, "X") ]]');
        is($processed->string, 'stringify', 'join(stringifyable_object) returns the stringified object');
    }

    {
        my $processed = $ztt->process_template('[[ join(magic4, ", ", " en ") ]]');
        is(
            $processed->string,
            'stringify, stringify en stringify',
            'join(array_of_stringifyable) returns the stringified objects, properly joined'
        );
    }
}

package Zaaksysteem::Test::ZTT::Dummy {
    use Moose;

    use overload qw{""} => sub { return "stringify" };

    __PACKAGE__->meta->make_immutable;
};

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
